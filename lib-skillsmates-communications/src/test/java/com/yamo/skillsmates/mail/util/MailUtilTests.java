package com.yamo.skillsmates.mail.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MailUtilTests {

    @BeforeEach
    public void setup(){
    }

    @Test
    @DisplayName("Url should be valid")
    void emailShouldBeValid(){
        assertThat(MailUtil.isEmailValid("belinda.mengue@yopmail.com")).isTrue();
    }

}
