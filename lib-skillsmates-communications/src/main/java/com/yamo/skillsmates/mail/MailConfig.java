package com.yamo.skillsmates.mail;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.util.Properties;

/**
 * 
 *
 */
@Configuration
@PropertySource(value = { "classpath:application.properties" })
public class MailConfig {

  @Value("${mail.host}")
  private String host;
  
  @Value("${mail.port}")
  private Integer port;

  @Value("${mail.address.username}")
  private String username;

  @Value("${mail.address.password}")
  private String password;
  
  @Value("${thymeleaf.template.suffix}")
  private String suffix;
  
  @Value("${thymeleaf.template.mode}")
  private String mode;
  
  @Value("${thymeleaf.template.encoding}")
  private String encoding;

  /**
   * Configuration de l'envoi de mail
   * 
   * @return Le bean {@link JavaMailSender}
   */
  @Bean
  public JavaMailSender javaMailService() {
    JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
    javaMailSender.setHost(host);
    javaMailSender.setPort(Integer.valueOf(port));
    javaMailSender.setUsername(username);
    javaMailSender.setPassword(password);
    javaMailSender.setJavaMailProperties(getMailProperties());
    return javaMailSender;
  }
  
  /**
   * Configuration du template thymeleaf
   * 
   * @return Le bean {@link ClassLoaderTemplateResolver}
   */
  @Bean
  public ClassLoaderTemplateResolver emailTemplateResolver(){
      ClassLoaderTemplateResolver emailTemplateResolver = new ClassLoaderTemplateResolver();
      emailTemplateResolver.setSuffix(suffix);
      emailTemplateResolver.setTemplateMode(mode);
      emailTemplateResolver.setCharacterEncoding(encoding);
      emailTemplateResolver.setOrder(1);

      return emailTemplateResolver;
  }
  
  private Properties getMailProperties() {
      Properties properties = new Properties();
      properties.setProperty("mail.transport.protocol", "smtp");
      properties.setProperty("mail.smtp.auth", "true");
      properties.setProperty("mail.smtp.starttls.enable", "true");
      properties.setProperty("mail.debug", "true");
      return properties;
  }
}
