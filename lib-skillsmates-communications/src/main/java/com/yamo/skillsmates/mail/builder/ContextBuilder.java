package com.yamo.skillsmates.mail.builder;

import org.thymeleaf.context.Context;

/**
 *
 */
public abstract class ContextBuilder {

  protected Context context;
  
  /**
   * Constructor.
   * 
   * @param context {@link Context}
   */
  public ContextBuilder(Context context) {
    super();
    this.context = context;
  }
  
  /**
   * 
   */
  public abstract void build();
}
