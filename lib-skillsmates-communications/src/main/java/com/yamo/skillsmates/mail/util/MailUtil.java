package com.yamo.skillsmates.mail.util;

import com.yamo.skillsmates.common.logging.LoggingUtil;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Classe utilitaire réalisant diverses opérations liées à un service de mail.
 * @author sebastien.planard
 */
public class MailUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MailUtil.class);
	
	/**
	 * <p>Permet de transformer une chaîne de caractères contenant des adresses email séparées par des virgules 
	 * en une liste d'objets {@link InternetAddress}.</p>
	 *  
	 * @param commaSeparatedString La chaîne de caractères. 
	 * @return Une liste d'adresses.
	 */
	public static List<InternetAddress> commaSeparatedStringToAddressesList( String commaSeparatedString ){
		return commaSeparatedStringToAddressesList( commaSeparatedString, false );
	}
	
	/**
	 * <p>Permet de transformer une chaîne de caractères contenant des adresses email séparées par des virgules 
	 * en une liste d'objets {@link InternetAddress}.</p>
	 *  
	 * @param commaSeparatedString La chaîne de caractères.
	 * @param strict Le booléen qui sera passé au constructeur <code>InternetAddress()</code>. 
	 * S'il vaut <code>true</code>, la synthaxe RFC822 sera validée de façon stricte. 
	 * @return Une liste d'adresses.
	 */
	public static List<InternetAddress> commaSeparatedStringToAddressesList( String commaSeparatedString, boolean strict ){
		boolean isInfoEnabled = LOGGER.isInfoEnabled();
		if( isInfoEnabled ) LOGGER.info( LoggingUtil.START + " [commaSeparatedString=" + commaSeparatedString + ", strict=" + strict + "]" );
		
		List<InternetAddress> adressesList = new ArrayList<InternetAddress>();
		if( commaSeparatedString != null && commaSeparatedString.trim().length() > 0 ){
			for( String email : Arrays.asList( commaSeparatedString.split("\\s*,\\s*") ) ){
				try {
					adressesList.add( new InternetAddress( email, strict ) );
				} catch( AddressException ae ){
					LOGGER.warn( "L'adresse email suivante est incorrecte : " + email, ae );
				}
			}
		}
		
		if( isInfoEnabled ) LOGGER.info(LoggingUtil.END + " [adressesList=" + adressesList + "]" );
		return adressesList;
	}

	/**
	 * check if email is valid
	 * @param email to check
	 * @return true if valid, false otherwise
	 */
	public static boolean isEmailValid(String email){
		return EmailValidator.getInstance().isValid(email);
	}
	
}
