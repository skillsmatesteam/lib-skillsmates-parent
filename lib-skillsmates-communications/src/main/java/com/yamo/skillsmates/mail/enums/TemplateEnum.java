package com.yamo.skillsmates.mail.enums;

/**
 * Enumération des différents templates
 */
public enum TemplateEnum {
  /** Template de validation d'email */
  REGISTRATION_VALIDATION("templates/registration-validation"),
  RESET_PASSWORD("templates/reset-password"),
  CONFIRM_RESET_PASSWORD("templates/confirm-reset-password"),
  ERROR_RESET_PASSWORD("templates/error-reset-password"),
  NOTIFICATION("templates/notification"),
  CONFIRMATION_VALIDATION("templates/confirmation-validation"),
  NOTIFICATION_NEW_LIKE("templates/notification-newlike"),
  NOTIFICATION_NEW_POST("templates/notification-newpost"),
  NOTIFICATION_NEW_COMMENT("templates/notification-newcomment"),
  NOTIFICATION_NEW_SHARE("templates/notification-newshare"),
  NOTIFICATION_NEW_MESSAGE("templates/notification-newmessage"),
  NOTIFICATION_NEW_FOLLOWER("templates/notification-newfollower"),
  NOTIFICATION_NEW_FAVORITE("templates/notification-newfavorite"),
  POST_VIEW("templates/post-view");

  /***/
  private final String value;

  /**
   * @param value
   */
  private TemplateEnum(String value) {
    this.value = value;
  }

  /**
   * @return the value
   */
  public String getValue() {
    return value;
  }
  
}
