package com.yamo.skillsmates.mail.service;

import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.mail.enums.ImageEnum;
import com.yamo.skillsmates.mail.enums.TemplateEnum;
import com.yamo.skillsmates.mail.enums.VariableEnum;
import com.yamo.skillsmates.mail.exception.SkillsmatesMailException;
import com.yamo.skillsmates.mail.helper.MailHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Service
@PropertySource(value = { "classpath:application.properties" })
// surchargé par l'application properties du microservice
public class MailService {

	private static final String PNG_CONTENT_TYPE = "image/png";

	private static final Logger LOGGER = LoggerFactory.getLogger(MailService.class);

	@Value("${mail.address.username}")
	private String addressFrom;

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private TemplateEngine templateEngine;

	@Value("${ms.skillsmates.resources.baseurl}")
	private String baseUrl;
	@Value("${ms.skillsmates.resources.facebook}")
	private String facebookLink;
	@Value("${ms.skillsmates.resources.twitter}")
	private String twitterLink;
	@Value("${ms.skillsmates.resources.snapchat}")
	private String snapchatLink;
	@Value("${ms.skillsmates.resources.instagram}")
	private String instagramLink;
	@Value("${ms.skillsmates.resources.tiktok}")
	private String tiktokLink;

	@Value("${ms.skillsmates.resources.email}")
	private String email;
	@Value("${ms.skillsmates.resources.website}")
	private String website;
	@Value("${ms.skillsmates.resources.address}")
	private String address;

	/*
	 * Ajoute les image signature et entete dans le temlplate
	 * 
	 * @param helper {@link MailHelper}
	 */
	private void addImgEnteteSignature(final MailHelper helper, ImageEnum imgFooter) {
		helper.addInlineImage(ImageEnum.IMG_ENTETE, PNG_CONTENT_TYPE);
		helper.addInlineImage(imgFooter, PNG_CONTENT_TYPE);
	}

	/**
	 * Envoi du mail
	 * 
	 * @param toAddresses
	 *            adresses des destinataires
	 * @param subject
	 *            objet du mail
	 * @param template
	 *            template à utiliser
	 * @param templateVars
	 *            variables nécessaire au template
	 * @param replyTo
	 *            adresse de réponse
	 * @throws SkillsmatesMailException
	 */
	private void sendMail(List<InternetAddress> toAddresses, String subject, TemplateEnum template, Map<String, ?> templateVars, InternetAddress replyTo) throws SkillsmatesMailException {
		try {
			// Si pas d'adresse de destination ajout de l'adresse mail de la propriété
			if (CollectionUtils.isEmpty(toAddresses)) {
				throw new SkillsmatesMailException("L'email destinataire inconnu");
			}

			final MailHelper helper = new MailHelper(this.templateEngine, this.mailSender.createMimeMessage(), true, "UTF-8");

			// Prepare the context
			final Context context = helper.prepareContext(templateVars);
			context.setVariable(VariableEnum.VAR_STAT_SIGNATURE_IMG.getVariable(), helper.getRessourceImageName(ImageEnum.IMG_SIGNATURE));
			context.setVariable(VariableEnum.VAR_STAT_ENTETE_IMG.getVariable(), helper.getRessourceImageName(ImageEnum.IMG_LOGO));
			context.setVariable(VariableEnum.VAR_BASE_URL.getVariable(), baseUrl);
			context.setVariable(VariableEnum.VAR_FACEBOOK_LINK.getVariable(), facebookLink);
			context.setVariable(VariableEnum.VAR_INSTAGRAM_LINK.getVariable(), instagramLink);
			context.setVariable(VariableEnum.VAR_TWITTER_LINK.getVariable(), twitterLink);
			context.setVariable(VariableEnum.VAR_SNAPCHAT_LINK.getVariable(), snapchatLink);
			context.setVariable(VariableEnum.VAR_TIKTOK_LINK.getVariable(), tiktokLink);
			context.setVariable(VariableEnum.VAR_EMAIL.getVariable(), email);
			context.setVariable(VariableEnum.VAR_WEBSITE.getVariable(), website);
			context.setVariable(VariableEnum.VAR_ADDRESS.getVariable(), address);

			helper.getMimeMessageHelper().setSubject(subject);
			helper.getMimeMessageHelper().setFrom(addressFrom);
			helper.getMimeMessageHelper().setTo(toAddresses.toArray(new InternetAddress[toAddresses.size()]));
			if (replyTo != null) {
				helper.getMimeMessageHelper().setReplyTo(replyTo);
			}

			// Add the html content
			helper.getMimeMessageHelper().setText(helper.getHtmlContent(template.getValue(), context), true);

			// Send mail
			this.mailSender.send(helper.getMimeMessage());

		} catch (MessagingException mess) {
			throw new SkillsmatesMailException("L'email est mal formaté", mess);
		} catch (MailSendException mse) {
			throw new SkillsmatesMailException("Erreur lors de l'envoi de l'email", mse);
		}
	}

	/**
	 * Envoi du mail
	 * 
	 * @param toAdresses
	 *            adresses des destinataires
	 * @param subject
	 *            objet du mail
	 * @param template
	 *            template à utiliser
	 * @param templateVars
	 *            variables nécessaire au template
	 * @throws SkillsmatesMailException
	 */
	private void sendMail(List<InternetAddress> toAdresses, String subject, TemplateEnum template, Map<String, ?> templateVars) throws SkillsmatesMailException {
		sendMail(toAdresses, subject, template, templateVars, null);
	}
	/*private static String getCurrentBaseUrl() {
		ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest req = sra.getRequest();
		return req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath();
	}*/
	/**
	 * Envoi du mail
	 * 
	 * @param toAddress
	 *            adresse du destinataire
	 * @param subject
	 *            objet du mail
	 * @param template
	 *            template à utiliser
	 * @param templateVars
	 *            image à insérer dans le footer
	 * @throws SkillsmatesMailException
	 */
	private void sendMail(InternetAddress toAddress, String subject, TemplateEnum template, Map<String, String> templateVars) throws SkillsmatesMailException {
		List<InternetAddress> toAdresses = new ArrayList<InternetAddress>();
		toAdresses.add(toAddress);
		sendMail(toAdresses, subject, template, templateVars);
	}

	@Async
	public void registrationValidation(Map<String, String> templateVars, InternetAddress toAddress) throws SkillsmatesMailException {
		LOGGER.info(LoggingUtil.START + " [templateVars=" + templateVars + ", toAddress=" + toAddress + "]");

		// Préparation objet du mail
		String subject = "Skillsmates® : Confirmation de la création de compte";

		// Envoi
		sendMail(toAddress, subject, TemplateEnum.REGISTRATION_VALIDATION, templateVars);
		LOGGER.info(LoggingUtil.END);
	}

	@Async
	public void resetPassword(Map<String, String> templateVars, InternetAddress toAddress) throws SkillsmatesMailException {
		LOGGER.info(LoggingUtil.START + " [templateVars=" + templateVars + ", toAddress=" + toAddress + "]");

		// Préparation objet du mail
		String subject = "Skillsmates® : Réinitialisation du mot de passe";

		// Envoi
		sendMail(toAddress, subject, TemplateEnum.RESET_PASSWORD, templateVars);
		LOGGER.info(LoggingUtil.END);
	}

	@Async
	public void confirmationValidation(Map<String, String> templateVars, InternetAddress toAddress) throws SkillsmatesMailException {
		LOGGER.info(LoggingUtil.START + " [templateVars=" + templateVars + ", toAddress=" + toAddress + "]");

		// Préparation objet du mail
		String subject = "Skillsmates® : Compte Skillsmates activé";

		// Envoi
		sendMail(toAddress, subject, TemplateEnum.CONFIRMATION_VALIDATION, templateVars);

		LOGGER.info(LoggingUtil.END);
	}

	@Async
	public void sendNotification(Map<String, String> templateVars, InternetAddress toAddress, TemplateEnum templateEnum) throws SkillsmatesMailException {
		LOGGER.info(LoggingUtil.START + " [templateVars=" + templateVars + ", toAddress=" + toAddress + "]");

		// Préparation objet du mail
		String subject = "Skillsmates® : Notification";

		// Envoi
		sendMail(toAddress, subject, templateEnum, templateVars);

		LOGGER.info(LoggingUtil.END);
	}

}
