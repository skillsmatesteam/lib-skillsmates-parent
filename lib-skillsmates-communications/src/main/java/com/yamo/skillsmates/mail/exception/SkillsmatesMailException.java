package com.yamo.skillsmates.mail.exception;

/**
 * Exception d'envoi de mail
 */
public class SkillsmatesMailException extends Exception {
  
  /** Separateur */  
  private static String SEP = " : ";
  
  /***/
  private static final long serialVersionUID = 4129963451956398300L;

  /**
   * Constructeur par défaut
   */
  public SkillsmatesMailException() {
    super();
  }
  
  /**
   * Constructeur avec un message
   * 
   * @param message le message de l'exception
   */
  public SkillsmatesMailException(String message) {
    super(SkillsmatesMailException.class.getName() + SEP + message);
  }
  
  /**
   * Constructeur avec un message et une cause
   * 
   * @param message le message de l'exception 
   * @param e la cause de l'exception
   */
  public SkillsmatesMailException(String message, Throwable e) {
    super(SkillsmatesMailException.class.getName() + SEP + message, e);
  }
}
