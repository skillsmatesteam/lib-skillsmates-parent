package com.yamo.skillsmates.mail.util;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.dto.account.AccountDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Pattern;


public class PasswordUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PasswordUtil.class);

	private static final Pattern PASSWORD_LENGTH_CHARACTER = Pattern.compile("^.{8,16}$");
	private static final Pattern PASSWORD_UPPERCASE_CHARACTER = Pattern.compile(".*[A-Z].*");
	private static final Pattern PASSWORD_LOWERCASE_CHARACTER = Pattern.compile(".*[a-z].*");
	private static final Pattern PASSWORD_DIGIT_CHARACTER_OR_SPECIAL_CHARACTER = Pattern.compile("(.*\\d.*)|(.*[!&%$].*)");

	public static void PasswordValidator(String password, AccountDto acountDto) throws GenericException {

		if(!PASSWORD_LENGTH_CHARACTER.matcher(password).matches()){

			throw new GenericException("Password must contain between 8 and 16 characters");
		}
		if(!PASSWORD_DIGIT_CHARACTER_OR_SPECIAL_CHARACTER.matcher(password).matches()){

			throw new GenericException("Password must contain at least 1 number or a special character: !&%$");
		}
		if(password.toLowerCase().contains(acountDto.getLastname().toLowerCase()) || password.toLowerCase().contains(acountDto.getFirstname().toLowerCase()) || password.toLowerCase().contains(acountDto.getEmail().toLowerCase())){

			throw new GenericException("Password must not contain your name or email");
		}
		if(!PASSWORD_UPPERCASE_CHARACTER.matcher(password).matches()){

			throw new GenericException("Password must contain at least one uppercase letter");
		}
		if(!PASSWORD_LOWERCASE_CHARACTER.matcher(password).matches()){

			throw new GenericException("Password must contain at least one lowercase letter");
		}

	}

}
