package com.yamo.skillsmates.mail.helper;

import com.yamo.skillsmates.common.logging.LoggingUtil;
import com.yamo.skillsmates.mail.enums.ImageEnum;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;
import java.util.Map;

/**
 * 
 */
public class MailHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(MailHelper.class);

	private TemplateEngine templateEngine;

	private MimeMessage mimeMessage;

	private MimeMessageHelper mimeMessageHelper;

	/**
	 * @param templateEngine Le moteur
	 * @param mimeMessage L'objet message
	 * @param multipart True si le message est multipart
	 * @param encoding L'encoding
	 */
	public MailHelper(TemplateEngine templateEngine, MimeMessage mimeMessage, boolean multipart, String encoding) {
		try {
			this.templateEngine = templateEngine;
			this.mimeMessage = mimeMessage;
			this.mimeMessageHelper = new MimeMessageHelper(this.mimeMessage, multipart, encoding);
		} catch (MessagingException mex) {
			LOGGER.error(mex.getMessage(), mex);
		}
	}

	/**
	 * Construction d'un nouveau context
	 * 
	 * @param templateVars
	 *            variables du template
	 * @return {@link Context}
	 */
	public Context prepareContext(Map<String, ?> templateVars) {
		final Context context = new Context(Locale.FRENCH);
		context.setVariables((Map<String, Object>) templateVars);
		return context;
	}

	/**
	 * Ajoute une image inline dans le contenu de l'email
	 * 
	 * @param imgEnum
	 *            L'image à inclure
	 * @param contentType
	 *            le content type de l'image
	 */
	public void addInlineImage(ImageEnum imgEnum, String contentType) {
		boolean infoEnabled = LOGGER.isInfoEnabled();
		if(infoEnabled) LOGGER.info(LoggingUtil.START +
				" [imgEnum=" + imgEnum + 
				", contentType=" + contentType + "]");
		
		try {
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			IOUtils.copy(this.getClass().getResourceAsStream(imgEnum.getValue()), output);
			InputStreamSource imageSource = new ByteArrayResource(output.toByteArray());
			getMimeMessageHelper().addInline(getRessourceImageName(imgEnum), imageSource, contentType);
		} catch (IOException ioe) {
			LOGGER.error(ioe.getMessage(), ioe);
		} catch (MessagingException me) {
			LOGGER.error(me.getMessage(), me);
		}
		
		if(infoEnabled) LOGGER.info(LoggingUtil.END);
	}

	/**
	 * Ajoute une image inline dans le contenu de l'email
	 * @param contentId L'identifiant dans le template de l'image.
	 * @param urlPath Le chemin de l'image.
	 */
	public void addInlineImage(String contentId, String urlPath) {
		boolean infoEnabled = LOGGER.isInfoEnabled();
		if(infoEnabled) LOGGER.info(LoggingUtil.START +
				" [contentId=" + contentId + 
				", urlPath=" + urlPath + "]");
		
		try {
			URL url = new URL(urlPath);
			URLConnection uc = url.openConnection();
			String contentType = uc.getContentType();
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			IOUtils.copy(new BufferedInputStream(uc.getInputStream()), output);
			InputStreamSource imageSource = new ByteArrayResource(output.toByteArray());
			getMimeMessageHelper().addInline(contentId, imageSource, contentType);
		} catch (IOException | MessagingException e) {
			LOGGER.error(e.getMessage(), e);
		}
		
		if(infoEnabled) LOGGER.info(LoggingUtil.END);
	}

	/**
	 * @return the mimeMessagehelper
	 */
	public MimeMessageHelper getMimeMessageHelper() {
		return this.mimeMessageHelper;
	}

	/**
	 * 
	 * @param template Le modèle de l'email
	 * @param context Conteneur des propriétés et valeurs utilisées par Thymeleaf
	 * @return Le HTML généré
	 */
	public String getHtmlContent(String template, Context context) {
		return this.templateEngine.process(template, context);
	}

	/**
	 * Retourne le nom d'une ressource image
	 * 
	 * @param imageEnum
	 *            {@link ImageEnum}
	 * @return Le nom de l'image
	 */
	public String getRessourceImageName(ImageEnum imageEnum) {
		return this.getClass().getResource(imageEnum.getValue()).getFile();
	}

	/**
	 * @return the mimeMessage
	 */
	public MimeMessage getMimeMessage() {
		return this.mimeMessage;
	}

}
