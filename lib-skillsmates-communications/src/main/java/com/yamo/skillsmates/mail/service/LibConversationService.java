package com.yamo.skillsmates.mail.service;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.communication.Conversation;
import com.yamo.skillsmates.models.communication.Message;
import com.yamo.skillsmates.repositories.communication.ConversationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.TreeMap;

@Service
@PropertySource(value = { "classpath:application.properties" })
public class LibConversationService {

    @Autowired
    private ConversationRepository conversationRepository;

    public Conversation saveConversation(String idServer) throws GenericException {
        Optional<Conversation> conversation  = conversationRepository.findByIdServer(idServer);
        if(!conversation.isPresent()){
            Conversation newConversation = new Conversation();
            newConversation.setName("");
            newConversation.setIdServer(idServer);
            return conversationRepository.save(newConversation);
        }
        return conversation.get();
    }

    public Conversation findConversationByIdServer(String idServer) throws GenericException{
        return conversationRepository.findByIdServer(idServer).orElse(null);
    }

    /**
     * Creation de l'id de la conversation avec le message
     * @param message: message
     * @return conversation id
     */
    public String getConversationId(Message message) {
        return getConversationId(message.getEmitterAccount().getIdServer(), message.getReceiverAccount().getIdServer());
    }

    /**
     * generate conversation id
     * @param emitterId: emitter id
     * @param receiverId: receiver id
     * @return conversation id
     */
    private String generateConversationId(String emitterId, String receiverId){
        TreeMap<String, String> map = new TreeMap<>();
        map.put(emitterId,emitterId);
        map.put(receiverId,receiverId);
        return String.join("", map.values());
    }

    /**
     * create conversation id with ids of accounts involved
     * @param emitterId: emitter id
     * @param receiverId: receiver id
     * @return conversation id
     */
    public String getConversationId(String emitterId, String receiverId){
        String conversationId = generateConversationId(receiverId, emitterId);
        Optional<Conversation> conversation = conversationRepository.findByIdServer(conversationId);
        if (!conversation.isPresent()){
            conversationId = generateConversationId(emitterId, receiverId);
            conversation = conversationRepository.findByIdServer(conversationId);
        }
        return conversation.isPresent() ? conversation.get().getIdServer() : conversationId;
    }

}
