package com.yamo.skillsmates.mail.service;

import com.yamo.skillsmates.common.logging.LoggingUtil;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.StopWatch;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Base des services de mail
 *
 * @param <T>
 *            Le type de l'objet utilisé pour générer le contenu de l'email
 */
public abstract class AbstractMailService<T> {
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractMailService.class);

	@Value("#{new javax.mail.internet.InternetAddress('${mail.address.from}')}")
	private InternetAddress from;
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private TemplateEngine templateEngine;

	/**
	 * Envoi un email à l'adresse spécifiée avec le sujet et les données de contenu spécifiés.
	 * 
	 * @param to
	 *            L'adresse cible
	 * @param sujet
	 *            Le sujet de l'email
	 * @param content
	 *            L'objet avec les données utilisées pour générer le contenu.
	 * @throws MessagingException
	 *             Problème d'email
	 * @throws IOException
	 *             Un problème avec un fichier ressource
	 */
	public void send(InternetAddress to, String sujet, T content) throws MessagingException, IOException {

		LOGGER.info(LoggingUtil.START + " [to=" + to.toString() + ", sujet=" + sujet + ", content=" + content);

		StopWatch stopwatch = new StopWatch("com.yamo.skillsmates.mail.service.AbstractMailService.send(InternetAddress, String, T)");

		stopwatch.start("init");
		MimeMessageHelper messageHelper = new MimeMessageHelper(mailSender.createMimeMessage(), true, "UTF-8");
		messageHelper.setSubject(sujet);
		messageHelper.setFrom(from);
		messageHelper.setTo(to);
		stopwatch.stop();

		stopwatch.start("prepareContext");
		Context ctx = makeContext(content);
		stopwatch.stop();

		stopwatch.start("templateEngine.process");
		String html = this.templateEngine.process(getTemplateName(), ctx);
		stopwatch.stop();

		messageHelper.setText(html, true);

		stopwatch.start("addInlineResources");
		this.addInlineResources(messageHelper, content);
		stopwatch.stop();

		stopwatch.start("mailSender.send");
		mailSender.send(messageHelper.getMimeMessage());
		stopwatch.stop();

		LOGGER.info(LoggingUtil.END);
	}

	/**
	 * Prépare le contexte a passer au template Thymeleaf.
	 * 
	 * @param content
	 *            L'objet contenant les donnees.
	 * @return Context Le context a preparer
	 */
	protected abstract Context makeContext(T content);

	/**
	 * Rajoute les ressources à emabrquer directement dans l'email
	 * 
	 * @param messageHelper
	 *            Message helper
	 * @param content
	 *            L'objet contenant les données.
	 * @throws IOException
	 *             un problème avec une fichier ressource
	 * @throws MessagingException
	 *             un problème email
	 */
	protected abstract void addInlineResources(MimeMessageHelper messageHelper, T content) throws IOException, MessagingException;

	protected void addInlineImage(MimeMessageHelper messageHelper, String resourcePath, String contentId) throws IOException, MessagingException {
		boolean infoEnabled = LOGGER.isInfoEnabled();
		if (infoEnabled) LOGGER.info(LoggingUtil.START + " [resourcePath=" + resourcePath + ", contentId=" + contentId + "]");

		String contentType = "image/" + resourcePath.substring(resourcePath.lastIndexOf(".") + 1);

		ByteArrayOutputStream output = new ByteArrayOutputStream();
		IOUtils.copy(this.getClass().getResourceAsStream(resourcePath), output);
		InputStreamSource imageSource = new ByteArrayResource(output.toByteArray());
		messageHelper.addInline(contentId, imageSource, contentType);

		if (infoEnabled) LOGGER.info(LoggingUtil.END);
	}

	protected void addInlineImage(MimeMessageHelper messageHelper, URL url, String contentId) throws IOException, MessagingException {
		boolean infoEnabled = LOGGER.isInfoEnabled();
		if (infoEnabled) LOGGER.info(LoggingUtil.START + " [contentId=" + contentId + ", url=" + url + "]");

		URLConnection uc = url.openConnection();
		String contentType = uc.getContentType();
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		IOUtils.copy(new BufferedInputStream(uc.getInputStream()), output);
		InputStreamSource imageSource = new ByteArrayResource(output.toByteArray());
		messageHelper.addInline(contentId, imageSource, contentType);

		if (infoEnabled) LOGGER.info(LoggingUtil.END);
	}

	/**
	 * @return L'adresse source
	 */
	public InternetAddress getFrom() {
		return from;
	}

	/**
	 * @param from
	 *            L'adresse source
	 */
	public void setFrom(InternetAddress from) {
		this.from = from;
	}

	/**
	 * @return the templateEngine
	 */
	public TemplateEngine getTemplateEngine() {
		return templateEngine;
	}

	/**
	 * @param templateEngine
	 *            the templateEngine to set
	 */
	public void setTemplateEngine(TemplateEngine templateEngine) {
		this.templateEngine = templateEngine;
	}

	protected abstract String getTemplateName();
}
