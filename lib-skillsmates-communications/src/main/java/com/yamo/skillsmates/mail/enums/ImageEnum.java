package com.yamo.skillsmates.mail.enums;

/**
 *
 */
public enum ImageEnum {
  /** Image d'entete */
  IMG_ENTETE("/static/images/entete.png"), 
  /** Image de signature */
  IMG_SIGNATURE("/static/images/signature.png"),
  /** Image de footer */
  IMG_FOOTER("/static/images/footer.png"),
  /** Image logo */
  IMG_LOGO("/static/images/logo.png"),

  /** Image facebook blanc */
  IMG_FACEBOOK_BLANC("/static/images/facebook-blanc.svg"),
  /** Image Instagram-blanc */
  IMG_INSTAGRAM_BLANC("/static/images/instagram-blanc.svg"),
  /** Image tiktok blanc */
  IMG_TIKTOK_BLANC("/static/images/tiktok-blanc.svg"),
  /** Image snapchat blanc */
  IMG_SNAPCHAT_BLANC("/static/images/snapchat-blanc.svg"),
  /** Image twitter blanc */
  IMG_TWITTER_BLANC("/static/images/twitter-blanc.svg");

  /***/
  private final String value;

  /**
   * @param value
   */
  private ImageEnum(String value) {
    this.value = value;
  }

  /**
   * @return the value
   */
  public String getValue() {
    return value;
  }

}
