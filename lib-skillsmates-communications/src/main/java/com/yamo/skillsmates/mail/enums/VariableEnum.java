package com.yamo.skillsmates.mail.enums;

/**
 * Enumération des variable de template
 */
public enum VariableEnum {
	/** Icon facebook */
	VAR_FACEBOOK_LINK("FACEBOOK_LINK"),
	/** Icon instagram */
	VAR_INSTAGRAM_LINK("INSTAGRAM_LINK"),
	/** Icon twitter */
	VAR_TWITTER_LINK("TWITTER_LINK"),
	VAR_SNAPCHAT_LINK("SNAPCHAT_LINK"),
	VAR_TIKTOK_LINK("TIKTOK_LINK"),
	VAR_BASE_URL("BASE_URL"),
	VAR_ACCOUNT_NAME("ACCOUNT_NAME"),
	VAR_HOST_LINK("HOST_LINK"),
	VAR_ACTIVATE_LINK("ACTIVATE_LINK"),
	VAR_EMAIL("EMAIL_SKILLSMATES"),
	VAR_WEBSITE("WEBSITE_SKILLSMATES"),
	VAR_ADDRESS("ADDRESS_SKILLSMATES"),
	/*VAR_VIEW_PROFILE_OFFLINE("VIEW_PROFILE_OFFLINE"),
	VAR_URL_PROFILE_IMAGE("URL_PROFILE_IMAGE"),
	VAR_FIRST_NAME("FIRST_NAME"),
	VAR_LAST_NAME("LAST_NAME"),
	VAR_CITY("CITY"),
	VAR_COUNTRY("COUNTRY"),
	VAR_CURRENT_JOB_TITLE("CURRENT_JOB_TITLE"),
	VAR_CURRENT_ESTABLISHMENT_NAME("CURRENT_ESTABLISHMENT_NAME"),*/

	/** Signature */
	VAR_STAT_SIGNATURE_IMG("SIGNATURE"),
	/** Entete */
	VAR_STAT_ENTETE_IMG("ENTETE");

	/***/
	private final String variable;

	/**
	 * @param name
	 */
	private VariableEnum(String name) {
		this.variable = name;
	}

	/**
	 * @return the fieldName
	 */
	public String getVariable() {
		return variable;
	}
}
