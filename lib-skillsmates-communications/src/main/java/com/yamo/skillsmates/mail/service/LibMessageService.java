package com.yamo.skillsmates.mail.service;

import com.yamo.skillsmates.common.enumeration.TypeMultimedia;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.media.helpers.AwsAmazonHelper;
import com.yamo.skillsmates.media.helpers.MetadataHelper;
import com.yamo.skillsmates.media.helpers.MultimediaHelper;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.communication.Conversation;
import com.yamo.skillsmates.models.communication.Message;
import com.yamo.skillsmates.models.multimedia.Metadata;
import com.yamo.skillsmates.models.multimedia.Multimedia;
import com.yamo.skillsmates.repositories.account.AccountRepository;
import com.yamo.skillsmates.repositories.communication.MessageRepository;
import com.yamo.skillsmates.repositories.multimedia.MetadataRepository;
import com.yamo.skillsmates.repositories.multimedia.MultimediaRepository;
import org.apache.commons.lang3.StringUtils;
import org.hornetq.utils.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@PropertySource(value = { "classpath:application.properties" })
public class LibMessageService {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private MultimediaRepository multimediaRepository;
    @Autowired
    private MultimediaHelper multimediaHelper;
    @Autowired
    private MetadataRepository metadataRepository;

    @Value("${media.directory.root}")
    protected String rootDirectory;
    @Value("${ms.skillsmates.media.aws.accessKey}")
    protected String awsAccessKey;
    @Value("${ms.skillsmates.media.aws.secretKey}")
    protected String secretKey;
    @Value("${ms.skillsmates.media.aws.s3bucket.name}")
    protected String awsS3Bucket;

    public Message saveMessage(Message message) throws GenericException {
        message.setIdServer(String.valueOf(System.nanoTime()));
        message.setActive(false); //to be active after reading
        message = messageRepository.save(message);
        return message;
    }

    public Message findMessageById(String id) throws GenericException{
        return messageRepository.findByIdServer(id).orElse(null);
    }

    public Message saveMessageMedia(Message message, MultipartFile multipartFile) throws GenericException {
        try {
            Set<Multimedia> multimediaSet = new HashSet<>();
            if (multipartFile != null) {
                if (!multimediaHelper.validateFileMimetype(multipartFile)){
                    throw new GenericException("This kind of document is not allowed");
                }

                AwsAmazonHelper awsAmazonHelper = new AwsAmazonHelper(awsAccessKey, secretKey, awsS3Bucket, rootDirectory);
                Multimedia multimedia = new Multimedia();
                multimedia.setIdServer(String.valueOf(System.nanoTime()));
                multimedia.setAccount(message.getEmitterAccount());
                multimedia.setName(StringUtils.stripAccents(multipartFile.getOriginalFilename()));
                multimedia.setExtension(multimediaHelper.getExtension(multipartFile));
                multimedia.setMimeType(multipartFile.getContentType());
                multimedia.setChecksum(multimediaHelper.computeChecksum(multipartFile.getInputStream()));
                multimedia.setDirectory(message.getEmitterAccount().getIdServer());
                multimedia.setType(getTypeMultimedia(multimedia).name());

                awsAmazonHelper.upload(multimedia, multipartFile.getInputStream());
                multimediaSet.add(multimedia);

            } else if (!CollectionUtils.isEmpty(message.getMultimedia())){ /* Gestion des urls des videos ou documents*/
                for (Multimedia multimedia: message.getMultimedia()) {
                    if (StringUtils.isNotBlank(multimedia.getUrl())) {
                        multimedia.setAccount(message.getEmitterAccount());
                        multimedia.setIdServer(String.valueOf(System.nanoTime()));

                        String html = MetadataHelper.getUrlContents(multimedia.getUrl());
                        Metadata metadata = GenericMapper.INSTANCE.asEntity(MetadataHelper.parseHtml(html, multimedia.getUrl()));
                        metadata.setIdServer(String.valueOf(System.nanoTime()));
                        multimedia.setMetadata(metadataRepository.save(metadata));
                        multimediaSet.add(multimedia);
                    }
                }

            }

            message.setMultimedia(multimediaRepository.saveAll(multimediaSet));
            message.setIdServer(String.valueOf(System.nanoTime()));
            message.setActive(false); //to be active after reading
            message.setContent("");

            return messageRepository.save(message);
        }catch (IOException | JSONException e){
            throw new GenericException("Error saving document message");
        }
    }

    public List<Message> findMessagesByConversation(Conversation conversation, Pageable pageable){
        return messageRepository.findByConversationAndDeletedFalse(conversation, pageable).orElse(new ArrayList<>());
    }

    public Long countUnreadMessagesByConversation(Conversation conversation){
        return messageRepository.countAllByActiveFalseAndDeletedFalseAndConversation(conversation);
    }

    public Long countUnreadMessagesByReceiver(Account receiver){
        return messageRepository.countAllByActiveFalseAndDeletedFalseAndReceiverAccount(receiver);
    }

    public List<Message> findUnreadMessagesByConversation(Conversation conversation){
        return messageRepository.findAllByActiveFalseAndDeletedFalseAndConversation(conversation).orElse(new ArrayList<>());
    }

    private TypeMultimedia getTypeMultimedia(Multimedia multimedia){
        if (multimedia.getMimeType().startsWith("image")){
            return TypeMultimedia.IMAGE;
        }else if (multimedia.getMimeType().startsWith("application/pdf")){
            return TypeMultimedia.DOCUMENT;
        }else if (multimedia.getMimeType().startsWith("audio")){
            return TypeMultimedia.AUDIO;
        }else if (multimedia.getMimeType().startsWith("video")){
            return TypeMultimedia.VIDEO;
        }else {
            return TypeMultimedia.DOCUMENT;
        }
    }
}
