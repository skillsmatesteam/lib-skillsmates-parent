package com.yamo.skillsmates.models.notifications;

import com.yamo.skillsmates.models.BaseActiveModel;
import com.yamo.skillsmates.models.account.Account;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = Notification.TABLE_NAME)
public class Notification extends BaseActiveModel {
    public static final String TABLE_NAME = "notification";
    public static final String NOTIFICATION_TYPE_COLUMN = "notificationType";
    public static final String TITLE_COLUMN = "title";
    public static final String EMITTER_ACCOUNT_COLUMN = "emitterAccount";
    public static final String RECEIVER_ACCOUNT_COLUMN = "receiverAccount";

    @Column(name = TITLE_COLUMN, nullable = false)
    private String title;

    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name = NOTIFICATION_TYPE_COLUMN)
    private NotificationType notificationType;

    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name = EMITTER_ACCOUNT_COLUMN)
    private Account emitterAccount;

    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name = RECEIVER_ACCOUNT_COLUMN)
    private Account receiverAccount;

    @OneToOne(mappedBy=TABLE_NAME)  // référence la relation dans la classe NotificationMap
    private NotificationMap notificationMap ;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
