package com.yamo.skillsmates.models.account.config;

import com.yamo.skillsmates.models.BaseModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = TeachingAreaSet.TABLE_NAME)
public class TeachingAreaSet extends BaseModel {
    public static final String TABLE_NAME = "teachingAreaSet";
    public static final String LABEL_COLUMN = "label";
    public static final String DESCRIPTION_COLUMN = "description";
    public static final String EDUCATION_COLUMN = "education";

    @Column(name = LABEL_COLUMN, nullable = false)
    private String label;

    @Column(name = DESCRIPTION_COLUMN)
    private String description;

    @ManyToOne
    @JoinColumn(name=EDUCATION_COLUMN)
    private Education education;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
