package com.yamo.skillsmates.models.communication;

import com.yamo.skillsmates.models.BaseActiveModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = Conversation.TABLE_NAME)
public class Conversation extends BaseActiveModel {
    public static final String TABLE_NAME = "conversation";
    public static final String NAME_COLUMN = "name";

    @Column(name = NAME_COLUMN, nullable = false)
    private String name;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
