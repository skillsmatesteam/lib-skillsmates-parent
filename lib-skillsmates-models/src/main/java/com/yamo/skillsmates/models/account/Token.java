package com.yamo.skillsmates.models.account;

import com.yamo.skillsmates.models.BaseModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = Token.TABLE_NAME)
public class Token extends BaseModel {
    public static final String TABLE_NAME = "token";
    public static final String ACCOUNT_COLUMN = "account";
    public static final String TOKEN_COLUMN = "token";
    public static final String CREATED_AT_COLUMN = "createdAt";

    @Column(name = TOKEN_COLUMN, columnDefinition = "TEXT", nullable = false, unique = true)
    private String token;

    @ManyToOne
    @JoinColumn(name=ACCOUNT_COLUMN)
    private Account account;

    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    @Column(name = CREATED_AT_COLUMN, updatable = true, nullable = false)
    protected Date createdAt;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
