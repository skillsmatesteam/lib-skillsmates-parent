package com.yamo.skillsmates.models.account.config;

import com.yamo.skillsmates.models.BaseModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = Education.TABLE_NAME)
public class Education extends BaseModel {
    public static final String TABLE_NAME = "education";
    public static final String CODE_COLUMN = "code";
    public static final String LABEL_COLUMN = "label";
    public static final String DESCRIPTION_COLUMN = "description";

    @Column(name = CODE_COLUMN, nullable = false)
    private String code;

    @Column(name = LABEL_COLUMN, nullable = false)
    private String label;

    @Column(name = DESCRIPTION_COLUMN)
    private String description;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
