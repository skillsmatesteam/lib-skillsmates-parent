package com.yamo.skillsmates.models.multimedia.config;

import com.yamo.skillsmates.models.BaseModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = InfosMediaType.TABLE_NAME)
public class InfosMediaType extends BaseModel {
    public static final String TABLE_NAME = "infosMediaType";
    public static final String COLOR_COLUMN = "color";
    public static final String URL_COLUMN = "url";
    public static final String POSITION_COLUMN = "position";

    @Column(name = COLOR_COLUMN, nullable = false)
    private String color;

    @Column(name = URL_COLUMN, nullable = false)
    private String url;

    @Column(name = POSITION_COLUMN, nullable = false)
    private byte position;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
