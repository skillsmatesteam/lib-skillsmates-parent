package com.yamo.skillsmates.models.post;

import com.yamo.skillsmates.models.BaseActiveModel;
import com.yamo.skillsmates.models.account.Account;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = SocialInteraction.TABLE_NAME)
public class SocialInteraction extends BaseActiveModel {
    public static final String TABLE_NAME = "socialInteraction";
    public static final String CONTENT_COLUMN = "content";
    public static final String EMITTER_ACCOUNT_COLUMN = "emitterAccount"; // account who initiated/emitted the social interaction
    public static final String RECEIVER_ACCOUNT_COLUMN = "receiverAccount"; // account who receive the social interaction
    public static final String SOCIAL_INTERACTION_TYPE_COLUMN = "socialInteractionType";

    @Column(name = CONTENT_COLUMN)
    @Length(max=2000)
    private String content;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name=EMITTER_ACCOUNT_COLUMN)
    private Account emitterAccount;

    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name = RECEIVER_ACCOUNT_COLUMN)
    private Account receiverAccount;

    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name = SOCIAL_INTERACTION_TYPE_COLUMN)
    private SocialInteractionType socialInteractionType;

    @OneToOne(mappedBy=TABLE_NAME)  // référence la relation dans la classe SocialInteractionMap
    private SocialInteractionMap socialInteractionMap ;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
