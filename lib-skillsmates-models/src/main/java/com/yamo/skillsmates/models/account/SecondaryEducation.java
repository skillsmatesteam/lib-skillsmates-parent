package com.yamo.skillsmates.models.account;

import com.yamo.skillsmates.models.account.config.Diploma;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = SecondaryEducation.TABLE_NAME)
public class SecondaryEducation extends SchoolPath {
    public static final String TABLE_NAME = "secondaryEducation";
    public static final String PREPARED_DIPLOMA_COLUMN = "preparedDiploma";
    public static final String ANOTHER_PREPARED_DIPLOMA_COLUMN = "anotherPreparedDiploma";

    @ManyToOne
    @JoinColumn(name=PREPARED_DIPLOMA_COLUMN)
    private Diploma preparedDiploma;

    @Column(name = ANOTHER_PREPARED_DIPLOMA_COLUMN)
    private String anotherPreparedDiploma;
}
