package com.yamo.skillsmates.models.account;

import com.yamo.skillsmates.common.enumeration.Gender;
import com.yamo.skillsmates.common.enumeration.Status;
import com.yamo.skillsmates.models.BaseActiveModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = Account.TABLE_NAME)
public class Account extends BaseActiveModel {
    public static final String TABLE_NAME = "account";
    public static final String FIRSTNAME_COLUMN = "firstname";
    public static final String LASTNAME_COLUMN = "lastname";
    public static final String ADDRESS_COLUMN = "address";
    public static final String EMAIL_COLUMN = "email";
    public static final String PHONE_NUMBER_COLUMN = "phoneNumber";
    public static final String BIRTHDATE_COLUMN = "birthdate";
    public static final String PASSWORD_COLUMN = "password";
    public static final String DESCRIPTION_COLUMN = "description";
    public static final String GENDER_COLUMN = "gender";
    public static final String BIOGRAPHY_COLUMN = "biography";
    public static final String CITY_COLUMN = "city";
    public static final String COUNTRY_COLUMN = "country";
    public static final String STATUS_COLUMN = "status";
    public static final String PROFILE_PICTURE_COLUMN = "profilePicture";
    public static final String ACCOUNT_ID_COLUMN = "account_id";
    public static final String CONNECTED_COLUMN = "connected";
    public static final String ROLE_COLUMN = "role";
    public static final String CURRENT_ESTABLISHMENT_NAME_COLUMN = "currentEstablishmentName";
    public static final String CURRENT_JOB_TITLE_COLUMN = "currentJobTitle";

    @Column(name = FIRSTNAME_COLUMN, nullable = false)
    private String firstname;

    @Column(name = LASTNAME_COLUMN, nullable = false)
    private String lastname;

    @Column(name = ADDRESS_COLUMN)
    private String address;

    @Column(name = EMAIL_COLUMN, nullable = false, unique = true)
    private String email;

    @Column(name = PHONE_NUMBER_COLUMN)
    private String phoneNumber;

    @Column(name = BIRTHDATE_COLUMN)
    private Date birthdate;

    @Column(name = PASSWORD_COLUMN, nullable = false)
    private String password;

    @Column(name = DESCRIPTION_COLUMN)
    private String description;

    @JoinColumn(name=GENDER_COLUMN)
    @Enumerated(EnumType.ORDINAL)
    private Gender gender;

    @Column(name = BIOGRAPHY_COLUMN)
    @Length(max = 10000)
    private String biography;

    @Column(name = CITY_COLUMN)
    private String city;

    @Column(name=COUNTRY_COLUMN)
    private String country;

    @Column(name=STATUS_COLUMN)
    @Enumerated(EnumType.ORDINAL)
    private Status status;

    @Column(name = PROFILE_PICTURE_COLUMN)
    private String profilePicture;

    @Column(name = CONNECTED_COLUMN, columnDefinition = "boolean default false")
    private boolean connected;

    @Column(name = ROLE_COLUMN, columnDefinition = "role of account, default is USER")
    private String role;

    @Column(name = CURRENT_ESTABLISHMENT_NAME_COLUMN)
    private String currentEstablishmentName;

    @Column(name = CURRENT_JOB_TITLE_COLUMN)
    private String currentJobTitle;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
