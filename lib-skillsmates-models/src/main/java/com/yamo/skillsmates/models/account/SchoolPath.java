package com.yamo.skillsmates.models.account;

import com.yamo.skillsmates.models.account.config.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = SchoolPath.TABLE_NAME)
@Inheritance(strategy= InheritanceType.TABLE_PER_CLASS)
public abstract class SchoolPath extends Cursus {
    public static final String TABLE_NAME = "schoolPath";
    public static final String STUDY_LEVEL_COLUMN = "studyLevel";
    public static final String ESTABLISHMENT_TYPE_COLUMN = "establishmentType";
    public static final String ANOTHER_STUDY_LEVEL_COLUMN = "anotherStudyLevel";
    public static final String ANOTHER_ESTABLISHMENT_TYPE_COLUMN = "anotherEstablishmentType";

    @ManyToOne
    @JoinColumn(name=ESTABLISHMENT_TYPE_COLUMN)
    private EstablishmentType establishmentType;

    @ManyToOne
    @JoinColumn(name=STUDY_LEVEL_COLUMN)
    private StudyLevel studyLevel;

    @Column(name = ANOTHER_STUDY_LEVEL_COLUMN)
    private String anotherStudyLevel;

    @Column(name = ANOTHER_ESTABLISHMENT_TYPE_COLUMN)
    private String anotherEstablishmentType;
}
