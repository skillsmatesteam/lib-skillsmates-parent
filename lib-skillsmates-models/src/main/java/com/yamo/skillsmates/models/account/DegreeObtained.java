package com.yamo.skillsmates.models.account;

import com.yamo.skillsmates.models.account.config.Diploma;
import com.yamo.skillsmates.models.account.config.EstablishmentType;
import com.yamo.skillsmates.models.account.config.TeachingArea;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = DegreeObtained.TABLE_NAME)
public class DegreeObtained extends Cursus {
    public static final String TABLE_NAME = "degreeObtained";
    public static final String DIPLOMA_COLUMN = "diploma";
    public static final String TEACHING_AREA_COLUMN = "teachingArea";
    public static final String ESTABLISHMENT_TYPE_COLUMN = "establishmentType";
    public static final String START_DATE_COLUMN = "startDate";
    public static final String END_DATE_COLUMN = "endDate";
    public static final String ANOTHER_DIPLOMA_COLUMN = "anotherDiploma";
    public static final String ANOTHER_ESTABLISHMENT_TYPE_COLUMN = "anotherEstablishmentType";

    @ManyToOne
    @JoinColumn(name=DIPLOMA_COLUMN)
    private Diploma diploma;

    @ManyToOne
    @JoinColumn(name=TEACHING_AREA_COLUMN)
    private TeachingArea teachingArea;

    @ManyToOne
    @JoinColumn(name=ESTABLISHMENT_TYPE_COLUMN)
    private EstablishmentType establishmentType;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = START_DATE_COLUMN, nullable = false)
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = END_DATE_COLUMN, nullable = false)
    private Date endDate;

    @Column(name = ANOTHER_DIPLOMA_COLUMN)
    private String anotherDiploma;

    @Column(name = ANOTHER_ESTABLISHMENT_TYPE_COLUMN)
    private String anotherEstablishmentType;

}
