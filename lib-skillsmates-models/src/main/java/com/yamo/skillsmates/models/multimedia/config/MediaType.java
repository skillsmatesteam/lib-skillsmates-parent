package com.yamo.skillsmates.models.multimedia.config;

import com.yamo.skillsmates.models.BaseModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = MediaType.TABLE_NAME)
public class MediaType extends BaseModel {
    public static final String TABLE_NAME = "MediaType";
    public static final String LABEL_COLUMN = "label";

    @Column(name = LABEL_COLUMN, nullable = false)
    private String label;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
