package com.yamo.skillsmates.models.account.config;

import com.yamo.skillsmates.models.BaseModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = Level.TABLE_NAME)
public class Level extends BaseModel {
    public static final String TABLE_NAME = "level";
    public static final String LABEL_COLUMN = "label";
    public static final String GRADE_COLUMN = "grade";
    public static final String ICON_COLUMN = "icon";
    public static final String MASTERED_COLUMN = "mastered";
    public static final String DESCRIPTION_COLUMN = "description";

    @Column(name = LABEL_COLUMN, nullable = false)
    private String label;

    @Column(name = GRADE_COLUMN, nullable = false)
    private int grade;

    @Column(name = ICON_COLUMN, nullable = false)
    private String icon;

    @Column(name = MASTERED_COLUMN)
    private boolean mastered;

    @Column(name = DESCRIPTION_COLUMN)
    private String description;


    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}

