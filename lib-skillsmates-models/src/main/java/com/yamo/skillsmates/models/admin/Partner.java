package com.yamo.skillsmates.models.admin;

import com.yamo.skillsmates.models.BaseActiveModel;
import com.yamo.skillsmates.models.account.config.ActivityArea;
import com.yamo.skillsmates.models.admin.config.Category;
import com.yamo.skillsmates.models.admin.config.PartnerType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = Partner.TABLE_NAME)
public class Partner extends BaseActiveModel {
    public static final String TABLE_NAME = "partner";
    public static final String NAME_COLUMN = "name";
    public static final String SIREN_COLUMN = "siren";
    public static final String PARTNER_TYPE_COLUMN = "partnerType";
    public static final String ADDRESS_COLUMN = "address";
    public static final String IS_PUBLIC_ADDRESS_COLUMN = "isPublicAddress";
    public static final String EMAIL_COLUMN = "email";
    public static final String PHONE_NUMBER_COLUMN = "phoneNumber";
    public static final String IS_PUBLIC_PHONE_NUMBER_COLUMN = "isPublicPhoneNumber";
    public static final String DESCRIPTION_COLUMN = "description";
    public static final String CITY_COLUMN = "city";
    public static final String COUNTRY_COLUMN = "country";
    public static final String PROFILE_PICTURE_COLUMN = "profilePicture";
    public static final String CATEGORY_COLUMN = "category";
    public static final String ACTIVITY_AREA_COLUMN = "activityArea";
    public static final String WEBSITE_COLUMN = "website";
    public static final String FACEBOOK_COLUMN = "facebook";
    public static final String TIKTOK_COLUMN = "tiktok";
    public static final String INSTAGRAM_COLUMN = "instagram";
    public static final String LINKEDIN_COLUMN = "linkedin";
    public static final String SNAPCHAT_COLUMN = "snapchat";

    @Column(name = NAME_COLUMN, nullable = false)
    private String name;

    @Column(name = ADDRESS_COLUMN)
    private String address;

    @Column(name = IS_PUBLIC_ADDRESS_COLUMN, columnDefinition="BOOLEAN DEFAULT true")
    private boolean isPublicAddress;

    @Column(name = IS_PUBLIC_PHONE_NUMBER_COLUMN, columnDefinition="BOOLEAN DEFAULT true")
    private boolean isPublicPhoneNumber;

    @Column(name = SIREN_COLUMN)
    private String siren;

    @Column(name = WEBSITE_COLUMN)
    private String website;

    @Column(name = FACEBOOK_COLUMN)
    private String facebook;

    @Column(name = INSTAGRAM_COLUMN)
    private String instagram;

    @Column(name = TIKTOK_COLUMN)
    private String tiktok;

    @Column(name = LINKEDIN_COLUMN)
    private String linkedin;

    @Column(name = SNAPCHAT_COLUMN)
    private String snapchat;

    @Column(name = EMAIL_COLUMN, nullable = false, unique = true)
    private String email;

    @Column(name = PHONE_NUMBER_COLUMN)
    private String phoneNumber;

    @Column(name = DESCRIPTION_COLUMN)
    private String description;

    @ManyToOne
    @JoinColumn(name=PARTNER_TYPE_COLUMN)
    private PartnerType partnerType;


    @Column(name = CITY_COLUMN)
    private String city;

    @Column(name=COUNTRY_COLUMN)
    private String country;

    @Column(name = PROFILE_PICTURE_COLUMN)
    private String profilePicture;

    @ManyToOne
    @JoinColumn(name=CATEGORY_COLUMN)
    private Category category;

    @ManyToOne
    @JoinColumn(name=ACTIVITY_AREA_COLUMN)
    private ActivityArea activityArea;

    @OneToMany(mappedBy = TABLE_NAME, fetch = FetchType.LAZY)
    private Collection<Activity> activities;


    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
