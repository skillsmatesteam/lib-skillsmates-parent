package com.yamo.skillsmates.models.post;

import com.yamo.skillsmates.models.BaseActiveModel;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.multimedia.Multimedia;
import com.yamo.skillsmates.models.multimedia.config.MediaSubtype;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = Post.TABLE_NAME)
public class Post extends BaseActiveModel {
    public static final String TABLE_NAME = "post";
    public static final String TITLE_COLUMN = "title";
    public static final String KEYWORDS_COLUMN = "keywords";
    public static final String DISCIPLINE_COLUMN = "discipline";
    public static final String DESCRIPTION_COLUMN = "description";
    public static final String ACCOUNT_COLUMN = "account";
    public static final String MEDIA_SUBTYPE_COLUMN = "mediaSubtype";
    public static final String TYPE_COLUMN = "type";
    public static final String MULTIMEDIA_COLUMN = "multimedia";

    @Column(name = TITLE_COLUMN, nullable = false)
    private String title;

    @Column(name = KEYWORDS_COLUMN)
    private String keywords;

    @Column(name = DISCIPLINE_COLUMN)
    private String discipline;

    @Column(name = DESCRIPTION_COLUMN)
    @Length(max=5000)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name = ACCOUNT_COLUMN)
    private Account account;

    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name = MEDIA_SUBTYPE_COLUMN)
    private MediaSubtype mediaSubtype;

//    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
//    private List<Multimedia> multimedia = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name = MULTIMEDIA_COLUMN)
    private Multimedia multimedia;

    @Column(name = TYPE_COLUMN)
    private String type;

    @Override
    public String getLog() {
        return null;
    }
    @Override
    public String getLogDetail() {
        return null;
    }
}
