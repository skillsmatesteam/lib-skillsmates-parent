package com.yamo.skillsmates.models.notifications;

import com.yamo.skillsmates.models.BaseModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = NotificationMap.TABLE_NAME)
public class NotificationMap extends BaseModel {
    public static final String TABLE_NAME = "notificationMap";
    public static final String NOTIFICATION_COLUMN = "notification";
    public static final String ID_ELEMENT_COLUMN = "idElement";

    @Column(name = ID_ELEMENT_COLUMN, nullable = false)
    private String idElement;

    @OneToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name = NOTIFICATION_COLUMN)
    private Notification notification;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
