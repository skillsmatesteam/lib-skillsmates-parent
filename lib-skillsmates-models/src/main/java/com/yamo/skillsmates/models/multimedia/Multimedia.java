package com.yamo.skillsmates.models.multimedia;

import com.yamo.skillsmates.models.BaseActiveModel;
import com.yamo.skillsmates.models.account.Account;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = Multimedia.TABLE_NAME)
public class Multimedia extends BaseActiveModel {
    public static final String TABLE_NAME = "multimedia";
    public static final String NAME_COLUMN = "name";
    public static final String EXTENSION_COLUMN = "extension";
    public static final String MIME_TYPE_COLUMN = "mimeType";
    public static final String TYPE_COLUMN = "type";
    public static final String CHECKSUM_COLUMN = "checksum";
    public static final String DIRECTORY_COLUMN = "directory";
    public static final String ACCOUNT_COLUMN = "account";
    public static final String URL_COLUMN = "url";
    public static final String METADATA_COLUMN = "metadata";
    public static final String ORIGIN_COLUMN = "origin";

    @Column(name = NAME_COLUMN)
    private String name;

    @Column(name = EXTENSION_COLUMN)
    private String extension;

    @Column(name = MIME_TYPE_COLUMN)
    private String mimeType;

    @Column(name = TYPE_COLUMN)
    private String type;

    @Column(name = CHECKSUM_COLUMN)
    private String checksum;

    @Column(name = DIRECTORY_COLUMN)
    private String directory;

    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name = ACCOUNT_COLUMN)
    private Account account;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name = METADATA_COLUMN)
    private Metadata metadata;

    @Column(name = URL_COLUMN)
    private String url;

    @Column(name = ORIGIN_COLUMN)
    private String origin;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
