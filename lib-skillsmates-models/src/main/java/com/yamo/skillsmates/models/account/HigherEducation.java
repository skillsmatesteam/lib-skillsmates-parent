package com.yamo.skillsmates.models.account;

import com.yamo.skillsmates.models.account.config.Diploma;
import com.yamo.skillsmates.models.account.config.TeachingArea;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = HigherEducation.TABLE_NAME)
public class HigherEducation extends SchoolPath {
    public static final String TABLE_NAME = "higherEducation";
    public static final String TEACHING_AREA_COLUMN = "teachingArea";
    public static final String TARGETED_DIPLOMA_COLUMN = "targetedDiploma";
    public static final String ANOTHER_TARGETED_DIPLOMA_COLUMN = "anotherTargetedDiploma";

    @ManyToOne
    @JoinColumn(name=TEACHING_AREA_COLUMN)
    private TeachingArea teachingArea;

    @ManyToOne
    @JoinColumn(name=TARGETED_DIPLOMA_COLUMN)
    private Diploma targetedDiploma ;

    @Column(name = ANOTHER_TARGETED_DIPLOMA_COLUMN)
    private String anotherTargetedDiploma;

}

