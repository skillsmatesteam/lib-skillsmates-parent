package com.yamo.skillsmates.models.account;

import com.yamo.skillsmates.models.BaseActiveModel;
import com.yamo.skillsmates.models.account.config.ActivityArea;
import com.yamo.skillsmates.models.account.config.EstablishmentCertificationType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = Certification.TABLE_NAME)
public class Certification extends BaseActiveModel {
    public static final String TABLE_NAME = "certification";
    public static final String ACTIVITY_AREA_COLUMN = "activityArea";
    public static final String ISSUING_INSTITUTION_NAME_COLUMN = "issuingInstitutionName";
    public static final String ENTITLED_CERTIFICATION_COLUMN = "entitledCertification";
    public static final String OBTAINED_DATE_COLUMN = "obtainedDate";
    public static final String EXPIRATION_DATE_COLUMN = "expirationDate";
    public static final String ANOTHER_ACTIVITY_AREA_COLUMN = "anotherActivityArea";
    public static final String ANOTHER_ACTIVITY_SECTOR_COLUMN = "anotherActivitySector";
    public static final String ESTABLISHMENT_CERTIFICATION_TYPE_COLUMN = "establishmentCertificationType";
    public static final String ANOTHER_ESTABLISHMENT_CERTIFICATION_TYPE_COLUMN = "anotherEstablishmentCertificationType";
    public static final String IS_PERMANENT_COLUMN= "isPermanent";
    public static final String IS_TEMPORARY_COLUMN = "isTemporary";
    public static final String DESCRIPTION_COLUMN = "description";
    public static final String ACCOUNT_COLUMN = "account";

    @ManyToOne
    @JoinColumn(name=ESTABLISHMENT_CERTIFICATION_TYPE_COLUMN)
    private EstablishmentCertificationType establishmentCertificationType;

    @Column(name = ISSUING_INSTITUTION_NAME_COLUMN, nullable = false)
    private String issuingInstitutionName;

    @Column(name = ENTITLED_CERTIFICATION_COLUMN, nullable = false)
    private String entitledCertification;

    @ManyToOne
    @JoinColumn(name=ACTIVITY_AREA_COLUMN)
    private ActivityArea activityArea;

    @Column(name = IS_PERMANENT_COLUMN, columnDefinition = "tinyint(1) default 0")
    private boolean isPermanent;

    @Column(name = IS_TEMPORARY_COLUMN, columnDefinition = "tinyint(1) default 0")
    private boolean isTemporary;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = OBTAINED_DATE_COLUMN, nullable = false)
    private Date obtainedDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = EXPIRATION_DATE_COLUMN)
    private Date expirationDate;

    @Column(name = ANOTHER_ACTIVITY_AREA_COLUMN)
    private String anotherActivityArea;

    @Column(name = ANOTHER_ESTABLISHMENT_CERTIFICATION_TYPE_COLUMN)
    private String anotherEstablishmentCertificationType;

    @Column(name = DESCRIPTION_COLUMN)
    private String description;

    @ManyToOne
    @JoinColumn(name=ACCOUNT_COLUMN)
    private Account account;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
