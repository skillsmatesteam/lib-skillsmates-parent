package com.yamo.skillsmates.models.assistance;

import com.yamo.skillsmates.models.BaseActiveModel;
import com.yamo.skillsmates.models.account.Account;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = Assistance.TABLE_NAME)
public class Assistance extends BaseActiveModel {
    public static final String TABLE_NAME = "assistance";
    public static final String TOPIC_COLUMN = "topic";
    public static final String CONTENT_COLUMN = "content";
    public static final String ACCOUNT_COLUMN = "account";

    @Column(name = TOPIC_COLUMN, nullable = false)
    private String topic;

    @Column(name = CONTENT_COLUMN, nullable = false)
    private String content;

    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name=ACCOUNT_COLUMN)
    private Account account;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
