package com.yamo.skillsmates.models.account;

import com.yamo.skillsmates.models.BaseModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = Subscription.TABLE_NAME)
public class Subscription extends BaseModel {
    public static final String TABLE_NAME = "subscription";
    public static final String FOLLOWER_COLUMN = "follower";
    public static final String FOLLOWEE_COLUMN = "followee";

    @ManyToOne
    @JoinColumn(name=FOLLOWER_COLUMN, nullable = false)
    private Account follower;

    @ManyToOne
    @JoinColumn(name=FOLLOWEE_COLUMN, nullable = false)
    private Account followee;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
