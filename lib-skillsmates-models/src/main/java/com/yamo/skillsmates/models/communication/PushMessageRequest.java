package com.yamo.skillsmates.models.communication;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class PushMessageRequest {
    private String title;
    private String message;
    private String topic;
    private String token;
}
