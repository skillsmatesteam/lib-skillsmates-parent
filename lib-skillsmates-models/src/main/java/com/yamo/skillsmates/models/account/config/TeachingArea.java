package com.yamo.skillsmates.models.account.config;

import com.yamo.skillsmates.models.BaseModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = TeachingArea.TABLE_NAME)
public class TeachingArea extends BaseModel {
    public static final String TABLE_NAME = "teachingArea";
    public static final String LABEL_COLUMN = "label";
    public static final String DESCRIPTION_COLUMN = "description";
    public static final String TEACHING_AREA_GROUP_COLUMN = "teachingAreaGroup";

    @Column(name = LABEL_COLUMN, nullable = false)
    private String label;

    @Column(name = DESCRIPTION_COLUMN)
    private String description;

    @ManyToOne
    @JoinColumn(name=TEACHING_AREA_GROUP_COLUMN)
    private TeachingAreaGroup teachingAreaGroup;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
