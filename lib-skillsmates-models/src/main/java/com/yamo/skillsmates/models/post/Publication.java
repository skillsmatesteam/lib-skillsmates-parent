package com.yamo.skillsmates.models.post;

import com.yamo.skillsmates.common.logging.Loggable;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Publication.TABLE_NAME)
public class Publication implements Serializable, Loggable {

    public static final String TABLE_NAME = "publication";
    private static final String COLUMN_TYPE = "type";
    private static final String COLUMN_AUTHOR_ID = "author_id";
    private static final String COLUMN_CONTENT_ID = "content_id";
    private static final String COLUMN_DATE = "date";
    private static final String COLUMN_ID = "id";

    @Id
    @Column(name = COLUMN_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = COLUMN_DATE, updatable = false, nullable = false)
    private Date date;
    @Enumerated(EnumType.STRING)
    @Column(name = COLUMN_TYPE, nullable = false)
    private PublicationType type;
    @Column(name = COLUMN_AUTHOR_ID, nullable = false)
    private Integer authorId;
    @Column(name = COLUMN_CONTENT_ID, nullable = false)
    private Integer contentId;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
