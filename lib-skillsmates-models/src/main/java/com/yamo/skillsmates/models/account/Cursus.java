package com.yamo.skillsmates.models.account;

import com.yamo.skillsmates.models.BaseActiveModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = Cursus.TABLE_NAME)
@Inheritance(strategy= InheritanceType.TABLE_PER_CLASS)
public abstract class Cursus extends BaseActiveModel {
    public static final String TABLE_NAME = "cursus";
    public static final String SPECIALTY_COLUMN = "specialty";
    public static final String ESTABLISHMENT_NAME_COLUMN = "establishmentName";
    public static final String CITY_COLUMN = "city";
    public static final String DESCRIPTION_COLUMN = "description";
    public static final String ACCOUNT_COLUMN = "account";
    public static final String CURRENT_POSITION_COLUMN = "currentPosition";

    @Column(name = ESTABLISHMENT_NAME_COLUMN, nullable = false)
    private String establishmentName;

    @Column(name = CITY_COLUMN, nullable = false)
    private String city;

    @Column(name = DESCRIPTION_COLUMN)
    @Length(max = 5000)
    private String description;

    @Column(name = SPECIALTY_COLUMN)
    private String specialty ;

    @ManyToOne
    @JoinColumn(name=ACCOUNT_COLUMN)
    private Account account;

    @Column(name = CURRENT_POSITION_COLUMN)
    private boolean currentPosition;

    @Override
    public String getLog() {
        return null;
    }
    @Override
    public String getLogDetail() {
        return null;
    }
}
