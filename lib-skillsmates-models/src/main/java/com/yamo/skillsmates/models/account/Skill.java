package com.yamo.skillsmates.models.account;

import com.yamo.skillsmates.models.BaseActiveModel;
import com.yamo.skillsmates.models.account.config.Level;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = Skill.TABLE_NAME)
public class Skill extends BaseActiveModel {
    public static final String TABLE_NAME = "skill";
    public static final String LABEL_COLUMN = "label";
    public static final String SKILL_MASTERED_COLUMN = "skillMastered";
    public static final String DISCIPLINE_COLUMN = "discipline";
    public static final String KEYWORDS_COLUMN = "keywords";
    public static final String LEVEL_COLUMN = "level";
    public static final String ACCOUNT_COLUMN = "account";

    @Column(name = LABEL_COLUMN, nullable = false)
    private String label;

    @Column(name = SKILL_MASTERED_COLUMN)
    private boolean skillMastered;

    @Column(name=DISCIPLINE_COLUMN)
    private String discipline;

    @Column(name=KEYWORDS_COLUMN)
    @Length(max = 5000)
    private String keywords;

    @ManyToOne
    @JoinColumn(name=LEVEL_COLUMN)
    private Level level;

    @ManyToOne
    @JoinColumn(name=ACCOUNT_COLUMN)
    private Account account;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
