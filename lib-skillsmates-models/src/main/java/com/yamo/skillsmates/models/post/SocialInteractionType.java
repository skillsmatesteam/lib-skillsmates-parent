package com.yamo.skillsmates.models.post;

import com.yamo.skillsmates.models.BaseModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = SocialInteractionType.TABLE_NAME)
public class SocialInteractionType extends BaseModel {
    public static final String TABLE_NAME = "socialInteractionType";
    public static final String LABEL_COLUMN = "label";

    @Column(name = LABEL_COLUMN, nullable = false, unique = true)
    private String label;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}

