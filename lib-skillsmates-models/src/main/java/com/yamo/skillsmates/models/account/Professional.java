package com.yamo.skillsmates.models.account;

import com.yamo.skillsmates.models.account.config.ActivityArea;
import com.yamo.skillsmates.models.account.config.ActivitySector;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = Professional.TABLE_NAME)
public class Professional extends Cursus {
    public static final String TABLE_NAME = "professional";
    public static final String ACTIVITY_SECTOR_COLUMN = "activitySector";
    public static final String ACTIVITY_AREA_COLUMN = "activityArea";
    public static final String JOB_TITLE_COLUMN = "jobTitle";
    public static final String START_DATE_COLUMN = "startDate";
    public static final String END_DATE_COLUMN = "endDate";
    public static final String ANOTHER_ACTIVITY_AREA_COLUMN = "anotherActivityArea";
    public static final String ANOTHER_ACTIVITY_SECTOR_COLUMN = "anotherActivitySector";

    @ManyToOne
    @JoinColumn(name=ACTIVITY_SECTOR_COLUMN)
    private ActivitySector activitySector;

    @Column(name = JOB_TITLE_COLUMN, nullable = false)
    private String jobTitle;

    @ManyToOne
    @JoinColumn(name=ACTIVITY_AREA_COLUMN)
    private ActivityArea activityArea;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = START_DATE_COLUMN, nullable = false)
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = END_DATE_COLUMN)
    private Date endDate;

    @Column(name = ANOTHER_ACTIVITY_SECTOR_COLUMN)
    private String anotherActivitySector;

    @Column(name = ANOTHER_ACTIVITY_AREA_COLUMN)
    private String anotherActivityArea;
}
