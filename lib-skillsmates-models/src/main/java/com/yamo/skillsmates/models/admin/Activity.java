package com.yamo.skillsmates.models.admin;

import com.yamo.skillsmates.models.BaseActiveModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = Activity.TABLE_NAME)
public class Activity extends BaseActiveModel {
    public static final String TABLE_NAME = "activity";
    public static final String TITLE_COLUMN = "title";
    public static final String DESCRIPTION_COLUMN = "description";
    public static final String WEBSITE_COLUMN = "website";
    public static final String PICTURE_COLUMN = "picture";
    public static final String PARTNER_COLUMN = "partner";

    @Column(name = TITLE_COLUMN, nullable = false)
    private String title;

    @Column(name = DESCRIPTION_COLUMN)
    private String description;

    @Column(name = WEBSITE_COLUMN)
    private String website;

    @Column(name = PICTURE_COLUMN)
    private String picture;

    @ManyToOne
    @JoinColumn(name=PARTNER_COLUMN)
    private Partner partner;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}

