package com.yamo.skillsmates.models.post;

import com.yamo.skillsmates.models.BaseModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = SocialInteractionMap.TABLE_NAME)
public class SocialInteractionMap extends BaseModel {
    public static final String TABLE_NAME = "socialInteractionMap";
    public static final String SOCIAL_INTERACTION_COLUMN = "socialInteraction";
    public static final String ELEMENT_COLUMN = "element";

    @Column(name = ELEMENT_COLUMN, nullable = false)
    private String element;

    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name = SOCIAL_INTERACTION_COLUMN)
    private SocialInteraction socialInteraction;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
