package com.yamo.skillsmates.models.multimedia.config;

import com.yamo.skillsmates.models.BaseModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = MediaSubtype.TABLE_NAME)
public class MediaSubtype extends BaseModel {
    public static final String TABLE_NAME = "MediaSubtype";
    public static final String LABEL_COLUMN = "label";
    public static final String MEDIA_TYPE_COLUMN = "mediaType";

    @Column(name = LABEL_COLUMN, nullable = false)
    private String label;

    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name = MEDIA_TYPE_COLUMN)
    private MediaType mediaType;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
