package com.yamo.skillsmates.models.communication;

import com.yamo.skillsmates.models.BaseActiveModel;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.multimedia.Multimedia;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = Message.TABLE_NAME)
public class Message extends BaseActiveModel {
    public static final String TABLE_NAME = "message";
    public static final String CONTENT_COLUMN = "content";
    public static final String CONVERSATION_COLUMN = "conversation";
    public static final String EMITTER_ACCOUNT_COLUMN = "emitterAccount";
    public static final String RECEIVER_ACCOUNT_COLUMN = "receiverAccount";

    @Column(name = CONTENT_COLUMN, nullable = false)
    private String content;

    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name= CONVERSATION_COLUMN)
    private Conversation conversation;

    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name=EMITTER_ACCOUNT_COLUMN)
    private Account emitterAccount;

    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JoinColumn(name=RECEIVER_ACCOUNT_COLUMN)
    private Account receiverAccount;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Multimedia> multimedia = new ArrayList<>();

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
