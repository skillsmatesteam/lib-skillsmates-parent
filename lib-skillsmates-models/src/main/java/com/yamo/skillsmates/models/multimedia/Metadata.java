package com.yamo.skillsmates.models.multimedia;

import com.yamo.skillsmates.models.BaseActiveModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = Metadata.TABLE_NAME)
public class Metadata extends BaseActiveModel {

    public static final String TABLE_NAME = "metadata";
    public static final String TITLE_COLUMN = "title";
    public static final String DESCRIPTION_COLUMN = "description";
    public static final String IMAGE_COLUMN = "image";
    public static final String AUDIO_COLUMN = "audio";
    public static final String VIDEO_COLUMN = "video";
    public static final String CANONICAL_COLUMN = "canonical";
    public static final String AUTHOR_COLUMN = "author";
    public static final String AVAILABILITY_COLUMN = "availability";
    public static final String KEYWORDS_COLUMN = "keywords";
    public static final String URL_COLUMN = "url";

    @Column(name = TITLE_COLUMN)
    private String title;
    @Column(name = DESCRIPTION_COLUMN)
    private String description;
    @Column(name = IMAGE_COLUMN)
    private String image;
    @Column(name = CANONICAL_COLUMN)
    private String canonical;
    @Column(name = URL_COLUMN)
    private String url;
    @Column(name = AUTHOR_COLUMN)
    private String author;
    @Column(name = AVAILABILITY_COLUMN)
    private String availability;
    @Column(name = KEYWORDS_COLUMN)
    private String keywords;
    @Column(name = AUDIO_COLUMN)
    private String audio;
    @Column(name = VIDEO_COLUMN)
    private String video;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
