package com.yamo.skillsmates.models.account.config;

import com.yamo.skillsmates.models.BaseModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = StudyLevel.TABLE_NAME)
public class StudyLevel extends BaseModel {
    public static final String TABLE_NAME = "studyLevel";
    public static final String LABEL_COLUMN = "label";
    public static final String DESCRIPTION_COLUMN = "description";
    public static final String EDUCATION_COLUMN = "education";
    public static final String SPECIFIED_COLUMN = "specified";

    @Column(name = LABEL_COLUMN, nullable = false)
    private String label;

    @Column(name = DESCRIPTION_COLUMN)
    private String description;

    @ManyToOne
    @JoinColumn(name=EDUCATION_COLUMN)
    private Education education;

    @Column(name = SPECIFIED_COLUMN, columnDefinition = "boolean default false")
    private boolean specified;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
