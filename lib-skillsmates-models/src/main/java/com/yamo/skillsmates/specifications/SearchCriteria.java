package com.yamo.skillsmates.specifications;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SearchCriteria {
    private String key;
    private Object value;
    private SearchOperation operation;
}
