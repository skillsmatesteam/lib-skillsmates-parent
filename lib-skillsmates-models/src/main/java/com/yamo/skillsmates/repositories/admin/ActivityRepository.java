package com.yamo.skillsmates.repositories.admin;

import com.yamo.skillsmates.models.admin.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Integer> {
    Optional<Activity> findByIdServer(String idServer);
    Optional<Activity> findByTitle(String title);
}
