package com.yamo.skillsmates.repositories.account;


import com.yamo.skillsmates.models.account.config.Level;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LevelRepository extends JpaRepository<Level, Integer> {
    Optional<Level> findByIdServer(String idServer);
    Optional<Level> findByLabel(String label);
}
