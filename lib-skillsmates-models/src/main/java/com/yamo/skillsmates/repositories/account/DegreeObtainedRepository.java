package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.DegreeObtained;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DegreeObtainedRepository extends JpaRepository<DegreeObtained, Integer> {
    Optional<DegreeObtained> findByIdServer(String idServer);
    Optional<List<DegreeObtained>> findAllByDeletedFalseAndAccount(Account account, Pageable pageable);
    Optional<DegreeObtained> findByIdServerAndAccount(String idServer, Account account);
    long countAllByActiveTrueAndDeletedFalseAndAccount(Account account);
}
