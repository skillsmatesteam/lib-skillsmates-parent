package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.HigherEducation;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HigherEducationRepository extends JpaRepository<HigherEducation, Integer> {
    Optional<HigherEducation> findByIdServer(String idServer);
    Optional<HigherEducation> findByIdServerAndAccount(String idServer, Account account);
    Optional<List<HigherEducation>> findAllByDeletedFalseAndAccount(Account account, Pageable pageable);
    Optional<List<HigherEducation>> findAllByActiveTrueAndDeletedFalseAndAccount(Account account);
    long countAllByActiveTrueAndDeletedFalseAndAccount(Account account);
}
