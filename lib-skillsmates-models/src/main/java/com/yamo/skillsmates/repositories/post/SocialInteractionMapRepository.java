package com.yamo.skillsmates.repositories.post;

import com.yamo.skillsmates.models.post.SocialInteractionMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SocialInteractionMapRepository extends JpaRepository<SocialInteractionMap, Integer> {
    Optional<SocialInteractionMap> findByIdServer(String idServer);
    Long countAllByElementAndSocialInteraction_SocialInteractionType_Label(String element, String label);
    Optional<List<SocialInteractionMap>> findAllByElement(String element);
}
