package com.yamo.skillsmates.repositories.multimedia;

import com.yamo.skillsmates.models.multimedia.Metadata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MetadataRepository extends JpaRepository<Metadata, Integer> {
    Optional<List<Metadata>> findByUrl(String url);
}
