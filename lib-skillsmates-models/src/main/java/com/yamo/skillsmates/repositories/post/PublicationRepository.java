package com.yamo.skillsmates.repositories.post;

import com.yamo.skillsmates.models.post.Publication;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface PublicationRepository extends JpaRepository<Publication, Integer>,
        JpaSpecificationExecutor<Publication> {
    List<Publication> findByAuthorIdInOrderByDateDesc(List<Integer> authors, Pageable pageable);

    List<Publication> findByAuthorIdOrderByDateDesc(Integer author, Pageable pageable);

    Optional<Publication> findByAuthorIdAndContentId(Integer authorId, Integer contentId);
}
