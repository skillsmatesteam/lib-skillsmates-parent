package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.config.ActivityArea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ActivityAreaRepository extends JpaRepository<ActivityArea, Integer> {
    Optional<ActivityArea> findByIdServer(String idServer);
    Optional<ActivityArea> findByLabel(String label);
}
