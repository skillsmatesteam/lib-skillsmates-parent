package com.yamo.skillsmates.repositories.post;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.multimedia.config.MediaSubtype;
import com.yamo.skillsmates.models.post.Post;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer>, JpaSpecificationExecutor<Post> {
    Optional<Post> findByIdServer(String idServer);
    Optional<List<Post>> findByActiveTrueAndDeletedFalseAndAccount(Account account, Pageable pageable);
    Long countByActiveTrueAndDeletedFalseAndAccount(Account account);
    Optional<List<Post>> findByTitleIgnoreCaseContainingAndMediaSubtype(String content, MediaSubtype mediaSubtype, Pageable pageable);
    Optional<List<Post>> findByActiveTrueAndDeletedFalseAndAccountIn(List<Account> accounts, Pageable pageable);
    @Query("SELECT p FROM Post as p \n" +
            "JOIN Account as a ON p.account = a\n" +
            "JOIN SocialInteractionMap as sim ON (sim.element = a.idServer OR sim.element = p.idServer)\n" +
            "JOIN SocialInteraction as si ON sim.socialInteraction = si\n" +
            "WHERE p.active = true AND p.deleted = false AND (si.socialInteractionType.id = 3 OR si.socialInteractionType.id = 4)\n" +
            "AND (p.account = ?1 OR si.emitterAccount = ?1 OR si.receiverAccount = ?1)\n" +
            "GROUP BY p.idServer\n" +
            "ORDER BY p.createdAt DESC")
    List<Post> findDashboardPosts(Account account, Pageable pageable);

    @Query(value = "SELECT * FROM post as p \n" +
            "JOIN account as a ON p.account = a.id\n" +
            "JOIN social_interaction_map as sim ON (sim.element = a.id_server OR sim.element = p.id_server)\n" +
            "JOIN social_interaction as si ON sim.social_interaction = si.id\n" +
            "WHERE p.active = true AND p.deleted = false AND (si.social_interaction_type = 3 OR si.social_interaction_type = 4)\n" +
            "AND (p.account = ?1 OR si.emitter_account = ?1 OR si.receiver_account = ?1)\n" +
            "GROUP BY p.id_server\n" +
            "ORDER BY p.created_at DESC LIMIT ?3 OFFSET ?2", nativeQuery=true)
    List<Post> findDashboardPosts(int accountId, int page, int limit);
}
