package com.yamo.skillsmates.repositories.multimedia;

import com.yamo.skillsmates.models.multimedia.config.InfosMediaType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InfosMediaTypeRepository extends JpaRepository<InfosMediaType, Integer> {
    Optional<InfosMediaType> findByIdServer(String idServer);
}
