package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.config.Discipline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DisciplineRepository extends JpaRepository<Discipline, Integer> {
    Optional<Discipline> findByIdServer(String idServer);
    Optional<Discipline> findByLabel(String label);
}
