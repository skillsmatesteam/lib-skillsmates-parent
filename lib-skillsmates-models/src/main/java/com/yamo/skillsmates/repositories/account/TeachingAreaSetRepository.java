package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.config.TeachingAreaSet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TeachingAreaSetRepository extends JpaRepository<TeachingAreaSet, Integer> {
    Optional<TeachingAreaSet> findByIdServer(String idServer);
    Optional<TeachingAreaSet> findByLabel(String label);
}
