package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.config.TeachingArea;
import com.yamo.skillsmates.models.account.config.TeachingAreaGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TeachingAreaRepository extends JpaRepository<TeachingArea, Integer> {
    Optional<TeachingArea> findByIdServer(String idServer);
    Optional<TeachingArea> findByLabel(String label);
    Optional<TeachingArea> findByLabelAndTeachingAreaGroup(String label, TeachingAreaGroup teachingAreaGroup);
}
