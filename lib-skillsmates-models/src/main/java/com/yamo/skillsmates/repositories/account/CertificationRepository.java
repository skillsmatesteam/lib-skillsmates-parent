package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.Certification;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CertificationRepository extends JpaRepository<Certification, Integer> {
    Optional<Certification> findByIdServer(String idServer);
    Optional<List<Certification>> findAllByDeletedFalseAndAccount(Account account, Pageable pageable);
    Optional<Certification> findByIdServerAndAccount(String idServer, Account account);
    long countAllByActiveTrueAndDeletedFalseAndAccount(Account account);
}
