package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.Cursus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CursusRepository extends JpaRepository<Cursus, Integer> {
}
