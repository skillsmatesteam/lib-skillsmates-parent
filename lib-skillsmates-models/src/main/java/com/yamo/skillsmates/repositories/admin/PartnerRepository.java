package com.yamo.skillsmates.repositories.admin;

import com.yamo.skillsmates.models.admin.Partner;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PartnerRepository extends JpaRepository<Partner, Integer>, JpaSpecificationExecutor<Partner> {
    Optional<Partner> findByIdServer(String idServer);
    Optional<Partner> findByActiveTrueAndDeletedFalseAndEmail(String email);
    Optional<Partner> findByDeletedFalseAndEmail(String email);
    Optional<List<Partner>> findAllByActiveTrueAndDeletedFalse();
    Optional<List<Partner>> findByActiveTrueAndDeletedFalseAndIdServerNot(String idServer, Pageable pageable);
    int countAllByActiveTrueAndDeletedFalse();
    Optional<List<Partner>> findByIdServer(String idServer, Pageable pageable);


}
