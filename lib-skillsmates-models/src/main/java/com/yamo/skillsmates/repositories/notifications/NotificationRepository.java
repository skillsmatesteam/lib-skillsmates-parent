package com.yamo.skillsmates.repositories.notifications;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.notifications.Notification;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Integer> {
    Optional<Notification> findByIdServer(String idServer);
    Optional<List<Notification>> findByEmitterAccount(Account emitterAccount, Pageable pageable);
    Long countAllByEmitterAccountAndNotificationType_LabelAndActiveTrue(Account emitterAccount, String labelTypeNotification);
    Optional<List<Notification>> findByReceiverAccountAndNotificationType_LabelAndActiveTrue(Account receiverAccount, String labelTypeNotification);
}
