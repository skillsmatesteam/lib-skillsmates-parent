package com.yamo.skillsmates.repositories.multimedia;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.multimedia.Multimedia;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MultimediaRepository extends JpaRepository<Multimedia, Integer> {
    Optional<Multimedia> findByIdServer(String idServer);
    Optional<List<Multimedia>> findByAccount(Account account);
    Optional<List<Multimedia>> findByActiveTrueAndDeletedFalseAndAccountAndType(Account account, String type);
    Optional<List<Multimedia>> findByAccountAndUrl(Account account, String url);
    int countByActiveTrueAndDeletedFalseAndAccountAndTypeAndOrigin(Account account, String type, String origin);
    Optional<List<Multimedia>> findByActiveTrueAndDeletedFalseAndAccountAndType(Account account, String type, Pageable pageable);
}
