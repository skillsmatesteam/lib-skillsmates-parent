package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.config.TeachingAreaGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TeachingAreaGroupRepository extends JpaRepository<TeachingAreaGroup, Integer> {
    Optional<TeachingAreaGroup> findByIdServer(String idServer);
    Optional<TeachingAreaGroup> findByLabel(String label);
}
