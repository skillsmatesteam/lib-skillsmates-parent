package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.config.EstablishmentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EstablishmentTypeRepository extends JpaRepository<EstablishmentType, Integer> {
    Optional<EstablishmentType> findByIdServer(String idServer);
    Optional<EstablishmentType> findByLabel(String label);
}
