package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer>, JpaSpecificationExecutor<Account> {
    Optional<Account> findByIdServer(String idServer);

    Optional<Account> findByActiveTrueAndDeletedFalseAndEmail(String email);

    Optional<Account> findByDeletedFalseAndEmail(String email);

    List<Account> findAllByActiveTrueAndDeletedFalse();

    Page<Account> findAllByActiveTrueAndDeletedFalse(Pageable pageable);

    Optional<List<Account>> findByActiveTrueAndDeletedFalseAndIdServerNot(String idServer, Pageable pageable);

    int countAllByActiveTrueAndDeletedFalse();

    Optional<List<Account>> findByIdServer(String idServer, Pageable pageable);

    Optional<List<Account>> findByActiveTrueAndDeletedFalseAndConnectedTrue();

    List<Account> findByActiveTrueAndDeletedFalseAndConnectedTrueAndIdServerNot(String idServer);

    Page<Account> findByActiveTrueAndDeletedFalseAndConnectedTrueAndIdServerNot(String idServer, Pageable pageable);

    Optional<Account> findByEmail(String email);

    Long countByActiveTrueAndDeletedFalseAndConnectedTrueAndIdServerNot(String idServer);

    Long countByActiveTrueAndDeletedFalseAndIdServerNot(String idServer);

    Page<Account> findAllByActiveTrueAndDeletedFalseAndConnectedTrueAndIdServerNot(String idServer, Pageable pageable);

    Page<Account> findAllByActiveTrueAndDeletedFalseAndIdServerNot(String idServer, Pageable pageable);

    List<Account> findByActiveTrueAndDeletedFalseAndIdServerNotIn(List<String> idServers);

    List<Account> findByActiveTrueAndDeletedFalseAndIdServerNotIn(List<String> idServers, Pageable pageable);

    Page<Account> findAllByActiveTrueAndDeletedFalseAndIdServerNotIn(List<String> idServers, Pageable pageable);

    Long countByActiveTrueAndDeletedFalseAndIdServerNotIn(List<String> idServers);

    @Query(value = "SELECT * from account WHERE deleted=false AND active=true AND id IN ( SELECT receiver_account FROM " +
            "social_interaction WHERE emitter_account=(SELECT id FROM account WHERE id_server=:id_server) AND " +
            "social_interaction_type=(SELECT id FROM social_interaction_type WHERE label='FOLLOWER') AND deleted=false " +
            "AND active=true)", nativeQuery = true)
    List<Account> findFollowees(@Param("id_server") String id_server);

    @Query(value = "SELECT * from account WHERE deleted=false AND active=true AND id IN ( SELECT emitter_account FROM " +
            "social_interaction WHERE receiver_account=(SELECT id FROM account WHERE id_server=:id_server) AND " +
            "social_interaction_type=(SELECT id FROM social_interaction_type WHERE label='FOLLOWER') AND deleted=false " +
            "AND active=true)", nativeQuery = true)
    List<Account> findFollowers(@Param("id_server") String id_server);

    @Query(value = "SELECT * from account WHERE deleted=false AND active=true AND id IN ( SELECT emitter_account FROM " +
            "social_interaction WHERE receiver_account=(SELECT id FROM account WHERE id_server=:id_server) AND " +
            "social_interaction_type=(SELECT id FROM social_interaction_type WHERE label='FAVORITE') AND deleted=false " +
            "AND active=true)", nativeQuery = true)
    List<Account> findFavorites(@Param("id_server") String id_server);

//    @Query(value = "SELECT * from account WHERE deleted=false AND active=true AND :searchPlace status in :status AND :searchParam id IN " +
//            "( SELECT receiver_account FROM social_interaction WHERE emitter_account=(SELECT id FROM account " +
//            "WHERE id_server=:id_server) AND social_interaction_type=(SELECT id FROM social_interaction_type " +
//            "WHERE label='FOLLOWER') AND deleted=false AND active=true)", nativeQuery = true)
//    Page<Account> searchFollowees(@Param("id_server") String id_server, @Param("status") List<Integer> status, @Param("searchParam") String searchParam, @Param("searchPlace") String searchPlace, Pageable pageable);
}
