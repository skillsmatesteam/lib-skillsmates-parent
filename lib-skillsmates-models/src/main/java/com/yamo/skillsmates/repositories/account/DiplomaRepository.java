package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.config.Diploma;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DiplomaRepository extends JpaRepository<Diploma, Integer> {
    Optional<Diploma> findByIdServer(String idServer);
    Optional<Diploma> findByLabel(String label);
}
