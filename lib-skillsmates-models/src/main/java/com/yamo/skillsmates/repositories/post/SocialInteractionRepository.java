package com.yamo.skillsmates.repositories.post;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.post.SocialInteraction;
import com.yamo.skillsmates.models.post.SocialInteractionType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SocialInteractionRepository extends JpaRepository<SocialInteraction, Integer> {
    Optional<SocialInteraction> findByIdServer(String idServer);

    List<SocialInteraction> findByActiveTrueAndDeletedFalseAndEmitterAccountIdServerAndSocialInteractionTypeLabel(String emitterIdServer, String socialInteractionTyleLabel);
    List<SocialInteraction> findByActiveTrueAndDeletedFalseAndEmitterAccountAndSocialInteractionType_Label(Account emitterAccount, String label);

    Page<SocialInteraction> findByActiveTrueAndDeletedFalseAndEmitterAccountAndSocialInteractionType_Label(Account emitterAccount, String label, Pageable pageable);

    Optional<List<SocialInteraction>> findByActiveTrueAndDeletedFalseAndSocialInteractionTypeAndSocialInteractionMap_Element(SocialInteractionType type, String element);

    boolean existsByActiveTrueAndDeletedFalseAndEmitterAccountAndSocialInteractionTypeAndSocialInteractionMap_Element(Account emitterAccount, SocialInteractionType type, String element);

    List<SocialInteraction> findByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(Account receiverAccount, String label);

    Page<SocialInteraction> findByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(Account receiverAccount, String label, Pageable pageable);

    Long countByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(Account receiverAccount, String labelType);

    Long countByActiveTrueAndDeletedFalseAndEmitterAccountAndSocialInteractionType_Label(Account emitterAccount, String labelType);

    Optional<List<SocialInteraction>> findByActiveTrueAndDeletedFalseAndEmitterAccountAndReceiverAccount(Account emitterAccount, Account receiverAccount);

    List<SocialInteraction> findByActiveTrueAndDeletedFalseAndSocialInteractionMap_Element(String element);

    Page<SocialInteraction> findAllByActiveTrueAndDeletedFalseAndEmitterAccountAndSocialInteractionType_Label(Account emitterAccount, String label, Pageable pageable);

    Page<SocialInteraction> findAllByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(Account receiverAccount, String label, Pageable pageable);
}
