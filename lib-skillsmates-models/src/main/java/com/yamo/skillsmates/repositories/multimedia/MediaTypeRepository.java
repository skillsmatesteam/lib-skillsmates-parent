package com.yamo.skillsmates.repositories.multimedia;

import com.yamo.skillsmates.models.multimedia.config.MediaType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MediaTypeRepository extends JpaRepository<MediaType, Integer> {
    Optional<MediaType> findByIdServer(String idServer);
    Optional<MediaType> findByLabel(String label);
}
