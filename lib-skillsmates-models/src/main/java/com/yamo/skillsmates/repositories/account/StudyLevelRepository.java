package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.config.StudyLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudyLevelRepository extends JpaRepository<StudyLevel, Integer> {
    Optional<StudyLevel> findByIdServer(String idServer);
    Optional<StudyLevel> findByLabel(String label);
}
