package com.yamo.skillsmates.repositories.admin;

import com.yamo.skillsmates.models.admin.config.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    Optional<Category> findByIdServer(String idServer);
    Optional<Category> findByLabel(String label);
}
