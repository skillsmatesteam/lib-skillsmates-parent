package com.yamo.skillsmates.repositories.notifications;

import com.yamo.skillsmates.models.notifications.NotificationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NotificationTypeRepository extends JpaRepository<NotificationType, Integer> {
    Optional<NotificationType> findByIdServer(String idServer);
    Optional<NotificationType> findByLabel(String label);

}
