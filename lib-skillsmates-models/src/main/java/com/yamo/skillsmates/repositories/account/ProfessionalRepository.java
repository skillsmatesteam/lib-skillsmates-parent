package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.Professional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProfessionalRepository extends JpaRepository<Professional, Integer> {
    Optional<Professional> findByIdServerAndAccount(String idServer, Account account);
    Optional<List<Professional>> findAllByDeletedFalseAndAccount(Account account, Pageable pageable);
    Optional<List<Professional>> findByAccountAndCurrentPositionTrue(Account account);
    long countAllByActiveTrueAndDeletedFalseAndAccount(Account account);
    Optional<List<Professional>> findByAccount_IdServer(String account_IdServer);
}
