package com.yamo.skillsmates.repositories.communication;

import com.yamo.skillsmates.models.communication.Conversation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConversationRepository extends JpaRepository<Conversation, Integer> {
    Optional<Conversation> findByIdServer(String idServer);
}
