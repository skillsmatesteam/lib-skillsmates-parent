package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.config.ActivitySector;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ActivitySectorRepository extends JpaRepository<ActivitySector, Integer> {
    Optional<ActivitySector> findByIdServer(String idServer);
    Optional<ActivitySector> findByLabel(String label);
}
