package com.yamo.skillsmates.repositories.multimedia;

import com.yamo.skillsmates.models.multimedia.config.MediaSubtype;
import com.yamo.skillsmates.models.multimedia.config.MediaType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MediaSubTypeRepository extends JpaRepository<MediaSubtype, Integer> {
    Optional<MediaSubtype> findByIdServer(String idServer);
    Optional<MediaSubtype> findByLabel(String label);
    Optional<MediaSubtype> findByMediaTypeAndLabelIgnoreCase(MediaType mediaType, String label);
    Optional<List<MediaSubtype>> findByMediaType(MediaType mediaType);
    Optional<List<MediaSubtype>> findByLabelIn(List<String> labels);
}
