package com.yamo.skillsmates.repositories.assistance;

import com.yamo.skillsmates.models.assistance.Assistance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssistanceRepository extends JpaRepository<Assistance, Integer> {

}
