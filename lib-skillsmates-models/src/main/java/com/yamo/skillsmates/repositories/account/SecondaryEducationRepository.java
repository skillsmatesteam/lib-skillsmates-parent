package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.SecondaryEducation;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SecondaryEducationRepository extends JpaRepository<SecondaryEducation, Integer> {
    Optional<SecondaryEducation> findByIdServer(String idServer);
    Optional<SecondaryEducation> findByIdServerAndAccount(String idServer, Account account);
    Optional<List<SecondaryEducation>> findAllByDeletedFalseAndAccount(Account account, Pageable pageable);
    Optional<List<SecondaryEducation>> findAllByActiveTrueAndDeletedFalseAndAccount(Account account);
    long countAllByActiveTrueAndDeletedFalseAndAccount(Account account);
}
