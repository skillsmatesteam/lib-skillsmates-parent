package com.yamo.skillsmates.repositories.notifications;

import com.yamo.skillsmates.models.notifications.NotificationMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface NotificationMapRepository extends JpaRepository<NotificationMap, Integer> {
    Optional<NotificationMap> findByIdServer(String idServer);
}
