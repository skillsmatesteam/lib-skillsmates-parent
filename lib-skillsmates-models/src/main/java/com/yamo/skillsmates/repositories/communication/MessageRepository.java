package com.yamo.skillsmates.repositories.communication;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.communication.Conversation;
import com.yamo.skillsmates.models.communication.Message;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {
    Optional<Message> findByIdServer(String idServer);
    Optional<ArrayList<Message>> findByDeletedFalseAndEmitterAccount(Account account, Pageable pageable);
    Optional<ArrayList<Message>> findByConversationAndDeletedFalse(Conversation conversation, Pageable pageable);
    Long countAllByActiveFalseAndDeletedFalseAndConversation(Conversation conversation);
    Long countAllByActiveFalseAndDeletedFalseAndReceiverAccount(Account receiver);
    Optional<ArrayList<Message>> findAllByActiveFalseAndDeletedFalseAndConversation(Conversation conversation);
    Optional<ArrayList<Message>> findAllByActiveFalseAndDeletedFalseAndReceiverAccountAndConversation(Account receiver, Conversation conversation);
    Optional<ArrayList<Message>> findAllByActiveFalseAndDeletedFalseAndReceiverAccount(Account receiverAccount);
    Optional<ArrayList<Message>> findAllByActiveFalseAndDeletedFalseAndReceiverAccount(Account receiverAccount, Pageable pageable);
    Optional<ArrayList<Message>> findAllByActiveFalseAndDeletedFalseAndReceiverAccountAndEmitterAccount(Account receiverAccount, Account emitterAccount, Pageable pageable);
}
