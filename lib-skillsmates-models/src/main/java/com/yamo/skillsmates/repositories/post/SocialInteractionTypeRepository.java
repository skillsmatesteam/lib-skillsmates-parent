package com.yamo.skillsmates.repositories.post;

import com.yamo.skillsmates.models.post.SocialInteractionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SocialInteractionTypeRepository extends JpaRepository<SocialInteractionType, Integer> {
    Optional<SocialInteractionType> findByIdServer(String idServer);
    Optional<SocialInteractionType> findByLabel(String label);
}
