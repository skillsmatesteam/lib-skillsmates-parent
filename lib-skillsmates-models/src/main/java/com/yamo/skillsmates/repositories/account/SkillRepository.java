package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.Skill;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Integer> {
    Optional<Skill> findByIdServerAndAccount(String idServer, Account account);
    Optional<List<Skill>> findAllByDeletedFalseAndAccount(Account account, Pageable pageable);
    long countAllByActiveTrueAndDeletedFalseAndAccount(Account account);
}
