package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.Subscription;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubscriptionRepository extends JpaRepository<Subscription, Integer> {
    Optional<List<Subscription>> findByFollower(Account follower); // find all accounts that i follow
    Optional<List<Subscription>> findByFollower(Account follower, Pageable pageable);
    Page<Subscription> findAllByFollower(Account follower, Pageable pageable);
    Page<Subscription> findAllByFollowee(Account followee, Pageable pageable);
    Optional<List<Subscription>> findByFollowee(Account followee); // find all account that follow me
    Optional<List<Subscription>> findByFollowee(Account followee, Pageable pageable); // find all account that follow me

    Optional<List<Subscription>> findByFollowerAndFollowee(Account follower, Account followee);
    Long countByFollower(Account follower); // count all accounts that i follow -> my followees
    Long countByFollowee(Account followee); // count all account that follow me -> my followers
}
