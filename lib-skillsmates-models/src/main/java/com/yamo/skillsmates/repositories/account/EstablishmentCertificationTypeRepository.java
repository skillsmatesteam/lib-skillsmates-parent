package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.config.EstablishmentCertificationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EstablishmentCertificationTypeRepository extends JpaRepository<EstablishmentCertificationType, Integer> {
    Optional<EstablishmentCertificationType> findByIdServer(String idServer);
    Optional<EstablishmentCertificationType> findByLabel(String label);
}
