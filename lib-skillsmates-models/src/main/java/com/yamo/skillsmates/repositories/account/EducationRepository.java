package com.yamo.skillsmates.repositories.account;

import com.yamo.skillsmates.models.account.config.Education;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EducationRepository extends JpaRepository<Education, Integer> {
    Optional<Education> findByIdServer(String idServer);
    Optional<Education> findByLabel(String label);
    Optional<Education> findByCode(String code);
}
