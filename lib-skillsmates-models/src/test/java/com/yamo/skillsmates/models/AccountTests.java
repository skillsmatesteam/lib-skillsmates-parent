package com.yamo.skillsmates.models;

import com.yamo.skillsmates.repositories.account.AccountRepository;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class AccountTests {

    @Autowired
    private AccountRepository accountRepository;

//    @Test
//    @DisplayName("should find one account")
    void shouldFindAccount(){
        try {
            accountRepository.findById(1);
        }catch (Exception e){
            Assert.fail(e.getMessage());
        }

    }
}
