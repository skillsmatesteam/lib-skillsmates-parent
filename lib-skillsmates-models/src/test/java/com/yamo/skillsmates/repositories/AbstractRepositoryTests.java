package com.yamo.skillsmates.repositories;

import com.yamo.skillsmates.models.account.Account;

public class AbstractRepositoryTests {

    protected final String emailNotExists =  "does.not.exist@email.com";
    protected final String emailExists =  "belinda.mengue@yopmail.com";
    protected String password = "belinda.mengue@yopmail.com";
    protected String securePassword = "9e00068ca1fe3558a1e85c241fb05fcb6e4d0464effd36d1eb8a985817bb9df0839b1cbb1c71d86bda1b5e71ae1a29cf2c2bd6953314a6f1e776e02e908f268a";

    protected Account generateAccount(){
        return generateAccount(emailExists, password);
    }

    protected Account generateAccount(String email, String password){
        Account account = new Account();
        account.setActive(true);
        account.setEmail(email);
        account.setPassword(password);
        account.setFirstname("Belinda");
        account.setLastname("MENGUE");

        return account;
    }
}
