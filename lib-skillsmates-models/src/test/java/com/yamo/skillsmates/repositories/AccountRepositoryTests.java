package com.yamo.skillsmates.repositories;

import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.repositories.account.AccountRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

//@DataJpaTest
//@ActiveProfiles("test")
public class AccountRepositoryTests extends AbstractRepositoryTests{

    @Autowired
    AccountRepository accountRepository;

//    @Test
    public void shouldSaveAccount(){
        Account account = generateAccount();
        Account savedAccount = accountRepository.save(account);
        assertThat(savedAccount).isNotNull();
    }
}
