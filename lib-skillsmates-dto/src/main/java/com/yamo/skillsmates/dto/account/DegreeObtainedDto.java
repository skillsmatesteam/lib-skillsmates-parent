package com.yamo.skillsmates.dto.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.account.config.DiplomaDto;
import com.yamo.skillsmates.dto.account.config.EstablishmentTypeDto;
import com.yamo.skillsmates.dto.account.config.TeachingAreaDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DegreeObtainedDto extends CursusDto{

    private DiplomaDto diploma;

    private TeachingAreaDto teachingArea;

    private EstablishmentTypeDto establishmentType;

    private Date startDate;

    private Date endDate;

    private String anotherDiploma;

    private String anotherEstablishmentType;

}
