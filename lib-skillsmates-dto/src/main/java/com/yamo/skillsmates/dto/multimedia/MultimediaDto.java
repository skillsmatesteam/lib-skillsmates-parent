package com.yamo.skillsmates.dto.multimedia;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.BaseActiveModelDto;
import com.yamo.skillsmates.dto.account.AccountDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MultimediaDto extends BaseActiveModelDto {

    private String name;

    private String extension;

    private String mimeType;

    private String type;

    private String checksum;

    private String directory;

    private AccountDto account;

    private String url;

    private MetadataDto metadata;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
