package com.yamo.skillsmates.dto.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.BaseActiveModelDto;
import com.yamo.skillsmates.dto.account.config.ActivityAreaDto;
import com.yamo.skillsmates.dto.account.config.EstablishmentCertificationTypeDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CertificationDto extends BaseActiveModelDto {

    private EstablishmentCertificationTypeDto establishmentCertificationType;

    private String issuingInstitutionName;

    private String entitledCertification;

    private ActivityAreaDto activityArea;

    private boolean isPermanent;

    private boolean isTemporary;

    private Date obtainedDate;

    private Date expirationDate;

    private String anotherActivityArea;

    private String anotherEstablishmentCertificationType;

    private String description;

    private AccountDto account;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
