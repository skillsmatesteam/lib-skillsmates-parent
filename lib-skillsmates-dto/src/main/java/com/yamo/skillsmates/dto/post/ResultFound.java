package com.yamo.skillsmates.dto.post;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultFound {

    int profile;
    int document;
    int video;
    int lien;
    int image;
    int audio;

    public ResultFound() {
        this.profile = 0;
        this.document = 0;
        this.video = 0;
        this.lien = 0;
        this.image = 0;
        this.audio = 0;
    }
}
