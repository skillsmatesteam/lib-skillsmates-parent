package com.yamo.skillsmates.dto.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.account.config.EstablishmentTypeDto;
import com.yamo.skillsmates.dto.account.config.StudyLevelDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class SchoolPathDto extends CursusDto {
    private EstablishmentTypeDto establishmentType;
    private StudyLevelDto studyLevel;
    private String anotherStudyLevel;
    private String anotherEstablishmentType;
}
