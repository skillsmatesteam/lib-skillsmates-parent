package com.yamo.skillsmates.dto.post;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.BaseActiveModelDto;
import com.yamo.skillsmates.dto.account.AccountDto;
import com.yamo.skillsmates.dto.multimedia.MultimediaDto;
import com.yamo.skillsmates.dto.multimedia.config.MediaSubtypeDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PostDto extends BaseActiveModelDto {

    private String title;
    private String type;
    private String keywords;
    private String discipline;
    private String description;
    private AccountDto account;
    private MediaSubtypeDto mediaSubtype;
    private List<MultimediaDto> multimedia;
    private int likes;
    private int shares;
    private String url;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
