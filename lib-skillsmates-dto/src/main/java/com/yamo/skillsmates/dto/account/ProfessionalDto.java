package com.yamo.skillsmates.dto.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.account.config.ActivityAreaDto;
import com.yamo.skillsmates.dto.account.config.ActivitySectorDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfessionalDto extends CursusDto {

    private ActivitySectorDto activitySector;

    private String jobTitle;

    private ActivityAreaDto activityArea;

    private Date startDate;

    private Date endDate;

    private String anotherActivitySector;

    private String anotherActivityArea;
}
