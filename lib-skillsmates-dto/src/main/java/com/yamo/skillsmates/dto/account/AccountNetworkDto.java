package com.yamo.skillsmates.dto.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.BaseModelDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountNetworkDto extends BaseModelDto {
    private List<AccountSocialInteractionsDto> followers;
    private List<AccountSocialInteractionsDto> followees;
    private List<AccountSocialInteractionsDto> suggestions;
    private List<AccountSocialInteractionsDto> aroundYou;
    private List<AccountSocialInteractionsDto> accountsOnline;
    private List<AccountSocialInteractionsDto> favorites;

    private Long numberFollowers;
    private Long numberFollowees;
    private Long numberSuggestions;
    private Long numberAccountsOnline;
    private Long numberAroundYou;
    private Long numberFavorites;

    private int[] pageFollowers;
    private int[] pageFollowees;
    private int[] pageSuggestions;
    private int[] pageAroundYou;
    private int[] pageFavorites;
    private int[] pageAccountsOnline;

    private int currentPage;


    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
