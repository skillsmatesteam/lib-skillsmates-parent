package com.yamo.skillsmates.dto.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.common.enumeration.Gender;
import com.yamo.skillsmates.common.enumeration.Status;
import com.yamo.skillsmates.dto.BaseModelDto;
import com.yamo.skillsmates.dto.account.config.ActivityAreaDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountGeneralInfosDto extends BaseModelDto {
    private AccountDto account;
    private Collection<Gender> genders;
    private Collection<String> countries;
    private Collection<Status> professionalStatuses;
    private Collection<ActivityAreaDto> activitiesAreas;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
