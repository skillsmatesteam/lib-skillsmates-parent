package com.yamo.skillsmates.dto.multimedia;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.BaseModelDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MultimediaInfosDto extends BaseModelDto {
    private List<MultimediaDto> documents;
    private int nbDocuments;
    private List<MultimediaDto> videos;
    private int nbVideos;
    private List<MultimediaDto> audios;
    private int nbAudios;
    private List<MultimediaDto> images;
    private int nbImages;
    private List<MultimediaDto> links;
    private int nbLinks;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
