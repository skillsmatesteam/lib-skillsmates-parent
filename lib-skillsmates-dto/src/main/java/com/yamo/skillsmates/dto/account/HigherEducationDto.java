package com.yamo.skillsmates.dto.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.account.config.DiplomaDto;
import com.yamo.skillsmates.dto.account.config.TeachingAreaDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HigherEducationDto extends SchoolPathDto {

    private TeachingAreaDto teachingArea;

    private DiplomaDto targetedDiploma;

    private String anotherTargetedDiploma;

}

