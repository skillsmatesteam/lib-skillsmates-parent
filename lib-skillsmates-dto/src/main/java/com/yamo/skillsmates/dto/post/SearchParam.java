package com.yamo.skillsmates.dto.post;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.common.enumeration.Status;
import com.yamo.skillsmates.dto.BaseModelDto;
import com.yamo.skillsmates.dto.multimedia.config.MediaSubtypeDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchParam extends BaseModelDto {
    String content;
    String city;
    String country;
    List<MediaSubtypeDto> mediaSubtypes;
    List<Status> statuses;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
