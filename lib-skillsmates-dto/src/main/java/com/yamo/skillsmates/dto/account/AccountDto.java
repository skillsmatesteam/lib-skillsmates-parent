package com.yamo.skillsmates.dto.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.common.enumeration.Gender;
import com.yamo.skillsmates.common.enumeration.Status;
import com.yamo.skillsmates.dto.BaseActiveModelDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountDto extends BaseActiveModelDto {

    private String firstname;

    private String lastname;

    private String address;

    private String email;

    private String phoneNumber;

    private Date birthdate;

    private String password;

    private String confirmPassword;

    private String description;

    private Gender gender;

    private String biography;

    private String city;

    private String country;

    private Status status;

    private String profilePicture;

    private String currentEstablishmentName;

    private String currentJobTitle;

    private boolean connected;

    private String role;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
