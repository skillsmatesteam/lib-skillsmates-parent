package com.yamo.skillsmates.dto.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.BaseModelDto;

import com.yamo.skillsmates.dto.account.config.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountAcademicsDto extends BaseModelDto {
    private AccountDto account;
    private Collection<DegreeObtainedDto> degreesObtained;
    private Collection<CertificationDto> certificates;
    private Collection<SecondaryEducationDto> secondaryEducations;
    private Collection<HigherEducationDto> higherEducations;
    private Collection<StudyLevelDto> studyLevels;
    private Collection<TeachingAreaDto> teachingAreas;
    private Collection<EducationDto> educations;
    private Collection<DiplomaDto> diplomas;
    private Collection<ActivityAreaDto> activityAreas;
    private Collection<EstablishmentTypeDto> establishmentTypes;
    private Collection<EstablishmentCertificationTypeDto> establishmentCertificationTypes;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
