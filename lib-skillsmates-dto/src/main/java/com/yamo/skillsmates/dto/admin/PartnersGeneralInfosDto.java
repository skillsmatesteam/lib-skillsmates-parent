package com.yamo.skillsmates.dto.admin;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.BaseModelDto;
import com.yamo.skillsmates.dto.account.config.ActivityAreaDto;
import com.yamo.skillsmates.dto.admin.config.CategoryDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnersGeneralInfosDto extends BaseModelDto {
    private Collection<PartnerDto> partners;
    private Collection<String> countries;
    private Collection<CategoryDto> categories;
    private Collection<ActivityAreaDto> activitiesAreas;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
