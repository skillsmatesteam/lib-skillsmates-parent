package com.yamo.skillsmates.dto.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.BaseModelDto;
import com.yamo.skillsmates.dto.post.PostDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountDashboardDto extends BaseModelDto {
    private AccountDto account;
    private Collection<AccountDto> accountsOnline;
    private Collection<AccountDto> accountsFollowed;
    private Collection<AccountDto> accountsFollowing;
    private Collection<AccountDto> accountsInteresting;
    private Collection<PostDto> posts;
    private Collection<String> shortcuts;
    private Collection<String> recommendations;
    private int documents;
    private int links;
    private int videos;
    private int audios;
    private int photos;
    private int follower;
    private int following;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
