package com.yamo.skillsmates.dto.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.BaseActiveModelDto;
import com.yamo.skillsmates.dto.account.config.LevelDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SkillDto extends BaseActiveModelDto {

    private String label;

    private boolean skillMastered;

    private String discipline;

    private String keywords;

    private LevelDto level;

    private AccountDto account;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
