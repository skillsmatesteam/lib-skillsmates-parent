package com.yamo.skillsmates.dto.helper;

import com.yamo.skillsmates.dto.BaseActiveModelDto;

import java.util.Comparator;

public class BaseActiveModelSorter<T extends BaseActiveModelDto> implements Comparator<T> {
    @Override
    public int compare(T o1, T o2) {
        return o2.getCreatedAt().compareTo(o1.getCreatedAt());
    }
}
