package com.yamo.skillsmates.dto.admin;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.BaseActiveModelDto;
import com.yamo.skillsmates.dto.account.config.ActivityAreaDto;
import com.yamo.skillsmates.dto.admin.config.CategoryDto;
import com.yamo.skillsmates.dto.admin.config.PartnerTypeDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnerDto extends BaseActiveModelDto {

    private String name;
    private String address;
    private boolean isPublicAddress;
    private String email;
    private String phoneNumber;
    private boolean isPublicPhoneNumber;
    private String description;
    private PartnerTypeDto partnerType;
    private String city;
    private String country;
    private String profilePicture;
    private CategoryDto category;
    private ActivityAreaDto activityArea;
    private String siren;
    private String website;
    private String facebook;
    private String instagram;
    private String tiktok;
    private String linkedin;
    private String snapchat;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
