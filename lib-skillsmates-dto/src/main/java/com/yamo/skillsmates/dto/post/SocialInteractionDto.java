package com.yamo.skillsmates.dto.post;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.BaseActiveModelDto;
import com.yamo.skillsmates.dto.account.AccountDto;
import com.yamo.skillsmates.dto.multimedia.MultimediaDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SocialInteractionDto extends BaseActiveModelDto {

    private String content;
    private String type;
    private String element;
    private AccountDto emitterAccount;
    private AccountDto receiverAccount;
    List<MultimediaDto> multimedia;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
