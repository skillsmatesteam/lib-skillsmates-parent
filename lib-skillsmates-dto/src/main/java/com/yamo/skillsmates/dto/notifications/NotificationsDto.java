package com.yamo.skillsmates.dto.notifications;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.BaseModelDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationsDto extends BaseModelDto {

    private List<NotificationDto> likes;
    private List<NotificationDto> comments;
    private List<NotificationDto> followers;
    private List<NotificationDto> shares;
    private List<NotificationDto> favorites;
    private List<NotificationDto> messages;
    private List<NotificationDto> posts;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
