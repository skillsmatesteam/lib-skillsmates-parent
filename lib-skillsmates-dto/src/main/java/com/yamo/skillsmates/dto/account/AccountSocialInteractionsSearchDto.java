package com.yamo.skillsmates.dto.account;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yamo.skillsmates.dto.BaseModelDto;
import com.yamo.skillsmates.dto.post.ResultFound;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountSocialInteractionsSearchDto extends BaseModelDto {
    private List<AccountSocialInteractionsDto> accountSocialInteractions;
    private Long numberAccounts;
    private int[] pageAccounts;
    private int currentPage;
    private ResultFound resultFound;

    @Override
    public String getLog() {
        return null;
    }

    @Override
    public String getLogDetail() {
        return null;
    }
}
