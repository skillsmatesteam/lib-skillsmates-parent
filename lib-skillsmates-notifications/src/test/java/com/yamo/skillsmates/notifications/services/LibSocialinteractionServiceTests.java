package com.yamo.skillsmates.notifications.services;

import com.yamo.skillsmates.common.enumeration.SocialInteractionTypeEnum;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.communication.Message;
import com.yamo.skillsmates.notifications.services.impl.LibSocialInteractionServiceImpl;
import com.yamo.skillsmates.repositories.communication.MessageRepository;
import com.yamo.skillsmates.repositories.post.PostRepository;
import com.yamo.skillsmates.repositories.post.SocialInteractionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class LibSocialinteractionServiceTests {

    private final SocialInteractionRepository socialInteractionRepository = Mockito.mock(SocialInteractionRepository.class);
    private final MessageRepository messageRepository = Mockito.mock(MessageRepository.class);
    private final PostRepository postRepository = Mockito.mock(PostRepository.class);

    public LibSocialInteractionService libSocialInteractionService;
    public Account account;

    private Account generateAccount(){
        Account account = new Account();
        account.setActive(true);
        account.setEmail("belinda.mengue@yopmail.com");
        account.setPassword("password");
        account.setFirstname("Belinda");
        account.setLastname("MENGUE");

        return account;
    }

    private Message generateMessage(){
        Message message = new Message();
        message.setActive(true);
        message.setContent("new message");
        message.setEmitterAccount(generateAccount());
        return message;
    }

    @BeforeEach
    public void setup(){
        libSocialInteractionService = new LibSocialInteractionServiceImpl(socialInteractionRepository, messageRepository, postRepository);
        generateAccount();
    }

    @Test
    void shouldFindNoUnreadMessagesByReceiver(){
        Mockito.when(messageRepository.findAllByActiveFalseAndDeletedFalseAndReceiverAccount(account)).thenReturn(Optional.of(new ArrayList<>()));

        List<Message> messages = libSocialInteractionService.findUnreadMessagesByReceiver(account);
        assertThat(messages).isEmpty();
    }

    @Test
    void shouldFindUnreadMessagesByReceiver(){
        ArrayList<Message> unreadMessages = new ArrayList<>();
        unreadMessages.add(generateMessage());
        Mockito.when(messageRepository.findAllByActiveFalseAndDeletedFalseAndReceiverAccount(account)).thenReturn(Optional.of(unreadMessages));

        List<Message> messages = libSocialInteractionService.findUnreadMessagesByReceiver(account);
        assertThat(messages).isNotEmpty();
        assertThat(messages.size()).isEqualTo(1);
        assertThat(messages.get(0).isActive()).isTrue();
        assertThat(messages.get(0).getContent()).isEqualTo("new message");
        assertThat(messages.get(0).getEmitterAccount()).isNotNull();
        assertThat(messages.get(0).getEmitterAccount().getLastname()).isEqualTo("MENGUE");
        assertThat(messages.get(0).getEmitterAccount().getFirstname()).isEqualTo("Belinda");
    }

    @Test
    void shouldCountSocialInteractionsByReceiverAndType(){
        Mockito.when(socialInteractionRepository.countByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(account, SocialInteractionTypeEnum.LIKE.name())).thenReturn(2L);

        assertThat(libSocialInteractionService.countSocialInteractionsByReceiverAndType(account, SocialInteractionTypeEnum.LIKE.name())).isEqualTo(2);
    }

    @Test
    void shouldCountPosts(){
        Mockito.when(postRepository.countByActiveTrueAndDeletedFalseAndAccount(account)).thenReturn(12L);

        assertThat(libSocialInteractionService.countPosts(account)).isEqualTo(12);
    }
}
