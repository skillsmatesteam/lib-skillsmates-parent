package com.yamo.skillsmates.notifications.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.post.SocialInteraction;

public interface LibNotificationService {
    /**
     * Add a notification
     * @param emitterAccount : account who initiated the social interaction
     * @param element : idServer of the element of notification (post, message, etc ...)
     * @param receiverAccount : account who is to receive the notification
     * @param type : type of the action
     * @return true is notification is sent, false otherwise
     * @throws GenericException
     */
    boolean addNotification(Account emitterAccount, String element, Account receiverAccount, String type) throws GenericException;

    boolean createNotification(SocialInteraction socialInteraction) throws GenericException;
}
