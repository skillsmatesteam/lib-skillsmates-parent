package com.yamo.skillsmates.notifications.services.impl;

import com.yamo.skillsmates.common.enumeration.NotificationTypeEnum;
import com.yamo.skillsmates.common.enumeration.SocialInteractionTypeEnum;
import com.yamo.skillsmates.common.enumeration.Status;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.mail.enums.TemplateEnum;
import com.yamo.skillsmates.mail.exception.SkillsmatesMailException;
import com.yamo.skillsmates.mail.service.MailService;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.communication.Message;
import com.yamo.skillsmates.models.notifications.Notification;
import com.yamo.skillsmates.models.notifications.NotificationMap;
import com.yamo.skillsmates.models.notifications.NotificationType;
import com.yamo.skillsmates.models.post.Post;
import com.yamo.skillsmates.models.post.SocialInteraction;
import com.yamo.skillsmates.notifications.services.LibNotificationService;
import com.yamo.skillsmates.repositories.account.AccountRepository;
import com.yamo.skillsmates.repositories.communication.MessageRepository;
import com.yamo.skillsmates.repositories.notifications.NotificationMapRepository;
import com.yamo.skillsmates.repositories.notifications.NotificationRepository;
import com.yamo.skillsmates.repositories.notifications.NotificationTypeRepository;
import com.yamo.skillsmates.repositories.post.PostRepository;
import com.yamo.skillsmates.repositories.post.SocialInteractionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class LibNotificationServiceImpl implements LibNotificationService {
    @Autowired
    private NotificationRepository notificationRepository;
    @Autowired
    private NotificationMapRepository notificationMapRepository;
    @Autowired
    private NotificationTypeRepository notificationTypeRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private SocialInteractionRepository socialInteractionRepository;
    @Autowired
    private MailService mailService;
    @Value("${ms.skillsmates.hostname}")
    private String hostname;
    @Value("${aws_s3_multimedia_bucket}")
    private String aws_s3_multimedia_bucket;
    @Value("${ms.skillsmates.resources.baseurl}")
    private String base_url;

    @Override
    public boolean addNotification(Account notifiedAccount, String idElement, Account account, String type) throws GenericException {
        try {
            Notification notification = createNotification(notifiedAccount, account, type);
            addNotificationMap(idElement, notification);
            sendNotificationEmail(notification);
            return true;
        } catch (Exception e) {
            throw new GenericException("Error adding notification");
        }
    }

    @Override
    public boolean createNotification(SocialInteraction socialInteraction) throws GenericException {
        try {
            Notification notification = createNotification(socialInteraction.getEmitterAccount(), socialInteraction.getReceiverAccount(), socialInteraction.getSocialInteractionType().getLabel());
            addNotificationMap(socialInteraction.getSocialInteractionMap().getElement(), notification);
//            sendNotificationEmail(notification);
            return true;
        } catch (Exception e) {
            throw new GenericException("Error adding notification");
        }
    }

    /**
     * add notification map
     * @param idElement
     * @param notification
     * @throws GenericException
     */
    private void addNotificationMap(String idElement, Notification notification) throws GenericException{
        NotificationMap notificationMap = new NotificationMap();
        notificationMap.setIdServer(String.valueOf(System.nanoTime()));
        notificationMap.setIdElement(idElement);
        notificationMap.setNotification(notification);
        notificationMap = notificationMapRepository.save(notificationMap);
        notification.setNotificationMap(notificationMap);
        notification.setTitle(setTitle(notification));
        notificationRepository.save(notification);
    }

    /**
     * generate the title of notification
     * @param notification
     * @return
     */
    private String setTitle(Notification notification) {
        String title = "";
        String element = notification.getNotificationMap().getIdElement();
        NotificationTypeEnum notificationType = NotificationTypeEnum.getEnumFromName(notification.getNotificationType().getLabel());
        switch (notificationType){
            case LIKE:
            case SHARE:
            case COMMENT:
            case POST:
                Optional<Post> post = postRepository.findByIdServer(element);
                title = post.isPresent() ? post.get().getTitle() : "";
                break;
            case FAVORITE:
            case FOLLOWER:
                Optional<Account> account = accountRepository.findByIdServer(element);
                title = account.isPresent() ? account.get().getFirstname() + " " + account.get().getLastname() : "";
                break;
            case MESSAGE:
                Optional<Message> message = messageRepository.findByIdServer(element);
                title = message.isPresent() ? message.get().getContent() : "";
                break;
            default:
                break;

        }
        return title;
    }

    /**
     * create a notification to send
     * @param emitterAccount: Account who did the social interaction
     * @param receiverAccount: Account who will receive the notification
     * @param type : type of notification
     * @return notification object
     * @throws GenericException
     */
    private Notification createNotification(Account emitterAccount, Account receiverAccount, String type) throws GenericException{
        Notification notification = new Notification();
        notification.setTitle("");
        notification.setIdServer(String.valueOf(System.nanoTime()));
        notification.setEmitterAccount(emitterAccount);
        notification.setReceiverAccount(receiverAccount);
        Optional<NotificationType> notificationType = notificationTypeRepository.findByLabel(type);
        if (!notificationType.isPresent()){
            throw new GenericException("Error adding notification");
        }
        notification.setNotificationType(notificationType.get());
        return  notificationRepository.save(notification);
    }

    /**
     * send an email of notification
     * @param notification
     * @throws SkillsmatesMailException
     * @throws AddressException
     * @throws GenericException
     */
    private void sendNotificationEmail(Notification notification) throws SkillsmatesMailException, AddressException, GenericException {

        Long nbrPost = postRepository.countByActiveTrueAndDeletedFalseAndAccount(notification.getEmitterAccount());
        Long nbrFollower = socialInteractionRepository.countByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(notification.getEmitterAccount(), SocialInteractionTypeEnum.FOLLOWER.name());
        Long nbrFollowee = socialInteractionRepository.countByActiveTrueAndDeletedFalseAndEmitterAccountAndSocialInteractionType_Label(notification.getEmitterAccount(), SocialInteractionTypeEnum.FOLLOWER.name());

        TemplateEnum templateEnum = getTempleteNotificationEmailByType(notification.getNotificationType().getLabel());
        Map<String, String> templateVars = new HashMap<>();
        InternetAddress internetAddress = new InternetAddress(notification.getReceiverAccount().getEmail(), true);
        templateVars.put("ACCOUNT_NAME", notification.getReceiverAccount().getFirstname() + " " + notification.getReceiverAccount().getLastname());
        templateVars.put("ACCOUNT_FIRST_NAME", notification.getReceiverAccount().getFirstname());

        templateVars.put("NBR_POST", String.valueOf(nbrPost));
        templateVars.put("NBR_FOLLOWER", String.valueOf(nbrFollower));
        templateVars.put("NBR_FOLLOWEE", String.valueOf(nbrFollowee));

        templateVars.put("FOLLOWER_USERNAME", notification.getEmitterAccount().getFirstname() + " " + notification.getEmitterAccount().getLastname());
        Optional<Post> post = postRepository.findByIdServer(notification.getNotificationMap().getIdElement());
        if (post.isPresent()){
            templateVars.put("POST_TITLE", post.get().getTitle());
            templateVars.put("FIRST_LAST_NAME", notification.getEmitterAccount().getFirstname() + " " + notification.getEmitterAccount().getLastname());

            if(notification.getEmitterAccount().getCity() != null && notification.getEmitterAccount().getCountry() != null)
                templateVars.put("LOCATION", notification.getEmitterAccount().getCity() + " - " + notification.getEmitterAccount().getCountry());

            if(notification.getEmitterAccount().getProfilePicture() != null)
                templateVars.put("URL_PROFILE_IMAGE", aws_s3_multimedia_bucket + notification.getEmitterAccount().getIdServer() + '/' + notification.getEmitterAccount().getProfilePicture().toLowerCase());
            else{
                templateVars.put("URL_PROFILE_IMAGE", base_url + "/images/user.png");
            }

            if(notification.getEmitterAccount().getStatus() == null){
                templateVars.put("ICON_STUDENT_PROF_TEACHER", base_url + "/images/job.png");
                templateVars.put("ICON_ESTABLISHMENT_ENTERPRISE", base_url + "/images/company.png");
            } else{
                if(notification.getEmitterAccount().getStatus().equals(Status.STUDENT)){
                    templateVars.put("ICON_STUDENT_PROF_TEACHER", base_url + "/images/training.png");
                    templateVars.put("ICON_ESTABLISHMENT_ENTERPRISE", base_url + "/images/school.png");
                }
                else if(notification.getEmitterAccount().getStatus().equals(Status.TEACHER)){
                    templateVars.put("ICON_STUDENT_PROF_TEACHER", base_url + "/images/board.png");
                    templateVars.put("ICON_ESTABLISHMENT_ENTERPRISE", base_url + "/images/school.png");
                }
                else{
                    templateVars.put("ICON_STUDENT_PROF_TEACHER", base_url + "/images/job.png");
                    templateVars.put("ICON_ESTABLISHMENT_ENTERPRISE", base_url + "/images/company.png");
                }
                templateVars.put("ACCOUNT_STATUS", notification.getEmitterAccount().getStatus().getCode());
            }

            if(notification.getEmitterAccount().getCurrentJobTitle() != null)
                templateVars.put("CURRENT_JOB_TITLE", notification.getEmitterAccount().getCurrentJobTitle());
           // else templateVars.put("CURRENT_JOB_TITLE", "ras");

            if(notification.getEmitterAccount().getCurrentEstablishmentName() != null)
                templateVars.put("CURRENT_ESTABLISHMENT_NAME", notification.getEmitterAccount().getCurrentEstablishmentName());
            //else templateVars.put("CURRENT_ESTABLISHMENT_NAME", "ras");

            switch (templateEnum){
                case NOTIFICATION_NEW_POST:
                case NOTIFICATION_NEW_LIKE:
                    templateVars.put("LIKE_LINK", hostname + "/posts/" + post.get().getIdServer());
                    break;
                case NOTIFICATION_NEW_COMMENT:
                    templateVars.put("COMMENT_LINK", hostname + "/posts/" + post.get().getIdServer());
                    break;
                case NOTIFICATION_NEW_SHARE:
                    templateVars.put("SHARE_LINK", hostname + "/posts/" + post.get().getIdServer());
                    break;
                case NOTIFICATION_NEW_MESSAGE:
                    templateVars.put("MESSAGE_LINK", hostname + "/messages/my");
                    break;
                case NOTIFICATION_NEW_FOLLOWER:
                case NOTIFICATION_NEW_FAVORITE:
                    templateVars.put("FOLLOWER_POFILE_LINK", hostname + "/profile/" + notification.getEmitterAccount().getIdServer());
                    break;
                default:
                    break;
            }
        }

        mailService.sendNotification(templateVars, internetAddress, templateEnum);
    }

    /**
     * get template of email according to type of notification
     * @param type of notification
     * @return template of email
     * @throws GenericException
     */
    private TemplateEnum getTempleteNotificationEmailByType(String type) throws GenericException {
        TemplateEnum templateEnum;
        Optional<NotificationType> notificationType = notificationTypeRepository.findByLabel(type);
        if (!notificationType.isPresent()){
            throw new GenericException("Error adding notification");
        }
        if (notificationType.get().getLabel().equalsIgnoreCase(NotificationTypeEnum.LIKE.name())){
            templateEnum = TemplateEnum.NOTIFICATION_NEW_LIKE;
        }else if (notificationType.get().getLabel().equalsIgnoreCase(NotificationTypeEnum.COMMENT.name())){
            templateEnum = TemplateEnum.NOTIFICATION_NEW_COMMENT;
        }else if (notificationType.get().getLabel().equalsIgnoreCase(NotificationTypeEnum.FOLLOWER.name())){
            templateEnum = TemplateEnum.NOTIFICATION_NEW_FOLLOWER;
        }else if (notificationType.get().getLabel().equalsIgnoreCase(NotificationTypeEnum.MESSAGE.name())){
            templateEnum = TemplateEnum.NOTIFICATION_NEW_MESSAGE;
        }else if (notificationType.get().getLabel().equalsIgnoreCase(NotificationTypeEnum.SHARE.name())){
            templateEnum = TemplateEnum.NOTIFICATION_NEW_SHARE;
        }else if (notificationType.get().getLabel().equalsIgnoreCase(NotificationTypeEnum.FAVORITE.name())){
            templateEnum = TemplateEnum.NOTIFICATION_NEW_FAVORITE;
        }else if (notificationType.get().getLabel().equalsIgnoreCase(NotificationTypeEnum.POST.name())){
            templateEnum = TemplateEnum.NOTIFICATION_NEW_POST;
        }else {
            templateEnum = TemplateEnum.NOTIFICATION;
        }
        return templateEnum;
    }
}
