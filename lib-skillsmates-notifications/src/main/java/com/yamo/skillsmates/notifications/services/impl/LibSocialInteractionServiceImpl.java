package com.yamo.skillsmates.notifications.services.impl;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.communication.Message;
import com.yamo.skillsmates.models.post.SocialInteraction;
import com.yamo.skillsmates.notifications.services.LibSocialInteractionService;
import com.yamo.skillsmates.repositories.communication.MessageRepository;
import com.yamo.skillsmates.repositories.post.PostRepository;
import com.yamo.skillsmates.repositories.post.SocialInteractionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LibSocialInteractionServiceImpl implements LibSocialInteractionService {
    private final SocialInteractionRepository socialInteractionRepository;
    private final MessageRepository messageRepository;
    private final PostRepository postRepository;

    @Autowired
    public LibSocialInteractionServiceImpl(SocialInteractionRepository socialInteractionRepository, MessageRepository messageRepository, PostRepository postRepository) {
        this.socialInteractionRepository = socialInteractionRepository;
        this.messageRepository = messageRepository;
        this.postRepository = postRepository;
    }

    @Override
    public List<SocialInteraction> findSocialInteractionsByReceiverAndType(Account receiverAccount, String labelSocialInteractionType) throws GenericException {
        return socialInteractionRepository
                .findByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(receiverAccount, labelSocialInteractionType);
//        return socialInteractionRepository.findByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(receiverAccount, labelSocialInteractionType).orElse(new ArrayList<>());
    }

    @Override
    public Page<SocialInteraction> findSocialInteractionsByReceiverAndType(Account receiverAccount, String labelSocialInteractionType, int page, int size) throws GenericException {
        return socialInteractionRepository.findByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(
                receiverAccount, labelSocialInteractionType, PageRequest.of(page, size));
    }

    @Override
    public List<Message> findUnreadMessagesByReceiver(Account receiverAccount) {
        return messageRepository.findAllByActiveFalseAndDeletedFalseAndReceiverAccount(receiverAccount).orElse(new ArrayList<>());
    }

    @Override
    public Long countSocialInteractionsByReceiverAndType(Account receiverAccount, String labelSocialInteractionType) {
        return socialInteractionRepository.countByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(receiverAccount, labelSocialInteractionType);
    }

    @Override
    public Long countPosts(Account account) {
        return postRepository.countByActiveTrueAndDeletedFalseAndAccount(account);
    }
}
