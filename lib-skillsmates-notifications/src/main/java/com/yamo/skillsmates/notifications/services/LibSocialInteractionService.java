package com.yamo.skillsmates.notifications.services;

import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.communication.Message;
import com.yamo.skillsmates.models.post.SocialInteraction;
import org.springframework.data.domain.Page;

import java.util.List;

public interface LibSocialInteractionService {
    /**
     * fetch all social interactions knowing account and social interaction type
     * @param receiverAccount: account of the receiver
     * @param labelSocialInteractionType: label of the social interaction type
     * @return list of social interactions
     */
    List<SocialInteraction> findSocialInteractionsByReceiverAndType(Account receiverAccount, String labelSocialInteractionType) throws GenericException;
    Page<SocialInteraction> findSocialInteractionsByReceiverAndType(Account receiverAccount, String labelSocialInteractionType, int page, int size) throws GenericException;

    /**
     * find all unread messages for a given receiver
     * @param receiverAccount: receiver account
     * @return list of messages
     */
    List<Message> findUnreadMessagesByReceiver(Account receiverAccount);

    /**
     * count all social interactions knowing the receiver and the type
     * @param receiverAccount: receiver of the social interaction
     * @param labelSocialInteractionType: social interaction type
     * @return number of social interactions
     */
    Long countSocialInteractionsByReceiverAndType(Account receiverAccount, String labelSocialInteractionType);

    Long countPosts(Account account);
}
