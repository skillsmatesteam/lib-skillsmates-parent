package com.yamo.skillsmates.common.enumeration;

/**
 * Availables languages
 */
public enum Language {
    /**
     * French
     */
    FR,
    /**
     * English
     */
    EN
}