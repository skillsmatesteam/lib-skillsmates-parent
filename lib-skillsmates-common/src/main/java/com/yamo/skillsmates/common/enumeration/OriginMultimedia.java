package com.yamo.skillsmates.common.enumeration;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public enum OriginMultimedia {
    POST("Post"), // if media is for a post
    MESSAGE("Message"); // if media if  for a message

    private String code;

    OriginMultimedia(String code) {
        this.code = code;
    }

    public static OriginMultimedia fromString(String string){
        OriginMultimedia typeMultimedia = null;
        if (StringUtils.isNotEmpty(string)){
            for (OriginMultimedia document: OriginMultimedia.values()){
                if (document.name().equalsIgnoreCase(string)){
                    typeMultimedia = document;
                    break;
                }
            }
        }
        return typeMultimedia;
    }
}
