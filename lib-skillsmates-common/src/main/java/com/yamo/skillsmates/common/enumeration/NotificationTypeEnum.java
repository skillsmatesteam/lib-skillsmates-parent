package com.yamo.skillsmates.common.enumeration;

import org.apache.commons.lang3.StringUtils;

public enum NotificationTypeEnum {
    LIKE,
    COMMENT,
    FOLLOWER,
    SHARE,
    FAVORITE,
    POST,
    MESSAGE;

    public static NotificationTypeEnum getEnumFromName(String name) {
        NotificationTypeEnum result = null;

        if (StringUtils.isNotBlank(name)) {
            for (NotificationTypeEnum notificationTypeEnum : NotificationTypeEnum.values()) {
                if (notificationTypeEnum.name().equalsIgnoreCase(name)) {
                    result = notificationTypeEnum;
                }
            }
        }
        return result;
    }
}
