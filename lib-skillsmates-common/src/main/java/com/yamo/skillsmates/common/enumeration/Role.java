package com.yamo.skillsmates.common.enumeration;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public enum Role {
    ADMIN("Admin"), // if account is admin
    USER("User"); // if account is simple user

    private String code;

    Role(String code) {
        this.code = code;
    }

    public static Role fromString(String string){
        Role role = null;
        if (StringUtils.isNotEmpty(string)){
            for (Role role1: Role.values()){
                if (role1.name().equalsIgnoreCase(string)){
                    role = role1;
                    break;
                }
            }
        }
        return role;
    }
}
