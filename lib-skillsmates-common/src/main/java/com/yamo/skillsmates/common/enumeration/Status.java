package com.yamo.skillsmates.common.enumeration;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public enum Status {
    STUDENT("Etudiant"),
    TEACHER("Enseignant"),
    PROFESSIONAL("Professionnel");

    private String code;

    Status(String code) {
        this.code = code;
    }

    public static Status fromString(String string){
        Status status = null;
        if (StringUtils.isNotEmpty(string)){
            for (Status s: Status.values()){
                if (s.getCode().equalsIgnoreCase(string)){
                    status = s;
                    break;
                }
            }
        }
        return status;
    }
}
