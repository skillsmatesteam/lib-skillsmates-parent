package com.yamo.skillsmates.common.enumeration;

public enum Gender {
    MALE,
    FEMALE
}
