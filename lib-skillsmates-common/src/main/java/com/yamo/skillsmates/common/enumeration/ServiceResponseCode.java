package com.yamo.skillsmates.common.enumeration;

public interface ServiceResponseCode {
    String getServiceResponseCode();
}
