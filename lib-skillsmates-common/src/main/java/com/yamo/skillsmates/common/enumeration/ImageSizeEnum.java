package com.yamo.skillsmates.common.enumeration;

public enum ImageSizeEnum {

    QQVGA(160, 120, 1),
    QVGA(320, 240, 2),
    VGA(640, 480, 3),
    SVGA(800, 600, 4),
    XGA(1024, 768, 5),
    UXGA(1600, 1200, 6),
    QXGA(2048, 1536, 7);

    private int width;
    private int height;
    private int order;

    /**
     * Constructeur
     *
     * @param width  la largeur du format.
     * @param height la hauteur du format.
     * @param order  l'ordre ddu format, du plus petit au plus grand.
     */
    ImageSizeEnum(int width, int height, int order) {
        this.width = width;
        this.height = height;
        this.order = order;
    }

    /**
     * Permet de retrouver un element de l'enumeration {@link ImageSizeEnum} en fonction de sa valeur textuelle.
     *
     * @param name la valeur textuelle dont on veut recupérer la clé.
     * @return la clé correspondante si elle existe, null sinon.
     */
    public static ImageSizeEnum getEnumFromName(String name) {
        ImageSizeEnum result = null;

        if (name != null) {
            for (ImageSizeEnum imageSize : ImageSizeEnum.values()) {
                if (imageSize.name().equals(name)) {
                    result = imageSize;
                }
            }
        }

        return result;
    }

    /**
     * getter de order
     *
     * @return la position de cette valeur d'enum.
     */
    public int getOrder() {
        return this.order;
    }

    /**
     * getter de width
     *
     * @return la largeur du format
     */
    public int getWidth() {
        return width;
    }

    /**
     * getter de height
     *
     * @return la hauteur du format
     */
    public int getHeight() {
        return height;
    }
}
