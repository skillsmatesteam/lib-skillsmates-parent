package com.yamo.skillsmates.common.enumeration;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public enum Mimetype {
    DOC("application/msword"),
    IMAGE("image/"),
    AUDIO("audio/"),
    TXT("text/"),
    PDF("application/pdf"),
    VIDEO("video/"),
    XLS("application/vnd.ms-excel"),
    PPT("application/vnd.ms-powerpoint"),
    MDB("application/vnd.ms-access"),
    DOCX("application/vnd.openxmlformats-officedocument");

    private String type;

    Mimetype(String type) {
        this.type = type;
    }

    public static Mimetype fromString(String string){
        Mimetype mimetype = null;
        if (StringUtils.isNotEmpty(string)){
            for (Mimetype document: Mimetype.values()){
                if (document.name().equalsIgnoreCase(string)){
                    mimetype = document;
                    break;
                }
            }
        }
        return mimetype;
    }

    /**
     * check if the file can be uploaded
     * @param type: mimetype of the file
     * @return true is mimetype correct, false otherwise
     */
    public static boolean isMimetypeAllowed(String type){
        for (Mimetype mimetype: Mimetype.values()){
            if (type.toLowerCase().startsWith(mimetype.type)){
                return true;
            }
        }
        return false;
    }
}
