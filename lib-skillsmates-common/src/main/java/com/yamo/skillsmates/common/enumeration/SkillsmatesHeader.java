package com.yamo.skillsmates.common.enumeration;

/**
 * Liste des headers que l'on peut passer dans les appels de services.
 */
public class SkillsmatesHeader {

    private SkillsmatesHeader() {
        // empty constructor
    }

    public static final String HEADER_PREFIX = "X-Ms-";
    public static final String LOG_LEVEL = HEADER_PREFIX + "LogLevel";
    public static final String TOKEN = HEADER_PREFIX + "Token";
    public static final String ID = HEADER_PREFIX + "Id";
    public static final String EXPIRATION_DATE = HEADER_PREFIX + "Expiration-Date";
}
