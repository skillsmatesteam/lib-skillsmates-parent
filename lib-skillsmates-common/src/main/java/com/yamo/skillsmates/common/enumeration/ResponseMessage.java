package com.yamo.skillsmates.common.enumeration;

import lombok.Getter;

/**
 * Enum contenant les messages des reponses aux requestes.
 */
@Getter
public enum ResponseMessage {

    ACCOUNT_NOT_ALLOWED("Votre compte n'est pas autorisé dans cette rubrique"),
    ACCOUNT_UNIDENTIFIED("Votre compte n'a pas pu être identifié"),
    ACCOUNT_NOT_FOUND("Votre compte n'existe pas"),
    ACCOUNT_ALREADY_EXISTS("Votre compte existe déjà, Veuillez vous connecté"),
    ACCOUNT_ALREADY_ACTIVE("Votre compte a déjà été activé"),
    ERROR_SAVING_ACCOUNT("Une erreur est survenue lors de la création de votre compte"),
    CREDENTIALS_INCORRECT("Login ou mot de passe incorrect"),
    INPUT_DATA_INVALID("Certaines données sont invalides"),
    POST_NOT_FOUND("Votre publication est introuvable"),
    DOCUMENT_TYPE_NOT_ALLOWED("ce type de document n'est pas autorisé"),
    POST_DOES_NOT_BELONG_TO_ACCOUNT("Cette action n'est pas possible car vous n'êtes pas l'auteur de cette publication"),
    ERROR_SAVING_POST("Une erreur est survenue lors de la sauvegarde de la publication");

    private final String message;

    ResponseMessage(String message) {
        this.message = message;
    }
}
