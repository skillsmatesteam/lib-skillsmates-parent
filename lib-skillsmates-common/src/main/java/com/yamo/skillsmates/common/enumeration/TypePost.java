package com.yamo.skillsmates.common.enumeration;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public enum TypePost {
    STATUS("Status"), // if post is a status
    ARTICLE("Article"); // if post is an article

    private String code;

    TypePost(String code) {
        this.code = code;
    }

    public static TypePost fromString(String string){
        TypePost typePost = null;
        if (StringUtils.isNotEmpty(string)){
            for (TypePost typePost1: TypePost.values()){
                if (typePost1.name().equalsIgnoreCase(string)){
                    typePost = typePost1;
                    break;
                }
            }
        }
        return typePost;
    }
}
