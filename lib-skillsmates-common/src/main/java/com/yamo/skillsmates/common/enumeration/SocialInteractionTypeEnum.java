package com.yamo.skillsmates.common.enumeration;

import org.apache.commons.lang3.StringUtils;

public enum SocialInteractionTypeEnum {
    LIKE,
    COMMENT,
    FOLLOWER,
    SHARE,
    FAVORITE,
    POST,
    MESSAGE;

    public static SocialInteractionTypeEnum getEnumFromName(String name) {
        SocialInteractionTypeEnum result = null;

        if (StringUtils.isNotBlank(name)) {
            for (SocialInteractionTypeEnum socialInteractionTypeEnum : SocialInteractionTypeEnum.values()) {
                if (socialInteractionTypeEnum.name().equalsIgnoreCase(name)) {
                    result = socialInteractionTypeEnum;
                }
            }
        }
        return result;
    }
}
