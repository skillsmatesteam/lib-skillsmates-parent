package com.yamo.skillsmates.common.enumeration;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
public enum TypeMultimedia {
    AVATAR("Avatar"),
    IMAGE("Image"),
    AUDIO("Audio"),
    LINK("Link"),
    DOCUMENT("Document"),
    VIDEO("Video");

    private String code;

    TypeMultimedia(String code) {
        this.code = code;
    }

    public static TypeMultimedia fromString(String string){
        TypeMultimedia typeMultimedia = null;
        if (StringUtils.isNotEmpty(string)){
            for (TypeMultimedia document: TypeMultimedia.values()){
                if (document.name().equalsIgnoreCase(string)){
                    typeMultimedia = document;
                    break;
                }
            }
        }
        return typeMultimedia;
    }
}
