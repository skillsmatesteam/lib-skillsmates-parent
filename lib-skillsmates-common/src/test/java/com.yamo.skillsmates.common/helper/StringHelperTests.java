package com.yamo.skillsmates.common.helper;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import java.security.NoSuchAlgorithmException;

import static org.assertj.core.api.Assertions.assertThat;

class StringHelperTests {

    @BeforeEach
    public void setup(){
    }

    @Test
    @DisplayName("Should capitalize first character")
    void shouldCapitalizeFirstCharacter() {
        assertThat(StringHelper.capitalizeFirstCharacter("fun")).isEqualTo("Fun");
    }

    @Test
    @DisplayName("Should generate secure password")
    void shouldGenerateSecurePassword() throws NoSuchAlgorithmException {
        String password = "password";
        String salt = "salt";
        String securePassword = "2908d2c28dfc047741fc590a026ffade237ab2ba7e1266f010fe49bde548b5987a534a86655a0d17f336588e540cd66f67234b152bbb645b4bb85758a1325d64";
        assertThat(StringHelper.generateSecurePassword(password, salt)).isEqualTo(securePassword);
    }

    @Test
    @DisplayName("Url should be valid")
    void urlShouldBeValid(){
        assertThat(StringHelper.isUrlValid("http://skills-mates.com")).isTrue();
    }

    @Test
    @DisplayName("Should parse integer")
    void shouldParseInteger(){
        assertThat(StringHelper.parseInt("2")).isEqualTo(2);
    }

    @Test
    @DisplayName("Should fail parsing integer")
    void shouldFailParsingInteger(){
        assertThat(StringHelper.parseInt("int")).isZero();
    }
}
