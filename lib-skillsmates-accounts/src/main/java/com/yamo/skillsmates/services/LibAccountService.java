package com.yamo.skillsmates.services;

import com.yamo.skillsmates.common.enumeration.Status;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.dto.post.ResultFound;
import com.yamo.skillsmates.dto.post.SearchParam;
import com.yamo.skillsmates.models.account.*;
import com.yamo.skillsmates.models.account.config.ActivityArea;
import com.yamo.skillsmates.models.post.Post;
import com.yamo.skillsmates.models.post.SocialInteraction;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface LibAccountService {
    Account authenticate(String identifier, String password) throws GenericException;
    Account updateAccount(Account account) throws GenericException;
    Account findAccountByIdServer(String idServer)throws GenericException;
    Account verificate(String email) throws GenericException;
    List<ActivityArea> findAllActivitiesAreas();

    /**
     * find accounts that i follow
     * @param id:
     * @return
     * @throws GenericException
     */
    List<AccountSocialInteractions> findFollowees(String id) throws GenericException;
    List<Account> findFollowees(Account account) throws GenericException;
    List<AccountSocialInteractions> findFollowees(String id, int page, int size) throws GenericException;
    Page<SocialInteraction> findPageFollowees(String id, int page, int size) throws GenericException;

    /**
     * find accounts who follow me
     * @param id
     * @return
     * @throws GenericException
     */
    List<AccountSocialInteractions> findFollowers(String id) throws GenericException;
    List<Account> findFollowers(Account account) throws GenericException;
    List<AccountSocialInteractions> findFollowers(String id, int page, int size) throws GenericException;
    Page<SocialInteraction> findPageFollowers(String id, int page, int size) throws GenericException;

    /**
     * find accounts who follow me
     * @param id
     * @return
     * @throws GenericException
     */
    List<AccountSocialInteractions> findSuggestions(String id) throws GenericException;
    List<AccountSocialInteractions> findSuggestions(String id, int page, int size) throws GenericException;
    Page<Account> findPageSuggestions(String id, int page, int size) throws GenericException;
    Page<AccountSocialInteractions> findSuggestionsAsAccountSocialInteraction(String id, int page, int size) throws GenericException;
    Page<AccountSocialInteractions> findFollowersAsAccountSocialInteraction(String id, int page, int size) throws GenericException;
    Page<AccountSocialInteractions> findFolloweesAsAccountSocialInteraction(String id, int page, int size) throws GenericException;
    Page<AccountSocialInteractions> findAroundYouAsAccountSocialInteraction(String id, int page, int size) throws GenericException;
    Page<AccountSocialInteractions>findAccountsOnlineAsAccountSocialInteraction(String id, int page, int size) throws GenericException;

    boolean deleteAccount(String idServer) throws GenericException;
    boolean deactivateAccount(String idServer) throws GenericException;
    List<Account> findAccountsMessages(String id) throws GenericException;
    Professional getLastProfessional(Account account) throws GenericException;
    SecondaryEducation getLastSecondaryEducation(Account account) throws GenericException;
    HigherEducation getLastHigherEducation(Account account) throws GenericException;
    String getLastEstablishment(Account account);
    void updateProfessionalTitleToAccount(Account account);

    Long countAroundYou(String accountId);
    List<AccountSocialInteractions> findAroundYou(String id) throws GenericException;
    List<AccountSocialInteractions> findAroundYou(String id, int page, int size) throws GenericException;

    Long countAccountsOnline(String accountId);
    List<AccountSocialInteractions> findAccountsOnline(String id) throws GenericException;
    List<AccountSocialInteractions> findAccountsOnline(String id, int page, int size) throws GenericException;

    Long countFollowers(Account account);
    Long countFollowees(Account account);
    Long countSuggestion(String id) throws GenericException;
    Long countSocialInteractionsByType(String accountId, String type) throws GenericException;
    Long countSearchAccounts(Specification<Account> specification) throws GenericException;

    Page<Account> findPageAroundYou(String id, int page, int size) throws GenericException;
    Page<Account> findPageAccountsOnline(String id, int page, int size) throws GenericException;

    /**
     * search accounts by firstName or lastName / country / city / status
     */
    List<AccountSocialInteractions> searchAccounts(Specification<Account> specification, int page, int limit) throws GenericException;

    Page<Account> findPageSearchAccounts(Specification<Account> specification, int page, int limit) throws GenericException;
    Specification<Account> generateAccountSpecification(SearchParam searchParam);
    Specification<Account> generateAccountSpecification(SearchParam searchParam, Status status);
    List<AccountSocialInteractions> convertToAccountSocialInteractions(List<Account> accounts);
    AccountSocialInteractions convertToAccountSocialInteractions(Account account);

    Specification<Post> generatePostSpecification(SearchParam searchParam);
    ResultFound countPosts (Specification<Post> postSpecification, int page, int limit);
}
