package com.yamo.skillsmates.services.impl;

import com.yamo.skillsmates.common.enumeration.ResponseMessage;
import com.yamo.skillsmates.common.enumeration.SocialInteractionTypeEnum;
import com.yamo.skillsmates.common.enumeration.Status;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.common.helper.StringHelper;
import com.yamo.skillsmates.dto.post.PostSocialInteractionsDto;
import com.yamo.skillsmates.dto.post.ResultFound;
import com.yamo.skillsmates.dto.post.SearchParam;
import com.yamo.skillsmates.mail.service.MailService;
import com.yamo.skillsmates.mapper.post.PostMapper;
import com.yamo.skillsmates.models.BaseActiveModel;
import com.yamo.skillsmates.models.account.*;
import com.yamo.skillsmates.models.account.config.ActivityArea;
import com.yamo.skillsmates.models.post.Post;
import com.yamo.skillsmates.models.post.PostSocialInteractions;
import com.yamo.skillsmates.models.post.SocialInteraction;
import com.yamo.skillsmates.repositories.account.*;
import com.yamo.skillsmates.repositories.post.PostRepository;
import com.yamo.skillsmates.repositories.post.SocialInteractionRepository;
import com.yamo.skillsmates.services.LibAccountService;
import com.yamo.skillsmates.specifications.ModelSpecification;
import com.yamo.skillsmates.specifications.SearchCriteria;
import com.yamo.skillsmates.specifications.SearchOperation;
import com.yamo.skillsmates.validators.CryptUtil;
import com.yamo.skillsmates.validators.JwtTokenUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotNull;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
public class LibAccountServiceImpl implements LibAccountService {

    private final PostRepository postRepository;
    private final AccountRepository accountRepository;
    private final SocialInteractionRepository socialInteractionRepository;
    private final ProfessionalRepository professionalRepository;
    private final SecondaryEducationRepository secondaryEducationRepository;
    private final HigherEducationRepository higherEducationRepository;
    private final ActivityAreaRepository activityAreaRepository;
    private final CryptUtil cryptUtil;
    private final MailService mailService;
    protected JwtTokenUtil jwtTokenUtil;
    @Value("${ms.skillsmates.accounts.url}")
    private String accountUrl;
    @Value("${ms.skillsmates.accounts.link}")
    private String accountLink;
    @Value("${ms.skillsmates.reset-password.link}")
    private String accountResetPasswordLink;

    @Autowired
    public LibAccountServiceImpl(PostRepository postRepository, AccountRepository accountRepository, SocialInteractionRepository socialInteractionRepository, ProfessionalRepository professionalRepository, SecondaryEducationRepository secondaryEducationRepository, HigherEducationRepository higherEducationRepository, ActivityAreaRepository activityAreaRepository, CryptUtil cryptUtil, MailService mailService, JwtTokenUtil jwtTokenUtil) {
        this.postRepository = postRepository;
        this.accountRepository = accountRepository;
        this.socialInteractionRepository = socialInteractionRepository;
        this.professionalRepository = professionalRepository;
        this.secondaryEducationRepository = secondaryEducationRepository;
        this.higherEducationRepository = higherEducationRepository;
        this.activityAreaRepository = activityAreaRepository;
        this.cryptUtil = cryptUtil;
        this.mailService = mailService;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @Override
    public Account authenticate(String email, String password) throws GenericException {
        Optional<Account> account = accountRepository.findByActiveTrueAndDeletedFalseAndEmail(email);
        if (!account.isPresent()) {
            throw new GenericException(ResponseMessage.CREDENTIALS_INCORRECT.getMessage());
        }
        try {
            String hashPassword = StringHelper.generateSecurePassword(password, email);
            if (!hashPassword.equals(account.get().getPassword())) {
                throw new GenericException(ResponseMessage.CREDENTIALS_INCORRECT.getMessage());
            }
        } catch (NoSuchAlgorithmException e) {
            throw new GenericException(ResponseMessage.CREDENTIALS_INCORRECT.getMessage());
        }
        return account.get();
    }

    @Override
    public Account updateAccount(Account account) throws GenericException {
        Account existedAccount = findAccountByIdServer(account.getIdServer());
        account.setId(existedAccount.getId());
        account.setPassword(existedAccount.getPassword());
        return accountRepository.save(account);
    }

    @Override
    public Account findAccountByIdServer(String idServer) throws GenericException {
        return accountRepository.findByIdServer(idServer)
                .orElseThrow(() -> new GenericException(ResponseMessage.ACCOUNT_UNIDENTIFIED.getMessage()));
    }

    @Override
    public Account verificate(String email) throws GenericException {
        return accountRepository.findByEmail(email)
                .orElseThrow(() -> new GenericException(ResponseMessage.ACCOUNT_NOT_FOUND.getMessage()));
    }

    @Override
    public List<ActivityArea> findAllActivitiesAreas() {
        return activityAreaRepository.findAll();
    }

    @Override
    public List<AccountSocialInteractions> findFollowees(String id) throws GenericException { // find those i follow
        Account account = findAccountByIdServer(id);
        return convertToAccountSocialInteractions(findFollowees(account));
    }

    @Override
    @Transactional
    public List<AccountSocialInteractions> findFollowees(String id, int page, int size) throws GenericException { // find those i follow
        return socialInteractionRepository
                .findByActiveTrueAndDeletedFalseAndEmitterAccountIdServerAndSocialInteractionTypeLabel(id, SocialInteractionTypeEnum.FOLLOWER.name())
                .parallelStream()
                .map(SocialInteraction::getReceiverAccount)
                .map(this::convertToAccountSocialInteractions)
                .collect(Collectors.toList());
    }


    @Override
    public Page<SocialInteraction> findPageFollowees(String id, int page, int size) throws GenericException { // find those i follow
        Account account = findAccountByIdServer(id);
        return socialInteractionRepository.findAllByActiveTrueAndDeletedFalseAndEmitterAccountAndSocialInteractionType_Label(account, SocialInteractionTypeEnum.FOLLOWER.name(), PageRequest.of(page, size));
    }

    @Override
    public List<AccountSocialInteractions> findFollowers(String id) throws GenericException { // find accounts who follow me
        Account account = findAccountByIdServer(id);
        return convertToAccountSocialInteractions(findFollowers(account));
    }

    @Override
    public List<AccountSocialInteractions> findFollowers(String id, int page, int size) throws GenericException { // find accounts who follow me
        Account account = findAccountByIdServer(id);
        return socialInteractionRepository
                .findByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(account, SocialInteractionTypeEnum.FOLLOWER.name())
                .parallelStream()
                .map(SocialInteraction::getEmitterAccount)
                .map(this::convertToAccountSocialInteractions)
                .collect(Collectors.toList());
    }

    @Override
    public Page<SocialInteraction> findPageFollowers(String id, int page, int size) throws GenericException { // find accounts who follow me
        Account account = findAccountByIdServer(id);
        return socialInteractionRepository.findAllByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(account, SocialInteractionTypeEnum.FOLLOWER.name(), PageRequest.of(page, size));
    }

    @Override
    @Transactional
    public List<AccountSocialInteractions> findSuggestions(String id) throws GenericException {
        return accountRepository.findByActiveTrueAndDeletedFalseAndIdServerNotIn(getKnownAccounts(id))
                .parallelStream()
                .map(this::convertToAccountSocialInteractions)
                .collect(Collectors.toList());
    }

    @Override
    public List<AccountSocialInteractions> findSuggestions(String id, int page, int size) throws GenericException {
        List<Account> accounts = accountRepository.findByActiveTrueAndDeletedFalseAndIdServerNotIn(getKnownAccounts(id));
        return convertToAccountSocialInteractions(accounts);
    }

    @Override
    public Page<Account> findPageSuggestions(String id, int page, int size) throws GenericException {
        return accountRepository.findAllByActiveTrueAndDeletedFalseAndIdServerNotIn(getKnownAccounts(id), PageRequest.of(page, size));
    }

    @Override
    public Page<AccountSocialInteractions> findSuggestionsAsAccountSocialInteraction(String id, int page, int size) throws GenericException {
        return accountRepository.findAllByActiveTrueAndDeletedFalseAndIdServerNotIn(
                getKnownAccounts(id), PageRequest.of(page, size)).map(this::convertToAccountSocialInteractions);
    }

    @Override
    public Page<AccountSocialInteractions> findFollowersAsAccountSocialInteraction(String id, int page, int size) throws GenericException {
        Account account = findAccountByIdServer(id);
        return socialInteractionRepository.findByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(
                        account, SocialInteractionTypeEnum.FOLLOWER.name(), PageRequest.of(page, size))
                .map(SocialInteraction::getEmitterAccount).map(this::convertToAccountSocialInteractions);
    }

    @Override
    public Page<AccountSocialInteractions> findFolloweesAsAccountSocialInteraction(String id, int page, int size) throws GenericException {
        Account account = findAccountByIdServer(id);
        return socialInteractionRepository.findByActiveTrueAndDeletedFalseAndEmitterAccountAndSocialInteractionType_Label(
                        account, SocialInteractionTypeEnum.FOLLOWER.name(), PageRequest.of(page, size))
                .map(SocialInteraction::getReceiverAccount).map(this::convertToAccountSocialInteractions);
    }

    @Override
    public Page<AccountSocialInteractions> findAroundYouAsAccountSocialInteraction(String id, int page, int size) throws GenericException {
        return accountRepository.findAllByActiveTrueAndDeletedFalse(PageRequest.of(page, size))
                .map(this::convertToAccountSocialInteractions);
    }

    @Override
    public Page<AccountSocialInteractions> findAccountsOnlineAsAccountSocialInteraction(String id, int page, int size) throws GenericException {
        return accountRepository.findByActiveTrueAndDeletedFalseAndConnectedTrueAndIdServerNot(id, PageRequest.of(page, size))
                .map(this::convertToAccountSocialInteractions);
    }

    @Override
    public boolean deleteAccount(String idServer) throws GenericException {
        Account account = findAccountByIdServer(idServer);
        account.setActive(false);
        account.setDeleted(true);
        accountRepository.save(account);
        return true;
    }

    @Override
    public boolean deactivateAccount(String idServer) throws GenericException {
        Account account = findAccountByIdServer(idServer);
        account.setActive(false);
        accountRepository.save(account);
        return true;
    }

    @Override
    public List<Account> findAccountsMessages(String id) throws GenericException {
        // TODO : search for accounts to whom you have already sent / who have sent you a message
        List<Account> accounts = new ArrayList<>();
        Account account = findAccountByIdServer(id);

        return accountRepository.findAll();
    }

    @Override
    public Professional getLastProfessional(Account account) {
        Optional<List<Professional>> professionals = professionalRepository.findByAccountAndCurrentPositionTrue(account);
        return professionals.map(professionalList -> professionalList.get(0)).orElse(null);
    }

    @Override
    public SecondaryEducation getLastSecondaryEducation(Account account) {
        Optional<List<SecondaryEducation>> secondaryEducations = secondaryEducationRepository.findAllByActiveTrueAndDeletedFalseAndAccount(account);
        return secondaryEducations.map(educations -> educations.get(0)).orElse(null);
    }

    @Override
    public HigherEducation getLastHigherEducation(Account account) {
        Optional<List<HigherEducation>> higherEducations = higherEducationRepository.findAllByActiveTrueAndDeletedFalseAndAccount(account);
        return higherEducations.map(educations -> educations.get(0)).orElse(null);
    }

    @Override
    public String getLastEstablishment(Account account) {
        if (account != null && account.getStatus() != null) {
            if (Status.PROFESSIONAL.equals(account.getStatus()) ||
                    Status.TEACHER.equals(account.getStatus())) {
                Professional professional = getLastProfessional(account);
                return professional != null ? professional.getEstablishmentName() : "";
            }

            if (Status.STUDENT.equals(account.getStatus())) {
                HigherEducation higherEducation = getLastHigherEducation(account);
                if (higherEducation != null) return higherEducation.getEstablishmentName();
                SecondaryEducation secondaryEducation = getLastSecondaryEducation(account);
                if (secondaryEducation != null) return secondaryEducation.getEstablishmentName();
                return "";
            }
        }
        return "";
    }

    @Override
    public void updateProfessionalTitleToAccount(Account account) {
        if (account != null && account.getStatus() != null) {
            if (Status.PROFESSIONAL.equals(account.getStatus()) ||
                    Status.TEACHER.equals(account.getStatus())) {
                Professional professional = getLastProfessional(account);
                if (professional != null) {
                    account.setCurrentEstablishmentName(professional.getEstablishmentName());
                    account.setCurrentJobTitle(professional.getJobTitle());
                }
            } else if (Status.STUDENT.equals(account.getStatus())) {
                HigherEducation higherEducation = getLastHigherEducation(account);
                if (higherEducation != null) {
                    account.setCurrentEstablishmentName(higherEducation.getEstablishmentName());
                    account.setCurrentJobTitle(higherEducation.getSpecialty());
                } else {
                    SecondaryEducation secondaryEducation = getLastSecondaryEducation(account);
                    if (secondaryEducation != null) {
                        account.setCurrentEstablishmentName(secondaryEducation.getEstablishmentName());
                        account.setCurrentJobTitle(secondaryEducation.getSpecialty());
                    }
                }
            }
            accountRepository.save(account);
        }
    }

    @Override
    public Long countAroundYou(String accountId) {
        return accountRepository.countByActiveTrueAndDeletedFalseAndIdServerNot(accountId);
    }

    @Override
    @Transactional
    public List<AccountSocialInteractions> findAroundYou(String accountId) throws GenericException {
        return accountRepository.findAllByActiveTrueAndDeletedFalse()
                .parallelStream()
                .map(this::convertToAccountSocialInteractions)
                .collect(Collectors.toList());
    }

    @Override
    public List<AccountSocialInteractions> findAroundYou(String id, int page, int size) throws GenericException {
        return accountRepository.findAllByActiveTrueAndDeletedFalse()
                .parallelStream()
                .map(this::convertToAccountSocialInteractions)
                .collect(Collectors.toList());
    }

    @Override
    public Long countAccountsOnline(String accountId) {
        return accountRepository.countByActiveTrueAndDeletedFalseAndConnectedTrueAndIdServerNot(accountId);
    }

    @Override
    public List<AccountSocialInteractions> findAccountsOnline(String id) throws GenericException {
        return accountRepository.findByActiveTrueAndDeletedFalseAndConnectedTrueAndIdServerNot(id)
                .parallelStream()
                .map(this::convertToAccountSocialInteractions)
                .collect(Collectors.toList());
    }

    @Override
    public List<AccountSocialInteractions> findAccountsOnline(String id, int page, int size) throws GenericException {
        List<Account> accounts = accountRepository.findByActiveTrueAndDeletedFalseAndConnectedTrueAndIdServerNot(id, PageRequest.of(page, size)).getContent();
        return convertToAccountSocialInteractions(accounts);
    }

    @Override
    public Long countFollowers(Account account) {
        return socialInteractionRepository.countByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(account, SocialInteractionTypeEnum.FOLLOWER.name());
    }

    @Override
    public Long countFollowees(Account account) {
        return socialInteractionRepository.countByActiveTrueAndDeletedFalseAndEmitterAccountAndSocialInteractionType_Label(account, SocialInteractionTypeEnum.FOLLOWER.name());
    }

    @Override
    public Long countSuggestion(String id) throws GenericException {
        return accountRepository.countByActiveTrueAndDeletedFalseAndIdServerNotIn(getKnownAccounts(id));
    }

    @Override
    public Long countSocialInteractionsByType(String accountId, String type) throws GenericException {
        Account account = findAccountByIdServer(accountId);
        SocialInteractionTypeEnum typeEnum = SocialInteractionTypeEnum.getEnumFromName(type);
        if (typeEnum == null) {
            throw new GenericException("Social Interaction type unidentified");
        }
        return socialInteractionRepository.countByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(account, typeEnum.name());
    }

    @Override
    public Page<Account> findPageAroundYou(String idServer, int page, int size) throws GenericException {
        return accountRepository.findAllByActiveTrueAndDeletedFalseAndIdServerNot(idServer, PageRequest.of(page, size));
    }

    @Override
    public Page<Account> findPageAccountsOnline(String idServer, int page, int size) throws GenericException {
        return accountRepository.findAllByActiveTrueAndDeletedFalseAndConnectedTrueAndIdServerNot(idServer, PageRequest.of(page, size));
    }

    @Override
    public List<AccountSocialInteractions> searchAccounts(Specification<Account> specification, int page, int limit)
            throws GenericException {
        return specification == null ? new ArrayList<>()
                : accountRepository.findAll(specification, PageRequest.of(page, limit))
                .map(this::convertToAccountSocialInteractions).getContent();
    }

    @Override
    public Long countSearchAccounts(Specification<Account> specification) throws GenericException {
        return accountRepository.count(specification);
    }

    @Override
    public Page<Account> findPageSearchAccounts(Specification<Account> specification, int page, int limit) throws GenericException {
        return accountRepository.findAll(specification, PageRequest.of(page, limit));
    }

    @Override
    public Specification<Account> generateAccountSpecification(SearchParam searchParam) {
        return generateAccountSpecification(searchParam, null);
    }

    /**
     * generate account specification for research
     *
     * @return specification of accounts
     */
    @Override
    public Specification<Account> generateAccountSpecification(SearchParam searchParam, Status status) {
        Specification<Account> specification = null;
        if (status != null) {
            ModelSpecification<Account> statusSpec = new ModelSpecification<>();
            statusSpec.add(new SearchCriteria(Account.STATUS_COLUMN, status.ordinal(), SearchOperation.EQUAL));
            specification = specification != null ? statusSpec.and(specification) : Specification.where(statusSpec);
        }
        if (StringUtils.isNotBlank(searchParam.getCountry())) {
            ModelSpecification<Account> countrySpec = new ModelSpecification<>();
            countrySpec.add(new SearchCriteria(Account.COUNTRY_COLUMN, searchParam.getCountry(), SearchOperation.EQUAL));
            specification = specification != null ? countrySpec.and(specification) : Specification.where(countrySpec);
        }
        if (StringUtils.isNotBlank(searchParam.getCity())) {
            ModelSpecification<Account> citySpec = new ModelSpecification<>();
            citySpec.add(new SearchCriteria(Account.CITY_COLUMN, searchParam.getCity(), SearchOperation.MATCH));
            specification = specification != null ? citySpec.and(specification) : Specification.where(citySpec);
        }
        if (StringUtils.isNotBlank(searchParam.getContent())) {
            Specification<Account> searchParamSpecification = null;
            String[] contentAsTable = searchParam.getContent().split(" ");
            for (String value : contentAsTable) {
                value = value.trim();
                if (value.length() > 0) {
                    ModelSpecification<Account> lastnameSpec = new ModelSpecification<>();
                    lastnameSpec.add(new SearchCriteria(Account.LASTNAME_COLUMN, value, SearchOperation.MATCH));
                    ModelSpecification<Account> firstnameSpec = new ModelSpecification<>();
                    firstnameSpec.add(new SearchCriteria(Account.FIRSTNAME_COLUMN, value, SearchOperation.MATCH));
                    Specification<Account> allFieldsSearch = lastnameSpec.or(firstnameSpec);
                    searchParamSpecification = searchParamSpecification != null ? searchParamSpecification.or(allFieldsSearch) : Specification.where(allFieldsSearch);
                }
            }
            specification = specification != null ? specification.and(searchParamSpecification) : Specification.where(searchParamSpecification);
        }
        ModelSpecification<Account> baseAccountSpec = new ModelSpecification<>();
        baseAccountSpec.add(new SearchCriteria(BaseActiveModel.ACTIVE_COLUMN, true, SearchOperation.EQUAL));
        baseAccountSpec.add(new SearchCriteria(BaseActiveModel.DELETED_COLUMN, false, SearchOperation.EQUAL));
        specification = specification != null ? specification.and(baseAccountSpec) : Specification.where(baseAccountSpec);
        return specification;
    }

    private Specification<Account> generateStatusSpecifications(Specification<Account> specification, List<Status> professionalStatusDtos) {
        if (!CollectionUtils.isEmpty(professionalStatusDtos)) {

//            ModelSpecification<Account> statusSpecifications = new ModelSpecification<>();
//
//            baseAccountSpec.add(new SearchCriteria(BaseActiveModel.ACTIVE_COLUMN, true, SearchOperation.EQUAL));
//            baseAccountSpec.add(new SearchCriteria(BaseActiveModel.DELETED_COLUMN, false, SearchOperation.EQUAL));
//            Specification<Account> specification = Specification.where(baseAccountSpec);

            List<ModelSpecification<Account>> modelSpecificationList = new ArrayList<>();
            for (Status professionalStatus : professionalStatusDtos) {
                ModelSpecification<Account> statusSpec = new ModelSpecification<>();
                statusSpec.add(new SearchCriteria(Account.STATUS_COLUMN, professionalStatus.ordinal(), SearchOperation.EQUAL));
                modelSpecificationList.add(statusSpec);
            }

//            int index = 0;
            for (ModelSpecification<Account> modelSpecification : modelSpecificationList) {
                specification = specification.or(modelSpecification);
//                if (index == 0){
//                    specification = specification.and(modelSpecification);
//                }else {
//                    specification = specification.or(modelSpecification);
//                }
//                index++;
            }
        }
        return specification;
    }

    public List<AccountSocialInteractions> convertToAccountSocialInteractions(List<Account> accounts) {
        return accounts.parallelStream().map(this::convertToAccountSocialInteractions).collect(Collectors.toList());
    }

    public AccountSocialInteractions convertToAccountSocialInteractions(Account account) {
        AccountSocialInteractions accountSocialInteractions = new AccountSocialInteractions();
        accountSocialInteractions.setId(account.getId());
        accountSocialInteractions.setIdServer(account.getIdServer());
        accountSocialInteractions.setAccount(account);
        accountSocialInteractions.setSocialInteractions(socialInteractionRepository.findByActiveTrueAndDeletedFalseAndSocialInteractionMap_Element(account.getIdServer()));
        return accountSocialInteractions;
    }

    public List<Account> findFollowers(Account account) {
        return socialInteractionRepository
                .findByActiveTrueAndDeletedFalseAndReceiverAccountAndSocialInteractionType_Label(account, SocialInteractionTypeEnum.FOLLOWER.name())
                .parallelStream()
                .map(SocialInteraction::getEmitterAccount)
                .collect(Collectors.toList());
    }

    public List<Account> findFollowees(Account account) {
        return socialInteractionRepository
                .findByActiveTrueAndDeletedFalseAndEmitterAccountAndSocialInteractionType_Label(account, SocialInteractionTypeEnum.FOLLOWER.name())
                .parallelStream()
                .map(SocialInteraction::getReceiverAccount)
                .collect(Collectors.toList());
    }

    private List<String> getKnownAccounts(String id) throws GenericException {
        List<Account> knownAccounts = new ArrayList<>();
        Account account = findAccountByIdServer(id);
        knownAccounts.add(account);
        knownAccounts.addAll(findFollowees(account));
        knownAccounts.addAll(findFollowers(account));
        List<String> idServers = new ArrayList<>();
        knownAccounts.forEach(a -> idServers.add(a.getIdServer()));
        return idServers;
    }

    @Override
    public Specification<Post> generatePostSpecification(@NotNull SearchParam searchParam) {
        Specification<Post> specification = null;

        if (StringUtils.isNotBlank(searchParam.getContent())) {
            Specification<Post> searchParamSpeccification = null;
            List<String> toIgnore = Arrays.asList("le", "la", "l'", "les", "mon", "ma", "mes", "m'", "en", "de", "du", "des", "d'", "se", "s'", "je", "j'ai");
            List<String> searchParamList = Stream.of(searchParam.getContent().split(" ")).map(String::trim).filter(e -> !toIgnore.contains(e)).collect(Collectors.toList());
            for (String stringToSearch : searchParamList) {
                ModelSpecification<Post> titleSpec = new ModelSpecification<>();
                titleSpec.add(new SearchCriteria(Post.TITLE_COLUMN, stringToSearch, SearchOperation.MATCH));
                ModelSpecification<Post> keywordsSpec = new ModelSpecification<>();
                keywordsSpec.add(new SearchCriteria(Post.KEYWORDS_COLUMN, stringToSearch, SearchOperation.MATCH));
                ModelSpecification<Post> descriptionSpec = new ModelSpecification<>();
                descriptionSpec.add(new SearchCriteria(Post.DESCRIPTION_COLUMN, stringToSearch, SearchOperation.MATCH));
                Specification<Post> allFieldsSearch = titleSpec.or(keywordsSpec).or(descriptionSpec);
                searchParamSpeccification = searchParamSpeccification != null ? searchParamSpeccification.or(allFieldsSearch) : Specification.where(allFieldsSearch);
            }
            specification = specification != null ? specification.and(searchParamSpeccification) : Specification.where(searchParamSpeccification);
        }

        if (specification != null) {
            ModelSpecification<Post> baseAccountSpec = new ModelSpecification<>();
            baseAccountSpec.add(new SearchCriteria(BaseActiveModel.ACTIVE_COLUMN, true, SearchOperation.EQUAL));
            baseAccountSpec.add(new SearchCriteria(BaseActiveModel.DELETED_COLUMN, false, SearchOperation.EQUAL));
            specification = specification.and(baseAccountSpec);
        }
        return specification;
    }

    @Override
    public ResultFound countPosts(Specification<Post> postSpecification, int page, int limit) {
        ResultFound resultFound = new ResultFound();
        List<Post> posts = postRepository.findAll(postSpecification, PageRequest.of(page, limit)).getContent();
        List<PostSocialInteractions> postSocialInteractions = convertToPostSocialInteractions(posts);
        List<PostSocialInteractionsDto> postSocialInteractionsDtos = PostMapper.getInstance().asPostSocialInteractionsDtos(postSocialInteractions);
        postSocialInteractionsDtos.stream().filter(e -> e.getPost().getMediaSubtype() != null)
                .map(e -> e.getPost().getMediaSubtype().getMediaType().getLabel().toUpperCase())
                .forEach(e -> {
                    if (e.equals("DOCUMENT")) {
                        resultFound.setDocument(resultFound.getDocument() + 1);
                    }
                    if (e.equals("VIDEO")) {
                        resultFound.setVideo(resultFound.getVideo() + 1);
                    }
                    if (e.equals("IMAGE")) {
                        resultFound.setImage(resultFound.getImage() + 1);
                    }
                    if (e.equals("AUDIO")) {
                        resultFound.setAudio(resultFound.getAudio() + 1);
                    }
                    if (e.equals("LINK")) {
                        resultFound.setLien(resultFound.getLien() + 1);
                    }
                });
        return resultFound;
    }

    private List<PostSocialInteractions> convertToPostSocialInteractions(List<Post> posts) {
        List<PostSocialInteractions> postsSocialInteractions = new ArrayList<>();
        for (Post post : posts) {
            PostSocialInteractions postSocialInteractions = new PostSocialInteractions();
            postSocialInteractions.setId(post.getId());
            postSocialInteractions.setIdServer(post.getIdServer());
            postSocialInteractions.setPost(post);
            postSocialInteractions.setSocialInteractions(socialInteractionRepository.findByActiveTrueAndDeletedFalseAndSocialInteractionMap_Element(post.getIdServer()));
            postsSocialInteractions.add(postSocialInteractions);
        }
        return postsSocialInteractions;
    }
}
