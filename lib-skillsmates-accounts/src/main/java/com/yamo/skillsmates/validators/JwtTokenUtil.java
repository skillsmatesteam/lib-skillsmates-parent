package com.yamo.skillsmates.validators;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtTokenUtil implements Serializable {

    private static final long serialVersionUID = -2550185165626007488L;

    @Value("${jwt.jwt_token_validity}")
    public String jwt_token_validity;

    @Value("${jwt.secret}")
    private String secret;

//retrieve accountId from jwt token
    public String getAccountIdFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

//retrieve expiration date from jwt token
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {

        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    //for retrieveing any information from token we will need the secret key
    private Claims getAllClaimsFromToken(String token) {

        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    //check if the token has expired
    private Boolean isTokenExpired(String token) {

        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());

    }

    //generate token by identifier
    public String generateToken(String identifier) {

        Map<String, Object> claims = new HashMap<>();
        return doGenerateToken(claims, identifier);
    }

//while creating the token

    private String doGenerateToken(Map<String, Object> claims, String subject) {

        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + Long.parseLong(jwt_token_validity) * 1000))
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

    //validate token
    public Boolean validateToken(String token, String identifier) {
        final String accountId = getAccountIdFromToken(token);
        return (accountId.equals(identifier) && !isTokenExpired(token));
    }

    // invalidate token
    public String invalidateToken(String identifier){
        Map<String, Object> claims = new HashMap<>();
        return Jwts.builder().setClaims(claims).setSubject(identifier).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() - Long.parseLong(jwt_token_validity) * 1000))
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

}
