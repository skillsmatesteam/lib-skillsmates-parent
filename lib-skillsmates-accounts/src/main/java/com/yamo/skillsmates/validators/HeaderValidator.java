package com.yamo.skillsmates.validators;

import com.yamo.skillsmates.common.logging.LoggingHelper;
import com.yamo.skillsmates.common.wrapper.GenericResultDto;
import com.yamo.skillsmates.repositories.account.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;


@Component
public class HeaderValidator {

    protected GenericResultDto result = null;

    @Autowired
    protected JwtTokenUtil jwtTokenUtil;
    /**
     * check the header of the request for token and id
     *
     * @param token of the user
     * @param id of the user
     * @throws Exception if something is not correct
     */
    public void validateHeader(String token, String id) throws Exception{
        if (!jwtTokenUtil.validateToken(token, id))
            throw new Exception("Invalid token");
    }

    /**
     * process header
     * @param token of the user
     * @param id of the user
     * @return GenericResultDto
     */
    public GenericResultDto processHeader(String token, String id){
        try {
            validateHeader(token, id);
        }catch (Exception e){
            result = new GenericResultDto();
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.setMessage(e.getMessage());
            result.setGenericMessageDetailsList(LoggingHelper.buildGenericMessageDetails(e));
            return result;
        }
        return null;
    }

}
