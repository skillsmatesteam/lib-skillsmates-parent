package com.yamo.skillsmates.accounts.services;

import com.yamo.skillsmates.common.enumeration.ResponseMessage;
import com.yamo.skillsmates.common.exception.GenericException;
import com.yamo.skillsmates.mail.service.MailService;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.repositories.account.*;
import com.yamo.skillsmates.repositories.post.PostRepository;
import com.yamo.skillsmates.repositories.post.SocialInteractionRepository;
import com.yamo.skillsmates.services.LibAccountService;
import com.yamo.skillsmates.services.impl.LibAccountServiceImpl;
import com.yamo.skillsmates.validators.CryptUtil;
import com.yamo.skillsmates.validators.JwtTokenUtil;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.mockito.Mockito;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class LibAccountServiceTests {

    private final PostRepository postRepository = Mockito.mock(PostRepository.class);
    private final AccountRepository accountRepository = Mockito.mock(AccountRepository.class);
    private final ActivityAreaRepository activityAreaRepository = Mockito.mock(ActivityAreaRepository.class);
    private final SocialInteractionRepository socialInteractionRepository = Mockito.mock(SocialInteractionRepository.class);
    private final ProfessionalRepository professionalRepository = Mockito.mock(ProfessionalRepository.class);
    private final SecondaryEducationRepository secondaryEducationRepository = Mockito.mock(SecondaryEducationRepository.class);
    private final HigherEducationRepository higherEducationRepository = Mockito.mock(HigherEducationRepository.class);
    private final CryptUtil cryptUtil = Mockito.mock(CryptUtil.class);
    private final MailService mailService = Mockito.mock(MailService.class);
    private final JwtTokenUtil jwtTokenUtil = Mockito.mock(JwtTokenUtil.class);

    public LibAccountService libAccountService;

    private final String emailNotExists =  "does.not.exist@email.com";
    private final String emailExists =  "belinda.mengue@yopmail.com";
    String password = "belinda.mengue@yopmail.com";
    String securePassword = "9e00068ca1fe3558a1e85c241fb05fcb6e4d0464effd36d1eb8a985817bb9df0839b1cbb1c71d86bda1b5e71ae1a29cf2c2bd6953314a6f1e776e02e908f268a";

    private Account generateAccount(String email, String password){
        Account account = new Account();
        account.setActive(true);
        account.setEmail(email);
        account.setPassword(password);
        account.setFirstname("Belinda");
        account.setLastname("MENGUE");

        return account;
    }

    @BeforeEach
    public void setup(){
        libAccountService = new LibAccountServiceImpl(postRepository, accountRepository, socialInteractionRepository, professionalRepository, secondaryEducationRepository, higherEducationRepository, activityAreaRepository, cryptUtil, mailService, jwtTokenUtil);
    }

    @Test
    @DisplayName("Should not authenticate because the email does not exist")
    public void shouldNotAuthenticateEmailDoesNotExist(){
        setup();
        Mockito.when(accountRepository.findByActiveTrueAndDeletedFalseAndEmail(emailNotExists)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> libAccountService.authenticate(emailNotExists, password))
                .isInstanceOf(GenericException.class)
                .hasMessage(ResponseMessage.CREDENTIALS_INCORRECT.getMessage());
    }

    @Test
    @DisplayName("Should not authenticate because the password does not match")
    public void shouldNotAuthenticatePasswordDoesNotMatch(){
        setup();
        Mockito.when(accountRepository.findByActiveTrueAndDeletedFalseAndEmail(emailExists)).thenReturn(Optional.of(generateAccount(emailExists, password)));

        assertThatThrownBy(() -> libAccountService.authenticate(emailExists, password))
                .isInstanceOf(GenericException.class)
                .hasMessage(ResponseMessage.CREDENTIALS_INCORRECT.getMessage());
    }

    @Test
    @DisplayName("Should authenticate account with its email and password")
    public void shouldAuthenticateWithEmailAndPassword() throws GenericException {
        setup();
        Mockito.when(accountRepository.findByActiveTrueAndDeletedFalseAndEmail(emailExists)).thenReturn(Optional.of(generateAccount(emailExists, securePassword)));

        Account account = libAccountService.authenticate(emailExists, password);
        assertThat(account.getPassword()).isEqualTo(securePassword);
        assertThat(account.getEmail()).isEqualTo(emailExists);
        assertThat(account.getFirstname()).isEqualTo("Belinda");
        assertThat(account.getLastname()).isEqualTo("MENGUE");
        assertThat(account.isActive()).isTrue();
    }
}
