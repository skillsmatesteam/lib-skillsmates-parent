package com.yamo.skillsmates.mapper.admin;

import com.yamo.skillsmates.dto.admin.config.CategoryDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.admin.config.Category;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class CategoryMapper {

    private static CategoryMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return CategoryMapper
     */
    public static CategoryMapper getInstance() {
        if (singleton == null) {
            singleton = new CategoryMapper();
        }
        return singleton;
    }

    /**
     * maps list of categories into list of CategoryDto
     *
     * @param categories list of permission to be mapped
     * @return list of CategoryDto mapped
     */
    public List<CategoryDto> asDtos(List<Category> categories){
        List<CategoryDto> categoryDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(categories)){
            for (Category category: categories){
                categoryDtos.add(GenericMapper.INSTANCE.asDto(category));
            }
        }
        return categoryDtos;
    }
}
