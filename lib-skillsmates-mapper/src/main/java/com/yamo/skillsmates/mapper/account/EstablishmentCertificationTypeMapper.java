package com.yamo.skillsmates.mapper.account;


import com.yamo.skillsmates.dto.account.config.EstablishmentCertificationTypeDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.config.EstablishmentCertificationType;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class EstablishmentCertificationTypeMapper {

    private static EstablishmentCertificationTypeMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return EstablishmentCertificationTypeMapper
     */
    public static EstablishmentCertificationTypeMapper getInstance() {
        if (singleton == null) {
            singleton = new EstablishmentCertificationTypeMapper();
        }
        return singleton;
    }

    /**
     * maps list of establishmentCertificationTypes into list of EstablishmentCertificationTypeDto
     *
     * @param establishmentCertificationTypes list of permission to be mapped
     * @return list of EstablishmentCertificationTypeDto mapped
     */
    public List<EstablishmentCertificationTypeDto> asDtos(List<EstablishmentCertificationType> establishmentCertificationTypes){
        List<EstablishmentCertificationTypeDto> establishmentCertificationTypeDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(establishmentCertificationTypes)){
            for (EstablishmentCertificationType establishmentCertificationType: establishmentCertificationTypes){
                establishmentCertificationTypeDtos.add(GenericMapper.INSTANCE.asDto(establishmentCertificationType));
            }
        }
        return establishmentCertificationTypeDtos;
    }
}
