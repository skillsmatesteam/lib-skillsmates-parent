package com.yamo.skillsmates.mapper.account;

import com.yamo.skillsmates.dto.account.CertificationDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.Certification;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class CertificationMapper {

    private static CertificationMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return CertificationMapper
     */
    public static CertificationMapper getInstance() {
        if (singleton == null) {
            singleton = new CertificationMapper();
        }
        return singleton;
    }

    /**
     * maps list of certification into list of certificationDto
     *
     * @param certification list of permission to be mapped
     * @return list of CertificationDto mapped
     */
    public List<CertificationDto> asDtos(List<Certification>  certification){
        List<CertificationDto>  certificationDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(certification)){
            for (Certification certificationOb: certification){
                certificationDtos.add(GenericMapper.INSTANCE.asDto(certificationOb));
            }
        }
        return certificationDtos;
    }
}
