package com.yamo.skillsmates.mapper.notification;

import com.yamo.skillsmates.common.enumeration.NotificationTypeEnum;
import com.yamo.skillsmates.dto.notifications.NotificationDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.communication.Message;
import com.yamo.skillsmates.models.notifications.Notification;
import lombok.NoArgsConstructor;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class NotificationMapper {

    private static NotificationMapper singleton;

    public static NotificationMapper getInstance() {

        if (singleton == null) {
            singleton = new NotificationMapper();
        }
        return singleton;
    }

    public NotificationDto asDto(Notification notification){
        NotificationDto notificationDto = GenericMapper.INSTANCE.asDto(notification);
        notificationDto.setType(notification.getNotificationType().getLabel());
        notificationDto.setElement(notification.getNotificationMap().getIdElement());
        return notificationDto;
    }

    public List<NotificationDto> asDtos(List<Notification> notifications){
        List<NotificationDto> notificationDtos = new ArrayList<>();
        if (notifications != null){
            for (Notification notification: notifications){
                notificationDtos.add(asDto(notification));
            }
        }
        return notificationDtos;
    }

//    /**
//     * convert social interaction into notification
//     * @param socialInteraction social interaction
//     * @return notification dto
//     */
//    public NotificationDto asNotification(SocialInteraction socialInteraction){
//        NotificationDto notificationDto = new NotificationDto();
//        notificationDto.setId(socialInteraction.getId());
//        notificationDto.setIdServer(socialInteraction.getIdServer());
//        notificationDto.setActive(socialInteraction.isActive());
//        notificationDto.setDeleted(socialInteraction.isDeleted());
//        notificationDto.setRevision(socialInteraction.getRevision());
//        notificationDto.setTitle(socialInteraction.getContent());
//        notificationDto.setEmitterAccount(GenericMapper.INSTANCE.asDto(socialInteraction.getEmitterAccount()));
//        notificationDto.setReceiverAccount(GenericMapper.INSTANCE.asDto(socialInteraction.getReceiverAccount()));
//        notificationDto.setType(socialInteraction.getSocialInteractionType().getLabel());
//        notificationDto.setElement(socialInteraction.getSocialInteractionMap().getElement());
//        return notificationDto;
//    }

    /**
     * convert message into notification
     * @param message message
     * @return notification dto
     */
    public NotificationDto asNotification(Message message){
        NotificationDto notificationDto = new NotificationDto();
        notificationDto.setId(message.getId());
        notificationDto.setIdServer(message.getIdServer());
        notificationDto.setActive(message.isActive());
        notificationDto.setDeleted(message.isDeleted());
        notificationDto.setRevision(message.getRevision());
        notificationDto.setTitle(message.getContent());
        notificationDto.setEmitterAccount(GenericMapper.INSTANCE.asDto(message.getEmitterAccount()));
        notificationDto.setReceiverAccount(GenericMapper.INSTANCE.asDto(message.getReceiverAccount()));
        notificationDto.setType(NotificationTypeEnum.MESSAGE.name());
        notificationDto.setElement(message.getIdServer());
        return notificationDto;
    }

//    /**
//     * convert list of social interactions into a list of notifications
//     * @param socialInteractions: list of social interactions
//     * @return list of notifications
//     */
//    public List<NotificationDto> asNotifications(List<SocialInteraction> socialInteractions){
//        List<NotificationDto> notificationsDtos = new ArrayList<>();
//        if (CollectionUtils.isNotEmpty(socialInteractions)){
//            socialInteractions.forEach(socialInteraction -> notificationsDtos.add(asNotification(socialInteraction)));
//        }
//        return notificationsDtos;
//    }

    /**
     * convert list of messages into a list of notifications
     * @param messages: list of messages
     * @return list of notifications
     */
    public List<NotificationDto> asNotifications(List<Message> messages){
        List<NotificationDto> notificationsDtos = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(messages)){
            messages.forEach(message -> notificationsDtos.add(asNotification(message)));
        }
        return notificationsDtos;
    }
}
