package com.yamo.skillsmates.mapper.account;

import com.yamo.skillsmates.dto.account.config.DisciplineDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.config.Discipline;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class DisciplineMapper {

    private static DisciplineMapper singleton;

    public static DisciplineMapper getInstance() {
        if (singleton == null) {
            singleton = new DisciplineMapper();
        }
        return singleton;
    }

    public List<DisciplineDto> asDtos(List<Discipline> disciplines){
        List<DisciplineDto> disciplineDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(disciplines)){
            for (Discipline discipline: disciplines){
                disciplineDtos.add(GenericMapper.INSTANCE.asDto(discipline));
            }
        }
        return disciplineDtos;
    }
}
