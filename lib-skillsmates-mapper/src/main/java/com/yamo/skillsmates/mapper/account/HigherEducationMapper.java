package com.yamo.skillsmates.mapper.account;


import com.yamo.skillsmates.dto.account.HigherEducationDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.HigherEducation;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class HigherEducationMapper {

    private static HigherEducationMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return HigherEducationMapper
     */
    public static HigherEducationMapper getInstance() {
        if (singleton == null) {
            singleton = new HigherEducationMapper();
        }
        return singleton;
    }

    /**
     * maps list of higherEducations into list of HigherEducationDto
     *
     * @param higherEducations list of permission to be mapped
     * @return list of HigherEducationDto mapped
     */
    public List<HigherEducationDto> asDtos(List<HigherEducation> higherEducations){
        List<HigherEducationDto> higherEducationDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(higherEducations)){
            for (HigherEducation higherEducation: higherEducations){
                higherEducationDtos.add(GenericMapper.INSTANCE.asDto(higherEducation));
            }
        }
        return higherEducationDtos;
    }
}
