package com.yamo.skillsmates.mapper.account;

import com.yamo.skillsmates.dto.account.AccountDto;
import com.yamo.skillsmates.dto.account.DegreeObtainedDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.DegreeObtained;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class DegreeObtainedMapper {

    private static DegreeObtainedMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return AccountMapper
     */
    public static DegreeObtainedMapper getInstance() {
        if (singleton == null) {
            singleton = new DegreeObtainedMapper();
        }
        return singleton;
    }

    /**
     * maps list of adegreeObtained into list of DegreeObtainedDto
     *
     * @param degreeObtained list of permission to be mapped
     * @return list of DegreeObtainedDto mapped
     */
    public List<DegreeObtainedDto> asDtos(List<DegreeObtained>  degreeObtained){
        List<DegreeObtainedDto>  degreeObtainedDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(degreeObtained)){
            for (DegreeObtained degreeOb: degreeObtained){
                degreeObtainedDtos.add(GenericMapper.INSTANCE.asDto(degreeOb));
            }
        }
        return degreeObtainedDtos;
    }
}
