package com.yamo.skillsmates.mapper.account;

import com.yamo.skillsmates.dto.account.config.StudyLevelDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.config.StudyLevel;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class StudyLevelMapper {

    private static StudyLevelMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return StudyLevelMapper
     */
    public static StudyLevelMapper getInstance() {
        if (singleton == null) {
            singleton = new StudyLevelMapper();
        }
        return singleton;
    }

    /**
     * maps list of studyLevels into list of StudyLevelDto
     *
     * @param studyLevels list of permission to be mapped
     * @return list of StudyLevelDto mapped
     */
    public List<StudyLevelDto> asDtos(List<StudyLevel> studyLevels){
        List<StudyLevelDto> studyLevelDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(studyLevels)){
            for (StudyLevel studyLevel: studyLevels){
                studyLevelDtos.add(GenericMapper.INSTANCE.asDto(studyLevel));
            }
        }
        return studyLevelDtos;
    }
}
