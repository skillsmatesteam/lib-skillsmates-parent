package com.yamo.skillsmates.mapper.account;


import com.yamo.skillsmates.dto.account.config.EstablishmentTypeDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.config.EstablishmentType;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class EstablishmentTypeMapper {

    private static EstablishmentTypeMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return EstablishmentTypeMapper
     */
    public static EstablishmentTypeMapper getInstance() {
        if (singleton == null) {
            singleton = new EstablishmentTypeMapper();
        }
        return singleton;
    }

    /**
     * maps list of establishmentTypes into list of EstablishmentTypeDto
     *
     * @param establishmentTypes list of permission to be mapped
     * @return list of EstablishmentTypeDto mapped
     */
    public List<EstablishmentTypeDto> asDtos(List<EstablishmentType> establishmentTypes){
        List<EstablishmentTypeDto> establishmentTypeDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(establishmentTypes)){
            for (EstablishmentType establishmentType: establishmentTypes){
                establishmentTypeDtos.add(GenericMapper.INSTANCE.asDto(establishmentType));
            }
        }
        return establishmentTypeDtos;
    }
}
