package com.yamo.skillsmates.mapper.account;

import com.yamo.skillsmates.dto.post.PostDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.post.Post;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class PostMapper {

    private static PostMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return PostMapper
     */
    public static PostMapper getInstance() {
        if (singleton == null) {
            singleton = new PostMapper();
        }
        return singleton;
    }

    public List<PostDto> asDtos(List<Post> posts){
        List<PostDto> postDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(posts)){
            for (Post post: posts){
                postDtos.add(GenericMapper.INSTANCE.asDto(post));
            }
        }
        return postDtos;
    }
}
