package com.yamo.skillsmates.mapper.admin;

import com.yamo.skillsmates.dto.admin.PartnerDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.admin.Partner;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class PartnerMapper {

    private static PartnerMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return PartnerMapper
     */
    public static PartnerMapper getInstance() {
        if (singleton == null) {
            singleton = new PartnerMapper();
        }
        return singleton;
    }

    /**
     * maps list of partners into list of PartnerDto
     *
     * @param partners list of permission to be mapped
     * @return list of PartnerDto mapped
     */
    public List<PartnerDto> asDtos(List<Partner> partners){
        List<PartnerDto> partnerDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(partners)){
            for (Partner partner: partners){
                partnerDtos.add(GenericMapper.INSTANCE.asDto(partner));
            }
        }
        return partnerDtos;
    }
}
