package com.yamo.skillsmates.mapper.admin;

import com.yamo.skillsmates.dto.admin.ActivityDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.admin.Activity;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class ActivityMapper {

    private static ActivityMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return ActivityMapper
     */
    public static ActivityMapper getInstance() {
        if (singleton == null) {
            singleton = new ActivityMapper();
        }
        return singleton;
    }

    /**
     * maps list of activities into list of ActivityDto
     *
     * @param activities list of permission to be mapped
     * @return list of ActivityDto mapped
     */
    public List<ActivityDto> asDtos(List<Activity> activities){
        List<ActivityDto> activityDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(activities)){
            for (Activity activity: activities){
                activityDtos.add(GenericMapper.INSTANCE.asDto(activity));
            }
        }
        return activityDtos;
    }
}
