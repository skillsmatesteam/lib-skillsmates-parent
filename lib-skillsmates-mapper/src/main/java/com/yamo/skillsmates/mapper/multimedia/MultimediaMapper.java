package com.yamo.skillsmates.mapper.multimedia;

import com.yamo.skillsmates.dto.multimedia.MultimediaDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.multimedia.Multimedia;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class MultimediaMapper {

    private static MultimediaMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return MultimediaMapper
     */
    public static MultimediaMapper getInstance() {

        if (singleton == null) {
            singleton = new MultimediaMapper();
        }
        return singleton;
    }

    /**
     * maps list of multimedias into list of MultimediaDto
     *
     * @param multimedias list of multimedia to be mapped
     * @return list of MultimediaDto mapped
     */
    public List<MultimediaDto> asDtos(List<Multimedia> multimedias){
        List<MultimediaDto> multimediaDtos = new ArrayList<>();
        if (multimedias != null){
            for (Multimedia multimedia : multimedias){
                multimediaDtos.add(GenericMapper.INSTANCE.asDto(multimedia));
            }
        }
        return multimediaDtos;
    }
}
