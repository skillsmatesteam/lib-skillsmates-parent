package com.yamo.skillsmates.mapper.post;

import com.yamo.skillsmates.dto.multimedia.MultimediaDto;
import com.yamo.skillsmates.dto.post.PostDto;
import com.yamo.skillsmates.dto.post.PostSocialInteractionsDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.multimedia.Multimedia;
import com.yamo.skillsmates.models.post.PostSocialInteractions;
import com.yamo.skillsmates.models.post.Post;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class PostMapper {

    private static PostMapper singleton;

    public static PostMapper getInstance() {
        if (singleton == null) {
            singleton = new PostMapper();
        }
        return singleton;
    }

    public List<PostDto> asDtos(List<Post> posts){
        List<PostDto> postDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(posts)){
            for (Post post: posts){
                postDtos.add(GenericMapper.INSTANCE.asDto(post));
            }
        }
        return postDtos;
    }

    public List<PostSocialInteractionsDto> asPostSocialInteractionsDtos(List<PostSocialInteractions> postsSocialInteractions){
        List<PostSocialInteractionsDto> postSocialInteractionsDtos = new ArrayList<>();
        for (PostSocialInteractions postSocialInteraction: postsSocialInteractions){
            postSocialInteractionsDtos.add(asPostSocialInteractionsDto(postSocialInteraction));
        }
        return postSocialInteractionsDtos;
    }

    public PostSocialInteractionsDto asPostSocialInteractionsDto(PostSocialInteractions postSocialInteractions){
            PostSocialInteractionsDto postSocialInteractionsDto = new PostSocialInteractionsDto();
            postSocialInteractionsDto.setPost(GenericMapper.INSTANCE.asDto(postSocialInteractions.getPost()));
            List<MultimediaDto> multimediaDtos = new ArrayList<>();
            multimediaDtos.add(GenericMapper.INSTANCE.asDto(postSocialInteractions.getPost().getMultimedia()));
            postSocialInteractionsDto.getPost().setMultimedia(multimediaDtos);
            postSocialInteractionsDto.setSocialInteractions(SocialInteractionMapper.getInstance().asDtos(postSocialInteractions.getSocialInteractions()));
            postSocialInteractionsDto.setShared(postSocialInteractions.isShared());
            postSocialInteractionsDto.setShareSocialInteraction(GenericMapper.INSTANCE.asDto(postSocialInteractions.getShareSocialInteraction()));
        return postSocialInteractionsDto;
    }
}
