package com.yamo.skillsmates.mapper.account;

import com.yamo.skillsmates.dto.account.SkillDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.Skill;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class SkillMapper {

    private static SkillMapper singleton;

    public static SkillMapper getInstance() {
        if (singleton == null) {
            singleton = new SkillMapper();
        }
        return singleton;
    }

    public List<SkillDto> asDtos(List<Skill> skills){
        List<SkillDto> skillDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(skills)){
            for (Skill skill: skills){
                skillDtos.add(GenericMapper.INSTANCE.asDto(skill));
            }
        }
        return skillDtos;
    }
}
