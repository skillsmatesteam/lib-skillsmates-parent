package com.yamo.skillsmates.mapper.multimedia;

import com.yamo.skillsmates.dto.multimedia.config.MediaSubtypeDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.multimedia.config.MediaSubtype;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class MediaSubtypeMapper {

    private static MediaSubtypeMapper singleton;

    public static MediaSubtypeMapper getInstance() {

        if (singleton == null) {
            singleton = new MediaSubtypeMapper();
        }
        return singleton;
    }

    public List<MediaSubtypeDto> asDtos(List<MediaSubtype> mediaSubtypes){
        List<MediaSubtypeDto> mediaSubtypeDtos = new ArrayList<>();
        if (mediaSubtypes != null){
            for (MediaSubtype mediaSubType : mediaSubtypes){
                mediaSubtypeDtos.add(GenericMapper.INSTANCE.asDto(mediaSubType));
            }
        }
        return mediaSubtypeDtos;
    }
}
