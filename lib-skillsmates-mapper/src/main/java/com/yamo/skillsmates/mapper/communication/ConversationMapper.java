package com.yamo.skillsmates.mapper.communication;

import com.yamo.skillsmates.dto.message.ConversationDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.communication.Conversation;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class ConversationMapper {

    private static ConversationMapper singleton;

    public static ConversationMapper getInstance() {
        if (singleton == null) {
            singleton = new ConversationMapper();
        }
        return singleton;
    }

    public List<ConversationDto> asDtos(List<Conversation> conversations){
        List<ConversationDto> conversationDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(conversations)){
            for (Conversation conversation : conversations){
                conversationDtos.add(GenericMapper.INSTANCE.asDto(conversation));
            }
        }
        return conversationDtos;
    }
}
