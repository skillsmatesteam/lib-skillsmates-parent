package com.yamo.skillsmates.mapper.account;

import com.yamo.skillsmates.dto.account.AccountDto;
import com.yamo.skillsmates.dto.account.AccountSocialInteractionsDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.mapper.post.SocialInteractionMapper;
import com.yamo.skillsmates.models.account.*;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class AccountMapper {

    private static AccountMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return AccountMapper
     */
    public static AccountMapper getInstance() {
        if (singleton == null) {
            singleton = new AccountMapper();
        }
        return singleton;
    }

    /**
     * maps list of accounts into list of AccountDto
     *
     * @param accounts list of permission to be mapped
     * @return list of AccountDto mapped
     */
    public List<AccountDto> asDtos(List<Account> accounts){
        List<AccountDto> accountDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(accounts)){
            for (Account account: accounts){
                accountDtos.add(GenericMapper.INSTANCE.asDto(account));
            }
        }
        return accountDtos;
    }

    public List<AccountSocialInteractionsDto> asAccountSocialInteractionsDtos(List<AccountSocialInteractions> accountsSocialInteractions){
        List<AccountSocialInteractionsDto> accountSocialInteractionsDtos = new ArrayList<>();
        for (AccountSocialInteractions accountSocialInteraction: accountsSocialInteractions){
            accountSocialInteractionsDtos.add(asAccountSocialInteractionsDto(accountSocialInteraction));
        }
        return accountSocialInteractionsDtos;
    }

    public AccountSocialInteractionsDto asAccountSocialInteractionsDto(AccountSocialInteractions accountSocialInteractions){
        AccountSocialInteractionsDto accountSocialInteractionsDto = new AccountSocialInteractionsDto();
        accountSocialInteractionsDto.setAccount(GenericMapper.INSTANCE.asDto(accountSocialInteractions.getAccount()));
        accountSocialInteractionsDto.setSocialInteractions(SocialInteractionMapper.getInstance().asDtos(accountSocialInteractions.getSocialInteractions()));
        accountSocialInteractionsDto.setNumberPosts(accountSocialInteractions.getNumberPosts());
        return accountSocialInteractionsDto;
    }
}
