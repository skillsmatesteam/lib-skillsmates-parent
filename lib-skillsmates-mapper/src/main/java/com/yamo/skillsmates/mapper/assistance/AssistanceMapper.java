package com.yamo.skillsmates.mapper.assistance;

import com.yamo.skillsmates.dto.assistance.AssistanceDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.assistance.Assistance;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class AssistanceMapper {

    private static AssistanceMapper singleton;

    public static AssistanceMapper getInstance() {
        if (singleton == null) {
            singleton = new AssistanceMapper();
        }
        return singleton;
    }

    public List<AssistanceDto> asDtos(List<Assistance> assistances){
        List<AssistanceDto> assistanceDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(assistances)){
            for (Assistance assistance: assistances){
                assistanceDtos.add(GenericMapper.INSTANCE.asDto(assistance));
            }
        }
        return assistanceDtos;
    }

}
