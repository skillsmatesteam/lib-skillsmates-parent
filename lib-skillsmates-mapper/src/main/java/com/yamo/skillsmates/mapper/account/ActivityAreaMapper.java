package com.yamo.skillsmates.mapper.account;

import com.yamo.skillsmates.dto.account.config.ActivityAreaDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.config.ActivityArea;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class ActivityAreaMapper {

    private static ActivityAreaMapper singleton;

    public static ActivityAreaMapper getInstance() {
        if (singleton == null) {
            singleton = new ActivityAreaMapper();
        }
        return singleton;
    }

    public List<ActivityAreaDto> asDtos(List<ActivityArea> activityAreas){
        List<ActivityAreaDto> activityAreaDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(activityAreas)){
            for (ActivityArea activityArea: activityAreas){
                activityAreaDtos.add(GenericMapper.INSTANCE.asDto(activityArea));
            }
        }
        return activityAreaDtos;
    }
}
