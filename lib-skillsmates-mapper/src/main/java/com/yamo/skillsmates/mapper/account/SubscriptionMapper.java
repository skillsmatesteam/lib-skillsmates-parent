package com.yamo.skillsmates.mapper.account;

import com.yamo.skillsmates.dto.account.SubscriptionDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.Subscription;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class SubscriptionMapper {

    private static SubscriptionMapper singleton;

    public static SubscriptionMapper getInstance() {
        if (singleton == null) {
            singleton = new SubscriptionMapper();
        }
        return singleton;
    }

    public List<SubscriptionDto> asDtos(List<Subscription> subscriptions){
        List<SubscriptionDto> subscriptionDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(subscriptions)){
            for (Subscription subscription: subscriptions){
                subscriptionDtos.add(GenericMapper.INSTANCE.asDto(subscription));
            }
        }
        return subscriptionDtos;
    }
}
