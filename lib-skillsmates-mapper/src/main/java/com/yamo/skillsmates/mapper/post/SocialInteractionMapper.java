package com.yamo.skillsmates.mapper.post;

import com.yamo.skillsmates.dto.post.SocialInteractionDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.post.SocialInteraction;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class SocialInteractionMapper {

    private static SocialInteractionMapper singleton;

    public static SocialInteractionMapper getInstance() {
        if (singleton == null) {
            singleton = new SocialInteractionMapper();
        }
        return singleton;
    }

    public SocialInteractionDto asDto(SocialInteraction socialInteraction){
        SocialInteractionDto socialInteractionDto = GenericMapper.INSTANCE.asDto(socialInteraction);
        socialInteractionDto.setElement(socialInteraction.getSocialInteractionMap().getElement());
        socialInteractionDto.setType(socialInteraction.getSocialInteractionType().getLabel());
        return socialInteractionDto;
    }

    public List<SocialInteractionDto> asDtos(List<SocialInteraction> socialInteractions){
        List<SocialInteractionDto> socialInteractionDtos = new ArrayList<>();
        if (socialInteractions != null){
            for (SocialInteraction socialInteraction: socialInteractions){
                SocialInteractionDto socialInteractionDto = GenericMapper.INSTANCE.asDto(socialInteraction);
                socialInteractionDto.setType(socialInteraction.getSocialInteractionType().getLabel());
                socialInteractionDto.setElement(socialInteraction.getSocialInteractionMap().getElement());
                socialInteractionDtos.add(socialInteractionDto);
            }
        }
        return socialInteractionDtos;
    }
}
