package com.yamo.skillsmates.mapper.account;


import com.yamo.skillsmates.dto.account.config.DiplomaDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.config.Diploma;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class DiplomaMapper {

    private static DiplomaMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return PreparedDiplomaMapper
     */
    public static DiplomaMapper getInstance() {
        if (singleton == null) {
            singleton = new DiplomaMapper();
        }
        return singleton;
    }

    /**
     * maps list of preparedDiplomas into list of PreparedDiplomaDto
     *
     * @param diplomas list of permission to be mapped
     * @return list of PreparedDiplomaDto mapped
     */
    public List<DiplomaDto> asDtos(List<Diploma> diplomas){
        List<DiplomaDto> diplomaDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(diplomas)){
            for (Diploma diploma : diplomas){
                diplomaDtos.add(GenericMapper.INSTANCE.asDto(diploma));
            }
        }
        return diplomaDtos;
    }
}
