package com.yamo.skillsmates.mapper.account;

import com.yamo.skillsmates.dto.account.AccountDto;
import com.yamo.skillsmates.dto.account.config.EducationDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.Account;
import com.yamo.skillsmates.models.account.config.Education;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class EducationMapper {

    private static EducationMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return AccountMapper
     */
    public static EducationMapper getInstance() {
        if (singleton == null) {
            singleton = new EducationMapper();
        }
        return singleton;
    }

    /**
     * maps list of educations into list of AccountDto
     *
     * @param educations list of permission to be mapped
     * @return list of EducationDto mapped
     */
    public List<EducationDto> asDtos(List<Education> educations){
        List<EducationDto> educationDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(educations)){
            for (Education education: educations){
                educationDtos.add(GenericMapper.INSTANCE.asDto(education));
            }
        }
        return educationDtos;
    }
}
