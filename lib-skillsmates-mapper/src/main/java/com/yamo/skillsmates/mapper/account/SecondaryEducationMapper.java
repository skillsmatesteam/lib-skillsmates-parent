package com.yamo.skillsmates.mapper.account;


import com.yamo.skillsmates.dto.account.SecondaryEducationDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.SecondaryEducation;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class SecondaryEducationMapper {

    private static SecondaryEducationMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return SecondaryEducationMapper
     */
    public static SecondaryEducationMapper getInstance() {
        if (singleton == null) {
            singleton = new SecondaryEducationMapper();
        }
        return singleton;
    }

    /**
     * maps list of secondaryEducations into list of SecondaryEducationDto
     *
     * @param secondaryEducations list of permission to be mapped
     * @return list of SecondaryEducationDto mapped
     */
    public List<SecondaryEducationDto> asDtos(List<SecondaryEducation> secondaryEducations){
        List<SecondaryEducationDto> secondaryEducationDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(secondaryEducations)){
            for (SecondaryEducation secondaryEducation: secondaryEducations){
                secondaryEducationDtos.add(GenericMapper.INSTANCE.asDto(secondaryEducation));
            }
        }
        return secondaryEducationDtos;
    }
}
