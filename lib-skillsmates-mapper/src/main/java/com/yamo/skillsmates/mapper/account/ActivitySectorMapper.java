package com.yamo.skillsmates.mapper.account;

import com.yamo.skillsmates.dto.account.config.ActivitySectorDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.config.ActivitySector;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class ActivitySectorMapper {

    private static ActivitySectorMapper singleton;

    public static ActivitySectorMapper getInstance() {
        if (singleton == null) {
            singleton = new ActivitySectorMapper();
        }
        return singleton;
    }

    public List<ActivitySectorDto> asDtos(List<ActivitySector> activitySectors){
        List<ActivitySectorDto> activitySectorDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(activitySectors)){
            for (ActivitySector activitySector: activitySectors){
                activitySectorDtos.add(GenericMapper.INSTANCE.asDto(activitySector));
            }
        }
        return activitySectorDtos;
    }
}
