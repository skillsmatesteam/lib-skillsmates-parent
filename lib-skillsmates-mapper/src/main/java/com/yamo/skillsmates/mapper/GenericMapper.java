package com.yamo.skillsmates.mapper;

import com.yamo.skillsmates.dto.account.*;
import com.yamo.skillsmates.dto.account.config.*;
import com.yamo.skillsmates.dto.admin.ActivityDto;
import com.yamo.skillsmates.dto.admin.PartnerDto;
import com.yamo.skillsmates.dto.admin.config.CategoryDto;
import com.yamo.skillsmates.dto.admin.config.PartnerTypeDto;
import com.yamo.skillsmates.dto.assistance.AssistanceDto;
import com.yamo.skillsmates.dto.message.ConversationDto;
import com.yamo.skillsmates.dto.message.MessageDto;
import com.yamo.skillsmates.dto.multimedia.MetadataDto;
import com.yamo.skillsmates.dto.multimedia.MultimediaDto;
import com.yamo.skillsmates.dto.multimedia.config.InfosMediaTypeDto;
import com.yamo.skillsmates.dto.multimedia.config.MediaSubtypeDto;
import com.yamo.skillsmates.dto.multimedia.config.MediaTypeDto;
import com.yamo.skillsmates.dto.notifications.NotificationDto;
import com.yamo.skillsmates.dto.post.PostDto;
import com.yamo.skillsmates.dto.post.SocialInteractionDto;
import com.yamo.skillsmates.dto.post.SocialInteractionTypeDto;
import com.yamo.skillsmates.dto.version.VersionDto;
import com.yamo.skillsmates.models.account.*;
import com.yamo.skillsmates.models.account.config.*;
import com.yamo.skillsmates.models.admin.Activity;
import com.yamo.skillsmates.models.admin.Partner;
import com.yamo.skillsmates.models.admin.config.Category;
import com.yamo.skillsmates.models.admin.config.PartnerType;
import com.yamo.skillsmates.models.assistance.Assistance;
import com.yamo.skillsmates.models.communication.Conversation;
import com.yamo.skillsmates.models.communication.Message;
import com.yamo.skillsmates.models.multimedia.Metadata;
import com.yamo.skillsmates.models.multimedia.Multimedia;
import com.yamo.skillsmates.models.multimedia.config.InfosMediaType;
import com.yamo.skillsmates.models.multimedia.config.MediaSubtype;
import com.yamo.skillsmates.models.multimedia.config.MediaType;
import com.yamo.skillsmates.models.notifications.Notification;
import com.yamo.skillsmates.models.post.Post;
import com.yamo.skillsmates.models.post.SocialInteraction;
import com.yamo.skillsmates.models.post.SocialInteractionType;
import com.yamo.skillsmates.models.version.Version;
import fr.xebia.extras.selma.IgnoreMissing;
import fr.xebia.extras.selma.Mapper;
import fr.xebia.extras.selma.Maps;
import fr.xebia.extras.selma.Selma;

@Mapper(withIgnoreMissing = IgnoreMissing.ALL)
public interface GenericMapper {

    /**
     * Instance of the mapper
     */
    GenericMapper INSTANCE = Selma.getMapper(GenericMapper.class);

    Account asEntity(AccountDto accountDto);
    DegreeObtained asEntity(DegreeObtainedDto degreeObtainedDto);
    HigherEducation asEntity(HigherEducationDto higherEducationDto);
    Professional asEntity(ProfessionalDto professionalDto);
    SecondaryEducation asEntity(SecondaryEducationDto secondaryEducationcursusDto);
    Skill asEntity(SkillDto skillDto);
    ActivityArea asEntity(ActivityAreaDto activityAreaDto);
    Diploma asEntity(DiplomaDto diplomaDto);
    ActivitySector asEntity(ActivitySectorDto activitySectorDto);
    Discipline asEntity(DisciplineDto disciplineDto);
    Education asEntity(EducationDto educationDto);
    EstablishmentType asEntity(EstablishmentTypeDto establishmentTypeDto);
    Level asEntity(LevelDto levelDto);
    StudyLevel asEntity(StudyLevelDto studyLevelDto);
    TeachingArea asEntity(TeachingAreaDto teachingAreaDto);
    @Maps(withIgnoreFields = {"multimedia"})
    Post asEntity(PostDto postDto);
    InfosMediaType asEntity(InfosMediaTypeDto infosMediaTypeDto);
    MediaType asEntity(MediaTypeDto mediaTypeDto);
    MediaSubtype asEntity(MediaSubtypeDto mediaSubtypeDto);
    TeachingAreaGroup asEntity(TeachingAreaGroupDto teachingAreaGroupDto);
    TeachingAreaSet asEntity(TeachingAreaSetDto teachingAreaSetDto);
    Subscription asEntity(SubscriptionDto subscriptionDto);
    Notification asEntity(NotificationDto notificationDto);
    Metadata asEntity(MetadataDto metadataDto);
    Message asEntity(MessageDto messageDto);
    Conversation asEntity(ConversationDto conversationDto);
    Version asEntity(VersionDto versionDto);
    SocialInteraction asEntity(SocialInteractionDto socialInteractionDto);
    SocialInteractionType asEntity(SocialInteractionTypeDto socialInteractionTypeDto);
    Assistance asEntity(AssistanceDto assistanceDto);
    Partner asEntity(PartnerDto partnerDto);
    PartnerType asEntity(PartnerTypeDto partnerTypeDto);
    Category asEntity(CategoryDto categoryDto);
    Activity asEntity(ActivityDto activityDto);
    Certification asEntity(CertificationDto certificationDto);
    EstablishmentCertificationType asEntity(EstablishmentCertificationTypeDto establishmentCertificationTypeDto);

    @Maps(withIgnoreFields = {"password"})
    AccountDto asDto(Account account);
    DegreeObtainedDto asDto(DegreeObtained degreeObtained);
    HigherEducationDto asDto(HigherEducation higherEducation);
    ProfessionalDto asDto(Professional professional);
    SecondaryEducationDto asDto(SecondaryEducation secondaryEducation);
    SkillDto asDto(Skill skill);
    ActivityAreaDto asDto(ActivityArea activityArea);
    DiplomaDto asDto(Diploma diploma);
    ActivitySectorDto asDto(ActivitySector activitySector);
    DisciplineDto asDto(Discipline discipline);
    EducationDto asDto(Education education);
    EstablishmentTypeDto asDto(EstablishmentType establishmentType);
    LevelDto asDto(Level level);
    StudyLevelDto asDto(StudyLevel studyLevel);
    TeachingAreaDto asDto(TeachingArea teachingArea);
    MultimediaDto asDto(Multimedia multimedia);
    @Maps(withIgnoreFields = {"multimedia"})
    PostDto asDto(Post post);
    InfosMediaTypeDto asDto(InfosMediaType infosMediaType);
    MediaTypeDto asDto(MediaType mediaType);
    MediaSubtypeDto asDto(MediaSubtype mediaSubType);
    TeachingAreaSetDto asDto(TeachingAreaSet teachingAreaSet);
    TeachingAreaGroupDto asDto(TeachingAreaGroup teachingAreaGroup);
    SubscriptionDto asDto(Subscription subscription);
    NotificationDto asDto(Notification notification);
    MetadataDto asDto(Metadata metadata);
    MessageDto asDto(Message message);
    ConversationDto asDto(Conversation conversation);
    VersionDto asDto(Version version);
    SocialInteractionDto asDto(SocialInteraction socialInteraction);
    SocialInteractionTypeDto asDto(SocialInteractionType socialInteractionType);
    AssistanceDto asDto(Assistance assistance);
    PartnerDto asDto(Partner partner);
    PartnerTypeDto asDto(PartnerType partnerType);
    CategoryDto asDto(Category category);
    ActivityDto asDto(Activity activity);
    CertificationDto asDto(Certification certification);
    EstablishmentCertificationTypeDto asDto(EstablishmentCertificationType establishmentCertificationType);
}
