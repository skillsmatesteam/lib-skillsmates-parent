package com.yamo.skillsmates.mapper.communication;

import com.yamo.skillsmates.dto.message.MessageDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.communication.Message;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class MessageMapper {

    private static MessageMapper singleton;

    public static MessageMapper getInstance() {
        if (singleton == null) {
            singleton = new MessageMapper();
        }
        return singleton;
    }

    public List<MessageDto> asDtos(List<Message> messages){
        List<MessageDto> messageDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(messages)){
            for (Message message: messages){
                messageDtos.add(GenericMapper.INSTANCE.asDto(message));
            }
        }
        return messageDtos;
    }

}
