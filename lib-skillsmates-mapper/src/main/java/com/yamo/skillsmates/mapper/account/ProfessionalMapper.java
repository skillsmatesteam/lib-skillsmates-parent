package com.yamo.skillsmates.mapper.account;

import com.yamo.skillsmates.dto.account.ProfessionalDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.Professional;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class ProfessionalMapper {

    private static ProfessionalMapper singleton;

    public static ProfessionalMapper getInstance() {
        if (singleton == null) {
            singleton = new ProfessionalMapper();
        }
        return singleton;
    }

    public List<ProfessionalDto> asDtos(List<Professional> professionals){
        List<ProfessionalDto> professionalDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(professionals)){
            for (Professional professional: professionals){
                professionalDtos.add(GenericMapper.INSTANCE.asDto(professional));
            }
        }
        return professionalDtos;
    }
}
