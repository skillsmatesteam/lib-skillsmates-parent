package com.yamo.skillsmates.mapper.account;

import com.yamo.skillsmates.dto.account.config.LevelDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.config.Level;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class LevelMapper {

    private static LevelMapper singleton;

    public static LevelMapper getInstance() {
        if (singleton == null) {
            singleton = new LevelMapper();
        }
        return singleton;
    }

    public List<LevelDto> asDtos(List<Level> levels){
        List<LevelDto> levelDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(levels)){
            for (Level level: levels){
                levelDtos.add(GenericMapper.INSTANCE.asDto(level));
            }
        }
        return levelDtos;
    }
}
