package com.yamo.skillsmates.mapper.account;


import com.yamo.skillsmates.dto.account.config.TeachingAreaDto;
import com.yamo.skillsmates.mapper.GenericMapper;
import com.yamo.skillsmates.models.account.config.TeachingArea;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class TeachingAreaMapper {

    private static TeachingAreaMapper singleton;

    /**
     * Récupère ou créer l'instance
     *
     * @return TeachingAreaMapper
     */
    public static TeachingAreaMapper getInstance() {
        if (singleton == null) {
            singleton = new TeachingAreaMapper();
        }
        return singleton;
    }

    /**
     * maps list of teachingAreas into list of TeachingAreaDto
     *
     * @param teachingAreas list of permission to be mapped
     * @return list of TeachingAreaDto mapped
     */
    public List<TeachingAreaDto> asDtos(List<TeachingArea> teachingAreas){
        List<TeachingAreaDto> teachingAreaDtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(teachingAreas)){
            for (TeachingArea teachingArea: teachingAreas){
                teachingAreaDtos.add(GenericMapper.INSTANCE.asDto(teachingArea));
            }
        }
        return teachingAreaDtos;
    }
}
