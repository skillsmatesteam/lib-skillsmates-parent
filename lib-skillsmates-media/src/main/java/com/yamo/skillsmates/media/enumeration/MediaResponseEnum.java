package com.yamo.skillsmates.media.enumeration;

/**
 * Enumeration des code de retour possible lors des traitements sur les media.
 */
public enum MediaResponseEnum{

    /**
     * Le media a bien été crée
     */
    MEDIA_CREATED("MEDIA_CREATED"),
    /**
     * Le media existait deja en base (mais a pu être mis à jour)
     */
    MEDIA_ALREADY_EXISTING("MEDIA_ALREADY_EXISTING"),
    /**
     * Le media a bien été supprimé de la base et du fileSystem
     */
    MEDIA_DELETED("MEDIA_DELETED"),
    /**
     * Le media a bien été supprimé de la base, mais certains fichier n'ont pas pu être supprimé du fileSystem
     */
    MEDIA_PARTIALLY_DELETED("MEDIA_PARTIALLY_DELETED"),
    /**
     * Le media à bien été mis à jour
     */
    MEDIA_UPDATED("MEDIA_UPDATED"),
    /**
     * Le contenu du media est vide (il s'agit du binaire, pas des champs annexes)
     */
    MEDIA_EMPTY("MEDIA_EMPTY"),
    /**
     * Le type du media n'est pas renseigné (utilisé pour les documents ou le type est obligatoire)
     */
    MEDIA_INVALID_TYPE("MEDIA_INVALID_TYPE"),
    /**
     * Le traitement du media à échoué
     */
    MEDIA_FAILED("MEDIA_FAILED"),
    /**
     * L'identifiant du media est incorrecte
     */
    MEDIA_INCORRECT_ID("MEDIA_INCORRECT_ID"),
    /**
     * L'accès au média est interdite
     */
    MEDIA_FORBIDDEN_ACCESS("MEDIA_FORBIDDEN_ACCESS"),
    /**
     * Le media est dejà dans l'état demandé
     */
    MEDIA_ALREADY_IN_STATE("MEDIA_ALREADY_IN_STATE");

    private String code;

    MediaResponseEnum(String code) {
        this.code = code;
    }
}
