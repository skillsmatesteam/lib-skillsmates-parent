package com.yamo.skillsmates.media.helpers;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.yamo.skillsmates.models.multimedia.Multimedia;
import org.apache.commons.io.FileUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class AwsAmazonHelper {
    public static final Logger LOGGER = LoggerFactory.getLogger(AwsAmazonHelper.class);
    public static final String PATH_DELIMITER = "/";
    private static final Integer AVATAR_WIDTH = 150;
    private static final Integer AVATAR_HEIGHT = 150;

    private AmazonS3 s3client;
    private String awsAccessKey;
    private String secretKey;
    private String awsS3Bucket;
    private String rootDirectory;

    public AwsAmazonHelper(String awsAccessKey, String secretKey, String awsS3Bucket, String rootDirectory) {
        this.awsAccessKey = awsAccessKey;
        this.secretKey = secretKey;
        this.awsS3Bucket = awsS3Bucket;
        this.rootDirectory = rootDirectory;
        initAWSClient();
    }

    private void initAWSClient() {
        AWSCredentials credentials = new BasicAWSCredentials(this.awsAccessKey, this.secretKey);
        this.s3client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.US_EAST_2)
                .build();
    }

    /**
     * upload a file into AWS S3
     * @param multimedia
     * @param inputStream
     * @throws IOException
     * @throws AmazonServiceException
     */
    public void upload(Multimedia multimedia, InputStream inputStream) throws IOException, AmazonServiceException{
        File tempFile = new File(MultimediaHelper.computeFilename(multimedia));

        FileUtils.copyInputStreamToFile(inputStream, tempFile);

        PutObjectRequest request = new PutObjectRequest(awsS3Bucket, rootDirectory + PATH_DELIMITER + multimedia.getDirectory() + PATH_DELIMITER + MultimediaHelper.computeFilename(multimedia), tempFile);
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentType(multimedia.getMimeType());
        metadata.addUserMetadata("title", multimedia.getName());
        request.setMetadata(metadata);
        s3client.putObject(request);

//        tempFile.delete();
        FileUtils.forceDelete(tempFile);
    }

    /**
     * Delete multimedia into AWS S3
     * @param multimedia
     * @throws AmazonServiceException
     */
    public void delete(Multimedia multimedia) throws AmazonServiceException{
        s3client.deleteObject(new DeleteObjectRequest(awsS3Bucket, rootDirectory + PATH_DELIMITER + multimedia.getDirectory() + PATH_DELIMITER + MultimediaHelper.computeFilename(multimedia)));
    }

    /**
     * Delete file into AWS S3
     * @param filepath
     * @throws AmazonServiceException
     */
    public void delete(String filepath) throws AmazonServiceException{
        s3client.deleteObject(new DeleteObjectRequest(awsS3Bucket, rootDirectory + PATH_DELIMITER + filepath));
    }

    private void copyInputStreamToFile(InputStream inputStream, File file) throws IOException {
        try(OutputStream outputStream = new FileOutputStream(file)) {
            IOUtils.copy(inputStream, outputStream);
        }
    }
}
