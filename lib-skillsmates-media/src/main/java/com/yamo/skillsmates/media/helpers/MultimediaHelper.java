package com.yamo.skillsmates.media.helpers;

import com.yamo.skillsmates.common.enumeration.Mimetype;
import com.yamo.skillsmates.common.helper.StringHelper;
import com.yamo.skillsmates.media.enumeration.MediaResponseEnum;
import com.yamo.skillsmates.models.multimedia.Multimedia;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Objects;
import java.util.zip.Adler32;

@Component
public class MultimediaHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(MultimediaHelper.class);
    private static final Integer AVATAR_WIDTH = 200;
    private static final Integer AVATAR_HEIGHT = 219;

//    @Value("${ms.skillsmates.mimetypes}")
//    private String[] mimetypes;

    public MediaResponseEnum saveMultimedia(InputStream inputStream, Multimedia multimedia, String rootDirectory) throws IOException {
        MediaResponseEnum saveResult;
        String multimediaName = multimedia.getType().toLowerCase() + "_" + multimedia.getChecksum() + "." + multimedia.getExtension();

        // Copie du document depuis l'inputStream sur le système de fichier
        File fileToSave = new File(rootDirectory + StringHelper.PATH_DELIMITER + multimedia.getDirectory() + StringHelper.PATH_DELIMITER + multimediaName);

        if (!fileToSave.exists()) {
            // Création du Path
            File directory = new File(rootDirectory+ StringHelper.PATH_DELIMITER + multimedia.getDirectory());
            final boolean mkdirs = directory.mkdirs();
            if (!mkdirs) {
                LOGGER.debug("Le dossier suivant n'a pas été crée : {}", rootDirectory);
            }

            try (OutputStream out = new FileOutputStream(fileToSave)) {
                IOUtils.copy(inputStream, out);
                saveResult = MediaResponseEnum.MEDIA_CREATED;
            } catch (FileNotFoundException e) {
                String errorMsg = "Echec de l'ecriture du document '" + multimediaName;
                LOGGER.error(errorMsg);
                throw new IOException(errorMsg, e);
            }
        } else {
            saveResult = MediaResponseEnum.MEDIA_ALREADY_EXISTING;
            LOGGER.debug("Document déjà présent sur le FS, pas de retraitement.");
        }
        return saveResult;
    }

    public MediaResponseEnum saveAvatar(InputStream document, Multimedia multimedia, String rootDirectory) throws IOException {

        MediaResponseEnum saveResult;
        String fileName = computeFilename(multimedia);

        // Variable permettant de redimmensionner le fichier
        ImageSize imageSize = new ImageSize(AVATAR_WIDTH, AVATAR_HEIGHT, null);

        // Redimmensionnement de l'image
        BufferedImage imageToSave = resizeImage(ImageIO.read(document), imageSize);

        // Copie du document depuis l'inputStream sur le système de fichier
        File fileToSave = new File(rootDirectory + StringHelper.PATH_DELIMITER + multimedia.getDirectory() + StringHelper.PATH_DELIMITER + fileName);
        OutputStream out = null;

        if (!fileToSave.exists()) {
            // Création du Path
            File directory = new File(rootDirectory+ StringHelper.PATH_DELIMITER + multimedia.getDirectory());
            final boolean mkdirs = directory.mkdirs();
            if (!mkdirs) {
                LOGGER.debug("Le dossier suivant n'a pas été crée : {}", rootDirectory);
            }

            try {
                out = new FileOutputStream(fileToSave);
                ImageIO.write(imageToSave, multimedia.getExtension(), fileToSave);
                saveResult = MediaResponseEnum.MEDIA_CREATED;
            } catch (FileNotFoundException e) {
                String errorMsg = "Echec de l'ecriture du document '" + fileName;
                LOGGER.error(errorMsg);
                throw new IOException(errorMsg, e);
            } finally {
                IOUtils.closeQuietly(out);
            }

        } else {
            saveResult = MediaResponseEnum.MEDIA_ALREADY_EXISTING;
            LOGGER.debug("Document déjà présent sur le FS, pas de retraitement.");
        }
        return saveResult;
    }

    public boolean deleteMultimedia(String rootDirectory, String fileName) {
        boolean deleteOk = false;
        File directory = new File(rootDirectory);
        if (directory.exists() && directory.isDirectory()) {
            File[] documents = directory.listFiles();
            deleteOk = true;
            for (File image : documents) {
                if (image.getName().startsWith(fileName)) {
                    deleteOk = deleteOk && image.delete();
                }
            }
        }
        return deleteOk;
    }

    /**
     * Construit et retourne le checksum Adler32 d'un inputStream sous forme de String.
     *
     * @param inputStream le flux du fichier d'entrée
     * @return checksumValue
     */
    public String computeChecksum(InputStream inputStream){
        String checksumValue = null;
        try {
            BufferedInputStream is = new BufferedInputStream(inputStream);
            byte[] bytes = new byte[1024];
            int len;
            Adler32 adler32 = new Adler32();
            while ((len = is.read(bytes)) >= 0) {
                adler32.update(bytes, 0, len);
            }
            is.close();
            checksumValue = Long.toHexString(adler32.getValue());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return checksumValue;
    }

    /**
     * Calcul le ratio largeur/hauteur d'une image
     *
     * @param image L'image dont on veut calculer le ration
     * @return ratio
     */
    public float computeRatio(BufferedImage image) {
        float width = image.getWidth();
        float height = image.getHeight();
        return  width / height;
    }

    /**
     * Calcul le facteur d'agrandissement ou retrecissement necessaire pour que l'image donnée soit la plus grande possible tout en
     * conservant ses proportions et en ne depassant pas les tailles maximales contenu dans l'objet ImageSize
     *
     * @param image     L'image à redimmensionner
     * @param imageSize L'objet contenant les informations de taille du format cible
     * @return image L'image redimmensionné
     */
    public BufferedImage resizeImage(BufferedImage image, ImageSize imageSize) {

        double widthFactor = (double) imageSize.getWidth() / (double) image.getWidth(null);
        double heightFactor = (double) imageSize.getHeight() / (double) image.getHeight(null);
        double factor = (widthFactor < heightFactor) ? widthFactor : heightFactor;

        BufferedImage resultImage = image;
        if (factor != 1) {
            LOGGER.debug("Redimensionnement necessaire [factor={}]", factor);
            resultImage = scaleImage(image, factor);
        }
        return resultImage;
    }

    /**
     * Permet de redimensionner une image
     *
     * @param source L'image source
     * @param factor le facteur d'agrandissement : inferieur à 1 pour rétrécir, superieur à 1 pour agrandir
     * @return resultImage
     */
    public BufferedImage scaleImage(final BufferedImage source, final double factor) {
        int width = (int) (source.getWidth(null) * factor);
        int height = (int) (source.getHeight(null) * factor);
        BufferedImage resultImage = scaleImage(source, width, height);
        return resultImage;
    }

    /**
     * Permet de redimensionner une image en donnant la hauteur et la largeur en pixel
     *
     * @param source l'image source
     * @param width  la largeur souhaité
     * @param height la hauteur souhaité
     * @return image
     */
    public static BufferedImage scaleImage(BufferedImage source, int width, int height) {
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = (Graphics2D) img.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(source, 0, 0, width, height, null);
        g.dispose();
        return img;
    }

    public String getExtension(MultipartFile file){
        // Recuperation de l'extension du fichier original
        String extension = "";
        if (Objects.requireNonNull(file.getOriginalFilename()).lastIndexOf('.') > 0) {
            extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.') + 1);
        }
        return extension;
    }

    public static String computeFilename(Multimedia multimedia){
        return multimedia.getType().toLowerCase() + "_" + multimedia.getChecksum() + "." + multimedia.getExtension();
    }

    /**
     * check if mimetype is correct
     * @param file
     * @return true is file mimetype correct
     */
    public boolean validateFileMimetype(MultipartFile file){
        return file != null && Mimetype.isMimetypeAllowed(file.getContentType());
    }
}
