package com.yamo.skillsmates.media.helpers;

import com.yamo.skillsmates.common.helper.StringHelper;
import com.yamo.skillsmates.dto.multimedia.MetadataDto;
import org.hornetq.utils.json.JSONException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Base64;

public class MetadataHelper {

    public static String decodeSafeUrl(String theUrl) throws IOException {
        byte[] decodedURLBytes = Base64.getDecoder().decode(theUrl.replace('-', '+').replace('_', '/'));
        return new String(decodedURLBytes);
    }

    public static MetadataDto parseHtml(String html, String urlDecoded) throws IOException, JSONException {
        MetadataDto metadataDto = new MetadataDto();
        Document doc = Jsoup.parse(html);

        // title
        String title;
        Elements metaOgTitle = doc.select("meta[property=og:title]");
        if(metaOgTitle != null && !metaOgTitle.isEmpty()){
            title = metaOgTitle.first().attr("content");
        } else{
            title = doc.title();
        }
        metadataDto.setTitle(StringHelper.removeUnicodeCharacters(title));

        // description
        String description = "";
        Elements metaOgDescription = doc.select("meta[property=og:description]");
        if(metaOgDescription != null && !metaOgDescription.isEmpty()){
            description = metaOgDescription.first().attr("content");
        } else{
            Elements elements = doc.select("meta[name=description]");
            if(elements != null && !elements.isEmpty()) {
                description = elements.get(0).attr("content");
            }
        }
        metadataDto.setDescription(StringHelper.removeUnicodeCharacters(description));

        // image
        Elements metaOgImage  = doc.select("meta[property=og:image]");
        if(metaOgImage != null && !metaOgImage.isEmpty()){
            metadataDto.setImage(metaOgImage.first().attr("content"));
        }

        // url de l audio
        Elements metaOgAudio  = doc.select("meta[property=og:audio]");
        if(metaOgAudio != null && !metaOgAudio.isEmpty()){
            metadataDto.setAudio(metaOgAudio.first().attr("content"));
            //type de l audio
            Elements metaOgAudioType  = doc.select("meta[property=og:audio:type]");
            if(metaOgAudioType != null && !metaOgAudioType.isEmpty()) {
//                json.put("og:audio:type", metaOgAudioType.first().attr("content"));
            }
        }

        // url de la video
        Elements metaOgVideo  = doc.select("meta[property=og:video]");
        if(metaOgVideo != null && !metaOgVideo.isEmpty()){
            metadataDto.setVideo(metaOgVideo.first().attr("content"));
            //type de la video
            Elements metaOgVideoType  = doc.select("meta[property=og:video:type]");
            if(metaOgVideoType != null && !metaOgVideoType.isEmpty()) {
//                json.put("og:video:type",metaOgVideoType.first().attr("content"));
            }

        }

        metadataDto.setUrl(urlDecoded);
        metadataDto.setCanonical(urlDecoded);
        return metadataDto;
    }

    public static String getUrlContents(String theUrl) throws IOException {
        StringBuilder content = new StringBuilder();
        // create a url object
        URL url = new URL(theUrl);

        // create a urlconnection object
        URLConnection urlConnection = url.openConnection();

        // wrap the urlconnection in a bufferedreader
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

        String line;

        // read from the urlconnection via the bufferedreader
        while ((line = bufferedReader.readLine()) != null) {
            content.append(line).append("\n");
        }
        bufferedReader.close();
        return content.toString();
    }
}
