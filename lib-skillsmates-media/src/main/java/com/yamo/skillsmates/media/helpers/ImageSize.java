package com.yamo.skillsmates.media.helpers;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yamo.skillsmates.common.enumeration.ImageSizeEnum;
import com.yamo.skillsmates.common.logging.Loggable;

/**
 * POJO permettant de manipuler des tailles d'images plus parlantes qu'un simple enum. La résolution correspondante est incluse dans
 * l'objet.
 */
public class ImageSize implements Loggable {

    private int width;
    private int height;
    private ImageSizeEnum format;

    /**
     * Constructeur
     *
     * @param width  la largeur de l'image
     * @param height la hauteur de l'image
     * @param format le format de l'image
     */
    public ImageSize(int width, int height, ImageSizeEnum format) {
        this.width = width;
        this.height = height;
        this.format = format;
    }

    /**
     * @return width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @return height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @return format
     */
    public ImageSizeEnum getFormat() {
        return format;
    }

    @Override
    @JsonIgnore
    public String getLog() {
        return "{format:" + format + "}";
    }


    @Override
    @JsonIgnore
    public String getLogDetail() {
        return "{width:" + width + ", height:" + height + ", format:" + format + "}";
    }
}
